import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HeaderComponent} from "./components/header.component";
import {FooterComponent} from "./components/footer.component";
import {MenubarModule} from "primeng/menubar";
import {AvatarModule} from "primeng/avatar";
import { AdminPageComponent } from './components/pages/admin-page/admin-page.component';
import { SettingsPageComponent } from './components/pages/settings-page/settings-page.component';
import {UserService} from "./services/user.service";
import {MessageModule} from "primeng/message";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ButtonModule} from "primeng/button";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "./interseptors/TokenInterceptor";
import {AuthRestService} from "./services/rest/auth-rest.service";
import {AuthService} from "./services/auth.service";
import {ErrorInterceptor} from "./interseptors/ErrorInterceptor";
import {CommonModule} from "@angular/common";
import {ScrollPanelModule} from "primeng/scrollpanel";
import {ComponentsModule} from "./components/components.module";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AdminPageComponent,
    SettingsPageComponent,


  ],
    imports: [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        MenubarModule,
        AvatarModule,
        MessageModule,
        ToastModule,
        BrowserAnimationsModule,
        ButtonModule,
        HttpClientModule,
        ScrollPanelModule,
        ComponentsModule
    ],
  providers: [
    UserService,
    AuthRestService,
    AuthService,
    MessageService,
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi:true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi:true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
