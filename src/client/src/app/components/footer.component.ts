import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div class="footer">
      <div class="footer-item">
        <div class="footer-text">© 2023-{{today|date:'YYYY'}} XF RetailGroup Inc.</div>
      </div>
      <div class="footer-item">
        <a href="https://www.linkedin.com/company/" target="_blank" class="social-btn w-inline-block">
          <img src="https://uploads-ssl.webflow.com/5d8125bcaf917cc3a7c46317/61e9ca57c2be2beef1e86e7a_linkedin.svg"
               loading="lazy"
               alt="linkedin icon"
               class="icon-n no-marg">
        </a>
        <a href="https://www.facebook.com/"
           target="_blank"
           class="social-btn w-inline-block">
          <img src="https://uploads-ssl.webflow.com/5d8125bcaf917cc3a7c46317/61e9ca57a8f37c0c6cfc415e_facebook.svg"
               loading="lazy"
               alt="facebook icon"
               class="icon-n no-marg">
        </a>
        <a href="https://github.com/zip313"
           target="_blank"
           class="social-btn w-inline-block">
          <img src="https://uploads-ssl.webflow.com/5d8125bcaf917cc3a7c46317/61e9ca572bb860480bf83db0_github-logo.svg"
               loading="lazy" alt="github icon"
               class="icon-n no-marg">
        </a>
        <a href="https://twitter.com/" target="_blank" class="social-btn w-inline-block">
          <img src="https://uploads-ssl.webflow.com/5d8125bcaf917cc3a7c46317/61e9ca57e7dc9daae97b9bee_twitter.svg"
               loading="lazy"
               alt="twitter icon"
               class="icon-n no-marg">
        </a>
      </div>
    </div>
  `,
  styles: [
    `
      .footer{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        //line-height: 50px;
        height: 100%;
        background: var(--gradient-footer);
        //background: linear-gradient(to right,#e66465 50%, #9198e5 100%);//linear-gradient(var(--gray-dark-color), var(--blue-dark-color));
        color: var(--white-color);
        //border-radius: 5px;
        padding: .5rem;
      }

     .footer-item{
       display: inline-block;
       margin:auto 1rem;
      }
     .footer-text{
       display: inline-block;
       margin: auto;
     }
    `
  ]
})
export class FooterComponent {
  today: number = Date.now();
}
