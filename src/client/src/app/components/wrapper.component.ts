import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-wrapper',
  template: `
    <div class="wrapper">
        <ng-content></ng-content>
    </div>
  `,
  styles:[
    `
      :host{

      }
    .wrapper{
      display: inline-block;
      //margin: 1rem 1rem;
      padding: 1rem;
      //width: 75%;
      height: 100%;
      overflow: auto;
      border-radius: 5px;
      box-shadow: 5px 5px 5px 5px var(--white-color-opacity);
      background-color: var(--white-color);
    }
  `]

})
export class WrapperComponent implements OnInit{
  @HostBinding('style.display') display?: string;
  @HostBinding('style.width.%') width?: number;
  @HostBinding('style.height.%') height?: number;
  ngOnInit(): void {
    this.display = 'block';
    this.width=+this.widthPercent;
    this.height=100;
  }
  @Input() widthPercent:string='100';
}
