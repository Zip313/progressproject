import { Component } from '@angular/core';
import {ThemeService} from "../../../services/theme.service";

@Component({
  selector: 'app-settings-page',
  template: `
    <div>
      <app-wrapper widthPercent="100">
        <p-scrollPanel [style]="{ width: '100%', height: '100%' }" >
          <button (click)="changeTheme('lara-light-blue')">saddddddddddddd</button>
        </p-scrollPanel>
      </app-wrapper>
    </div>
  `,
  styles: [
  ]
})
export class SettingsPageComponent {
  constructor(private readonly themeService: ThemeService) {
  }
  changeTheme(theme: string) {
    console.log(theme)
    this.themeService.switchTheme(theme)
  }
}
