import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsPageRoutingModule } from './reports-page-routing.module';
import {ComponentsModule} from "../../components.module";
import {ReportsPageComponent} from "./reports-page.component";
import {ScrollPanelModule} from "primeng/scrollpanel";
import { SummaryReportComponent } from './summary-report.component';
import {ReportsService} from "../../../services/reports.service";
import {ReportsRestService} from "../../../services/rest/reports-rest.service";
import {HttpClientModule} from "@angular/common/http";
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";
import {CheckboxModule} from "primeng/checkbox";
import {FormsModule} from "@angular/forms";
import {TagModule} from "primeng/tag";


@NgModule({
  declarations: [
    ReportsPageComponent,
    SummaryReportComponent
  ],
  imports: [
    CommonModule,
    ReportsPageRoutingModule,
    ComponentsModule,
    ScrollPanelModule,
    TableModule,
    ButtonModule,
    CheckboxModule,
    FormsModule,
    TagModule,
  ],
  providers:[ReportsService,ReportsRestService]
})
export class ReportsPageModule { }
