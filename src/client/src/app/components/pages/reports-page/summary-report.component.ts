import {Component, OnDestroy, OnInit} from '@angular/core';
import {ReportsService} from "../../../services/reports.service";
import {ReportsRestService} from "../../../services/rest/reports-rest.service";
import {take, tap} from "rxjs";
import {summaryReportDto} from "../../../models/dto/summaryReportDto";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-summary-report',
  template: `
    <div style="padding:.5rem; font-size: .8rem ">
      <p-table
        [value]="summaryReportDtosFiltered"
        [paginator]="true"
        [rows]="8"
        [showCurrentPageReport]="true"
        [tableStyle]="{ 'min-width': '50rem' }"
        [tableStyle]="{ 'width': '100%' }"
        [globalFilterFields]="['SecId']"
        currentPageReportTemplate="Показано {first} - {last} записей из {totalRecords}"
        [rowsPerPageOptions]="[8, 15, 50]"
      >
        <ng-template pTemplate="header">
          <tr>
            <th pSortableColumn="secId" style="width:10%">
              <div class="flex align-items-center">
                SecId
                <p-columnFilter type="text" field="secId" display="menu"></p-columnFilter>
                <p-sortIcon field="stockShortName"></p-sortIcon>
              </div>
            </th>
            <th pSortableColumn="currentPrice" style="width:5%">Цена<p-sortIcon field="currentPrice"></p-sortIcon></th>
            <th pSortableColumn="userPrice" style="width:5%">Цена покупки</th>
            <th pSortableColumn="count" style="width:5%">Количество<p-sortIcon field="count"></p-sortIcon></th>
            <th pSortableColumn="currentAmount" style="width:7%">Сумма<p-sortIcon field="currentAmount"></p-sortIcon></th>
            <th pSortableColumn="userAmount" style="width:7%">Сумма себестоимость</th>
            <th pSortableColumn="currentAmountCommission" style="width:5%">Комиссия при продаже</th>
            <th pSortableColumn="currentAmountPure" style="width:7%">На руки<p-sortIcon field="currentAmountPure"></p-sortIcon></th>
            <th pSortableColumn="saleProfit" style="width:7%">Доход<p-sortIcon field="saleProfit"></p-sortIcon></th>
            <th pSortableColumn="dividendsValue" style="width:5%">Дивиденд<p-columnFilter type="numeric" field="dividendsValue" display="menu" currency="RUB"></p-columnFilter><p-sortIcon field="DivValue"></p-sortIcon></th>
            <th pSortableColumn="dividendsRegistryDate" style="width:7%">Реестр<p-sortIcon field="dividendsRegistryDate"></p-sortIcon></th>
            <th pSortableColumn="dividendsAmount" style="width:7%">Сумма<p-sortIcon field="dividendsAmount"></p-sortIcon></th>
            <th pSortableColumn="dividendsAmountTax" style="width:5%">Налог<p-sortIcon field="dividendsAmountTax"></p-sortIcon></th>
            <th pSortableColumn="dividendsAmountPure" style="width:7%">На руки<p-sortIcon field="dividendsAmountPure"></p-sortIcon></th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-summary>
          <tr>
            <td><p-tag [value]="summary.secId" severity="Success" ></p-tag>{{ summary.stockShortName}}</td>
            <td>{{ summary.currentPrice | currency:'RUB':'symbol-narrow' }}</td>
            <td [ngStyle]="{'color':summary.userPrice<summary.currentPrice?'green':'red'}">{{ summary.userPrice }}</td>
            <td>{{ summary.count }}</td>
            <td>{{ summary.currentAmount | currency:'RUB':'symbol-narrow' }}</td>
            <td>{{ summary.userAmount | currency:'RUB':'symbol-narrow' }}</td>
            <td>{{ summary.currentAmountCommission | currency:'RUB':'symbol-narrow' }}</td>
            <td>{{ summary.currentAmountPure | currency:'RUB':'symbol-narrow' }}</td>
            <td [ngStyle]="{'color':summary.isProfit?'green':'red'}">
              <i [ngClass]="summary.isProfit?'pi pi-arrow-up':'pi pi-arrow-down'" style="font-size: .6rem" ></i>
              &nbsp;{{summary.saleProfitPercent | percent:'1.1'}}
              &nbsp;{{ summary.saleProfit  | currency:'RUB':'symbol-narrow' }}
            </td>
            <td style="color: green">
              {{summary.dividendsValuePercent | percent:'1.1'}}
              &nbsp;{{ summary.dividendsValue  | currency:'RUB':'symbol-narrow' }}
            </td>
            <td>{{ summary.dividendsRegistryCloseDate | date:"dd.MM.yyyy" }}</td>
            <td>{{ summary.dividendsAmount | currency:'RUB':'symbol-narrow' }}</td>
            <td>{{ summary.dividendsAmountTax | currency:'RUB':'symbol-narrow' }}</td>
            <td>{{ summary.dividendsAmountPure | currency:'RUB':'symbol-narrow' }}</td>
          </tr>
        </ng-template>
        <ng-template pTemplate="paginatorleft" class="flex flex-row">
          <div class="">
            <p-checkbox [(ngModel)]="filter['justMy']" [binary]="true" inputId="jm" (onChange)="filterTable()"></p-checkbox><label for="jm">Только портфель</label>
          </div>
        </ng-template>
        <ng-template pTemplate="paginatorright">
          <p-button type="button" icon="pi pi-cloud" styleClass="p-button-text"></p-button>
        </ng-template>
      </p-table>
    </div>
  `,
  styles: [
  ]
})
export class SummaryReportComponent implements OnInit,OnDestroy{
  filter:{justMy:boolean} = {justMy:true}
  summaryReportDtos:summaryReportDto[];
  summaryReportDtosFiltered:summaryReportDto[];
  constructor(
    private readonly reportsService:ReportsService,
    private readonly authService:AuthService,
  ) {
  }
  ngOnInit(): void {
    if(this.authService.isDemo){
      this.reportsService.getSummaryDemo$()
        .pipe(
          take(1),
          tap(response=>{
            this.summaryReportDtos=response;
            this.summaryReportDtosFiltered=response.filter(s=>s.isMy);
            this.filter.justMy=true;
          })
        )
        .subscribe();
      return
    }

    this.reportsService.getSummary$()
      .pipe(
        take(1),
        tap(response=>{
          this.summaryReportDtos=response;
          this.summaryReportDtosFiltered=response.filter(s=>s.isMy);
          this.filter.justMy=true;
          console.log(this.summaryReportDtosFiltered)
        })
      )
      .subscribe();
  }
  ngOnDestroy(): void {
  }

  filterTable() {
    this.summaryReportDtosFiltered = this.summaryReportDtos.filter(s=>{
      return (this.filter.justMy
        ?s.isMy
        :true)
    })
  }
}
