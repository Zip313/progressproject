import { Component } from '@angular/core';

@Component({
  selector: 'app-reports-page',
  template: `
    <div class="wrapper">
      <app-wrapper widthPercent="100">
        <p-scrollPanel [style]="{ width: '100%', height: '100%' }" >
            <router-outlet></router-outlet>
        </p-scrollPanel>
      </app-wrapper>
    </div>
  `,
  styles: [
  ]
})
export class ReportsPageComponent { }
