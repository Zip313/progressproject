import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReportsPageComponent} from "./reports-page.component";
import {SummaryReportComponent} from "./summary-report.component";

const routes: Routes = [
  {
    path:'',
    component:ReportsPageComponent,
    children:[
      {
        path:'summary',
        component:SummaryReportComponent
      },
      {
        path:'**',
        redirectTo:'summary'
      }
    ]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsPageRoutingModule { }
