import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkspacePageComponent} from "./workspace-page.component";
import {WorkspacePageRoutingModule} from "./workspace-page-routing.module";
import {WorkSpaceService} from "../../../services/work-space.service";
import {WorkSpaceRestService} from "../../../services/rest/work-space-rest.service";
import {ComponentsModule} from "../../components.module";
import {BrowserModule} from "@angular/platform-browser";
import {ButtonModule} from "primeng/button";
import { OrderEditComponent } from './order-edit/order-edit.component';
import {FormsModule} from "@angular/forms";
import {InputTextareaModule} from "primeng/inputtextarea";
import {InputTextModule} from "primeng/inputtext";
import {StyleClassModule} from "primeng/styleclass";
import {InputNumberModule} from "primeng/inputnumber";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";
import {ScrollPanelModule} from "primeng/scrollpanel";
import {TooltipModule} from "primeng/tooltip";
import {AppModule} from "../../../app.module";
import {PanelModule} from "primeng/panel";
@NgModule({
  declarations: [
    WorkspacePageComponent,
    OrderEditComponent,
  ],
    imports: [
        CommonModule,
        FormsModule,
        WorkspacePageRoutingModule,
        ComponentsModule,
        ButtonModule,
        InputTextModule,
        StyleClassModule,
        InputNumberModule,
        InputTextareaModule,
        CalendarModule,
        DropdownModule,
        ScrollPanelModule,
        TooltipModule,
        PanelModule,
    ],
  providers:[WorkSpaceService,WorkSpaceRestService]
})
export class WorkspacePageModule { }
