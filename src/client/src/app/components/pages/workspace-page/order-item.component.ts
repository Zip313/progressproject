import {AfterViewInit, Component, Input} from '@angular/core';
import {Order} from "../../../models/Order";
@Component({
  selector: 'app-order-item',
  template: `
    <div class="content">
      <div class="card" [ngClass]="{'card_checked':isChecked}">
        <div class="badges">
          <i class="badge" pBadge style="font-size: 2rem; " value="{{order.stock.secId}}"></i>
        </div>
        <div class="card_info" [ngClass]="{'card_info_checked':isChecked}">
          <span class="logo">{{order.stock.shortName}}</span>
          <div class="price-row">
            <span class="">{{order.stock.currencyId}}</span>
            <span [ngStyle]="{'color':this.order.stock.isIncrease?'green':'red'}" style="font-size: .8rem; ">
              <i [ngClass]="order.stock.isIncrease?'pi pi-arrow-up':'pi pi-arrow-down'" style="font-size: .6rem"></i>&nbsp;
              {{order.stock.increaseValuePer | percent:'1.3-5'}}
            </span>
            <span class="price"
                  [ngStyle]="{'color':this.order.isIncrease?'green':'red'}"
            >
                {{order.stock.prevPrice | currency:'RUB':'symbol-narrow'}}
              </span>
          </div>
          <span>дата покупки: {{order.ownDate | date:'dd.MM.yyyy'}}</span>
          <span>количество: {{order.count}}шт.</span>

          <span>на сумму: {{this.order.currentAmount | currency:'RUB':'symbol-narrow'}}</span>
        </div>
        <div class="card_hidden_info" [ngClass]="{'card_hidden_info_checked':isChecked}">
          <span class="card_hidden_info_item">
            цена покупки:   <i
            [ngStyle]="{'color':this.order.isIncrease?'green':'red'}">{{this.order.price | currency:'RUB':'symbol-narrow'}}</i></span>
          <span
            class="card_hidden_info_item">себестоимость:   {{this.order.costAmount | currency:'RUB':'symbol-narrow'}}</span>
          <span class="card_hidden_info_item"
                [ngStyle]="{'color':this.order.isIncrease?'green':'red'}"
          >
            рост:&nbsp;&nbsp;
            {{this.order.increaseValue | currency:'RUB':'symbol-narrow'}}&nbsp;&nbsp;&nbsp;
            <i [ngClass]="order.isIncrease?'pi pi-arrow-up':'pi pi-arrow-down'" style="font-size: .6rem"></i>&nbsp;
            <i style="font-size: .8rem">{{this.order.increaseValuePer | percent:'1.3'}}</i>
          </span>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .content{
        padding: .5rem;
        font-weight: 600;
      }
      .card{
        position: relative;
        display: inline-block;
        cursor: pointer;
        padding:  20px .5rem .5rem .5rem;
        border-radius: 5px;
        height: 170px;
        width: 240px;
        transition: all .2s ease-in-out;
      }
        .card:hover{
          background-color: #ffffff;
          box-shadow: 0 20px 25px rgba(61,74,88,0.2);
          transition: all .3s ease-in-out;
        }
        .card_checked{
          background-color: #ffffff;
          box-shadow: 0 20px 25px rgba(61,74,88,0.2);
        }
      .card_info{
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        border-radius: 5px;
        width: 100%;
        height: 90%;

      }
        .card:hover > .card_info {
          background-color: #ffffff;
          transition: all 2.2s ease-in-out;
        }
      .card_info_checked{
        background-color: #ffffff;
        transition: all 2.2s ease-in-out;
      }
      .card_hidden_info{
        position: absolute;
        flex-direction: column;
        justify-content: space-around;
        width: 100%;
        //height: 130px;
        display: none;
        padding: 1rem .5rem;
        left:0;
        bottom:10px;
        transform: translateY(100%);
        border-radius: 5px;
        box-shadow: 0 20px 25px rgba(61,74,88,0.1);
        z-index: 1000;
      }
        .card:hover > .card_hidden_info {
          display: flex;
          background-color: #ffffff;
          transition: all .2s ease-in-out;
        }
      .card_hidden_info_checked{
        display: flex;
        background-color: #ffffff;
        transition: all .2s ease-in-out;
      }

      .badge{
        position: absolute;
        top:5px;
        left:0px;
        display: inline-block;
        width:20px;
      }
      .logo{
        margin: .1rem 0;
        font-size: 1.2rem;
        font-weight: 600;
        text-align: center;
      }
      .price-row{
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        margin: .2rem 0;
        font-size: 1rem;
        font-weight: 600;
      }
      .card_hidden_info_item{
        margin: .3rem;
      }
    `
  ]
})
export class OrderItemComponent implements AfterViewInit{
  @Input() order: Order;
  @Input() isChecked: boolean = false;
  ngAfterViewInit(): void {
  }
}
