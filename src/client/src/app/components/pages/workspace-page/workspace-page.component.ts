import {Component, OnDestroy, OnInit} from "@angular/core";
import {WorkSpaceService} from "../../../services/work-space.service";
import {interval, mergeMap, pipe, Subject, Subscription, take, tap} from "rxjs";
import {Order} from "../../../models/Order";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-workspace',
  template: `
    <div class="wrapper">
      <app-wrapper widthPercent="74">
        <p-scrollPanel [style]="{ width: '100%', height: '100%' }" >
          <div class="stocks-item-list">
            <div *ngFor="let order of workSpaceService.Orders; let i = index" (click)="handleOrderClick(order)">
              <app-order-item [order]="order" [isChecked]="order===workSpaceService.checkedOrder"></app-order-item>
            </div>
          </div>
          <div style="margin: 1rem 0 1rem 0;position: absolute; bottom: 5px">{{workSpaceService.summaryText2()}}</div>
        </p-scrollPanel>
      </app-wrapper>

      <app-wrapper widthPercent="24">
        <div *ngIf="!authService.isDemo">
          <p-scrollPanel [style]="{ width: '100%', height: '100%' }" >
              <app-order-edit [ShortNames]="this.workSpaceService.ShortNames" ></app-order-edit>
          </p-scrollPanel>
        </div>
      </app-wrapper>
    </div>
  `,
  styles: [
    `
      .stocks-item-list{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: flex-start;
      }
    `
  ]
})
export class WorkspacePageComponent implements OnInit,OnDestroy{
  subscriptionStocks$?:Subscription;
  subscriptionShortNamesList$?:Subscription;
  constructor(
    public readonly workSpaceService:WorkSpaceService,
    public readonly authService:AuthService,
  ) { }

  ngOnInit(): void {
    if(this.authService.isDemo){
      this.workSpaceService.getOrdersDemo().pipe(take(1)).subscribe();
      return;
    }
    this.workSpaceService.getOrders$().pipe(take(1)).subscribe();
    this.subscriptionStocks$ = interval(1 * 60 * 1000)
      .pipe(
        tap(x=>console.log('jksdlf')),
        mergeMap(() => this.workSpaceService.getOrders$())
      )
      .subscribe()

     this.subscriptionShortNamesList$ = this.workSpaceService.getShortNames$()
       .subscribe(names=>{
       })
  }
  ngOnDestroy(): void {
    if(this.subscriptionStocks$) this.subscriptionStocks$.unsubscribe();
  }
  handleOrderClick(order: Order) {
    this.workSpaceService.checkedOrder = order;
    this.workSpaceService.subjectCheckedOrder$.next(order)
  }

}
