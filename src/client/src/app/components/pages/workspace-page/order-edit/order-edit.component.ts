import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {WorkSpaceService} from "../../../../services/work-space.service";
import {Subscription, take} from "rxjs";
import {AddOrderDto} from "../../../../models/dto/addOrderDto";
import {MessageService} from "primeng/api";
import {ShortStock, ShortStocks} from "../../../../models/ShortStocks";

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit,OnDestroy {
  shortStocks: ShortStock[];
  subscribtion: Subscription;

  @Input() set ShortNames(sn: ShortStock[]) {
    this.shortStocks = sn
  };
  selectedShortStock?: ShortStock;
  price?: number | undefined;
  count?: number
  ownDate?: Date = new Date();

  get IsFormValid() {
    return Boolean(this.selectedShortStock)
      && Boolean(this.price)
      && Boolean(this.count)
      && Boolean(this.ownDate)
  }

  get isFormEdited() {
    return Boolean(this.selectedShortStock)
      || Boolean(this.price)
      || Boolean(this.count)
      || Boolean(this.ownDate)
  }
  constructor(
    public readonly workSpaceService: WorkSpaceService,
    public readonly messageService: MessageService,
  ) {  }
  get isOrderExists() {
    try {
      return this.workSpaceService.Orders.some(o => o.stock.shortName === this.selectedShortStock?.shortName);
    } catch (e) {
    }
    return false;
  }
  canSaveOrder() {
    try {
      return !this.workSpaceService.Orders
        .filter(o=>o.stock.shortName!==this.workSpaceService.checkedOrder?.stock.shortName)
        .some(o => o.stock.shortName === this.selectedShortStock?.shortName);
    } catch (e) {
    }
    return false;
  }
  ngOnInit(): void {
    this.subscribtion = this.workSpaceService.subjectCheckedOrder$
      .subscribe(order => {
        // this.checkedOrder=order;
        this.selectedShortStock = this.shortStocks.find(x=>x.shortName === order.stock.shortName);
        this.count = order.count;
        this.ownDate = order.ownDate;
        this.price = order.price
      })
  }

  ngOnDestroy(): void {
    if (this.subscribtion) this.subscribtion.unsubscribe();
  }
  sendForm(form: NgForm) {  }
  handleCancel() {
    this.workSpaceService.checkedOrder = undefined;
    this.selectedShortStock = undefined;
    this.count = undefined;
    this.ownDate = undefined;
    this.price = undefined;
  }
  handleDelete() {
    if (!this.workSpaceService.checkedOrder) {
      this.messageService.add({severity: 'error', summary: 'Ошибка, не выбран ордер'})
      return;
    }
    this.workSpaceService.deleteOrder$(this.workSpaceService.checkedOrder)
      .subscribe(response => {
        this.messageService.add({severity: 'success', summary: `Удалено ${response.deletedCount} записей`});

        this.workSpaceService.checkedOrder = undefined;
        this.handleCancel()
      })
  }
  handleSave() {
    if (!this.selectedShortStock || !this.count || !this.ownDate || !this.price) {
      this.messageService.add({severity: 'error', summary: 'Ошибка, не все поля заполнены'})
      return;
    }
    if (!this.canSaveOrder()) {
      this.messageService.add({severity: 'error', summary: 'Такой ордер уже есть в списке'})
      return;
    }
    console.log('save order ', this.workSpaceService.checkedOrder?.id)
    const updateOrder: AddOrderDto = new AddOrderDto(this.selectedShortStock.shortName, this.count, this.ownDate, this.price);
    updateOrder.id = this.workSpaceService.checkedOrder?.id;
    this.workSpaceService.updateOrder$(updateOrder)
      .pipe(take(1))
      .subscribe(order => {
        console.log('saved order ', order.id)
        this.messageService.add({severity: 'success', summary: 'Ордер успешно обновлен'})
        this.handleCancel()
      })
  }
  handleAdd() {
    console.log(this.selectedShortStock);
    if (!this.selectedShortStock || !this.count || !this.ownDate || !this.price) {
      this.messageService.add({severity: 'error', summary: 'Ошибка, не все поля заполнены'})
      return;
    }
    if (this.isOrderExists) {
      this.messageService.add({severity: 'error', summary: 'Такой ордер уже существует, отредактируйте его'})
      return;
    }
    const newOrder: AddOrderDto = new AddOrderDto(this.selectedShortStock.shortName, this.count, this.ownDate, this.price)
    this.workSpaceService.addOrder$(newOrder)
      .pipe(take(1))
      .subscribe(order => {
        this.messageService.add({severity: 'success', summary: 'Ордер успешно добавлен'})
        this.handleCancel()
      })
  }
}
