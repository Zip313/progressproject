import { Component } from '@angular/core';

@Component({
  selector: 'app-demo',
  template: `
    <div class="demo">
      <p-carousel [value]="demoPhotos" *ngIf="demoPhotos"  [numVisible]="1" [numScroll]="1" [circular]="true" autoPlayInterval="1000" autoplay="true">
        <ng-template pTemplate="header">
        </ng-template>
        <ng-template let-photo pTemplate="item">
          <div class="img-carousel_block">
            <img src="{{photo}}" class="img-carousel"/>
          </div>
        </ng-template>
      </p-carousel>
    </div>
  `,
  styles: [`
    .demo{
      height: 100%;
    }
    .img-carousel_block{
      max-height: 80%;
      text-align: center;
      vertical-align: top;
    }
    .img-carousel{
      width: 100%;
      max-height: 600px;
      object-fit: fill;
    }
  `
  ]
})
export class DemoComponent {
  public demoPhotos:string[] = [
    '../../../../../assets/img/demo7.png',
    '../../../../../assets/img/demo8.png',
    '../../../../../assets/img/demo6.png',
  ];
  responsiveOptions: any[] | undefined = [
    {
      breakpoint: '1199px',
      numVisible: 1,
      numScroll: 1
    },
    {
      breakpoint: '991px',
      numVisible: 2,
      numScroll: 1
    },
    {
      breakpoint: '767px',
      numVisible: 1,
      numScroll: 1
    }
  ];

}
