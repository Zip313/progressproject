import {Component, OnDestroy} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../../../../services/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-auth',
  template: `
    <div class="wrapper" *ngIf="!authService.isAuthenticated">
      <form #form="ngForm" (ngSubmit)="sendFormToAuth(form)">
        <div class="regform">
          <div class="regform-item">
            <span>Email</span>
            <input type="text"
                   pInputText
                   autocomplete="email"
                   [ngClass]="{'input-error': !email}"
                   #email="ngModel"
                   ngModel
                   name="email"/>
          </div>
          <div class="regform-item">
            <span>Password</span>
            <input type="password"
                   autocomplete="password"
                   pInputText
                   [ngClass]="{'input-error': !psw}"
                   #psw="ngModel"
                   ngModel
                   name="psw"/>
          </div>
        </div>
        <div style="">
          <p-button label="Войти"
                    styleClass="p-button-sm"
                    type="submit"
                    [disabled]="!email || !psw "
          ></p-button>
        </div>
        <div class="regform-item">
          <a routerLink="/login/registration"> Еще нет учетной записи? Зарегистрироваться</a>
        </div>
        <div class="regform-item">
          <a routerLink="/login/forgotPassword"> Забыли пароль?</a>
        </div>
        <p-button label="Демо режим"
                  styleClass="p-button-sm"
                  (onClick)="demoLogin()"
        ></p-button>
      </form>
    </div>
    <div *ngIf="authService.isAuthenticated">
      <div>Здравствуйте {{authService.userService.User?.email}}</div>
      <div>Рады видеть вас в нашем сервисе</div>
      <div *ngIf="!authService.userService.User?.isActivated">Необходимо активировать аккаунт по ссылке направленной
        письмом на указанный Email
      </div>
      <br>
      <p-button styleClass="p-button-secondary" (onClick)="logout()">Выйти из сервиса</p-button>
    </div>
  `,
  styles: [`
    .wrapper{
      display: flex;
      flex-direction: column;
      width: 100%;
      padding: 1rem;
    }
    .regform{
      display: flex;
      flex-direction: column;
    }
    .regform-item{
      margin: .5rem ;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    }
    .input-error{
      border: 1px solid red !important;
    }
  `
  ]
})
export class AuthComponent implements OnDestroy{
  subscription?:Subscription;
  constructor(
    public authService:AuthService,
    private messageService: MessageService,
    private router: Router
  ) {
  }
  sendFormToAuth(form: NgForm) {
    const user = form.value;
    if(!user.psw || !user.email){
      this.messageService.add({
        severity:'error',
        summary:'error validation from',
        detail:`Поля формы заполнены не корректно`
      });
      return;
    }
    const res = this.authService.authenticate(user.email,user.psw)
    this.subscription = res.subscribe(
      auth=>{
        this.authService.setAuth(auth);

        form.reset();
        this.messageService.add({
          severity:'success',
          summary:`Успешный вход ${user.email} `,
          detail:`Рады видеть Вас в нашем сервисе`
        });
      },
      error=>{
        this.messageService.add({
          severity:'error',
          summary:`Ошибка входа (${error.error.statusCode})`,
          detail:`${error.error.message}`
        });
      })
  }
  ngOnDestroy(): void {
    if(this.subscription) this.subscription.unsubscribe();
  }
  logout() {
    this.authService.logout();
  }
  demoLogin() {
    this.messageService.add({
      severity:'success',
      summary:`Успешный вход Demo `,
      detail:`Запущен демо режим, рады видеть Вас в нашем сервисе`
    });
    this.authService.setDemoAuth();
  }
}
