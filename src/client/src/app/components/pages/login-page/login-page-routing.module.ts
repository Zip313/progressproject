import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginPageComponent} from "./login-page.component";
import {DemoComponent} from "./demo/demo.component";
import {RegistrationComponent} from "./registration/registration.component";
import {AuthComponent} from "./auth/auth.component";


const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
    children:[
      {
        path:'auth',
        component:AuthComponent
      },
      {
        path:'registration',
        component:RegistrationComponent
      },
      {
        path:'**',
        redirectTo:'auth'
      }
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginPageRoutingModule { }
