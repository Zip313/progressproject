import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginPageComponent} from "./login-page.component";
import {RouterOutlet} from "@angular/router";
import {LoginPageRoutingModule} from "./login-page-routing.module";
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './registration/registration.component';
import {DemoComponent} from "./demo/demo.component";
import {CarouselModule} from "primeng/carousel";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {InputNumberModule} from "primeng/inputnumber";
import {ButtonModule} from "primeng/button";
import {CheckboxModule} from "primeng/checkbox";
import {ComponentsModule} from "../../components.module";

@NgModule({
  declarations: [
    LoginPageComponent,
    AuthComponent,
    RegistrationComponent,
    DemoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    InputNumberModule,
    RouterOutlet,
    LoginPageRoutingModule,
    CarouselModule,
    ButtonModule,
    CheckboxModule,
    ComponentsModule
  ],
  providers:[]
})
export class LoginPageModule { }
