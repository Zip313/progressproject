import {Component, OnDestroy} from '@angular/core';
import {AuthService} from "../../../../services/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {Subscription} from "rxjs";
import {UserService} from "../../../../services/user.service";

@Component({
  selector: 'app-registration',
  template: `
    <div class="wrapper">
      <form  #form="ngForm" (ngSubmit)="sendFormToRegistration(form)">
        <div class="regform">
          <div class="regform-item">
            <span >Email</span>
            <input type="text"
                   pInputText
                   autocomplete="email"
                   #email="ngModel"
                   [ngClass]="{'input-error': !email}"
                   ngModel
                   name="email"/>
          </div>
          <div class="regform-item">
            <span >Password</span>
            <input type="password"
                   pInputText
                   [ngClass]="{'input-error': !psw}"
                   #psw="ngModel"
                   ngModel
                   name="psw"/>
          </div>
          <div class="regform-item">
            <span >Подтверждение пароля</span>
            <input type="password"
                   pInputText
                   [ngClass]="{'input-error': !pswRepeat }"
                   #pswRepeat="ngModel"
                   ngModel
                   name="pswRepeat"/>
          </div>
        </div>
        <div class="">
          <p-button label="Зарегистрироваться"
                    styleClass="p-button-sm"
                    type="submit"
                    [disabled]="!email || !psw || !pswRepeat"
          >
          </p-button>
        </div>
        <div class="regform-item">
          <a routerLink="/login/auth"><< Вернуться к вооду логина и пароля</a>
        </div>
      </form>
    </div>
  `,
  styles: [`
    .wrapper{
      display: flex;
      flex-direction: column;
      width: 100%;
      padding: 1rem;
    }
    .regform{
      display: flex;
      flex-direction: column;
    }
    .regform-item{
      margin: .5rem ;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    }
    .input-error{
      border: 1px solid red !important;
    }
  `
  ]
})
export class RegistrationComponent implements OnDestroy{
  subscription?:Subscription;
  constructor(
    private authService:AuthService,
    private userService:UserService,
    private messageService: MessageService,
    private router: Router
  ) {  }

  async sendFormToRegistration(form: NgForm) {
    const user = form.value;
    if(!user.psw || !user.email || user.psw!=user.pswRepeat){
      this.messageService.add({
        severity:'error',
        summary:'error validation from',
        detail:`Поля формы заполнены не корректно`
      });
      return;
    }
    delete user['pswRepeat'];
    const res = this.authService.registration(user.email,user.psw);
    this.subscription = res.subscribe(
      auth=>{
     this.authService.setAuth(auth);
     form.reset();
      this.messageService.add({
        severity:'success',
        summary:`Успешно ${user.email} зарегистрирован`,
        detail:`Необходимо активировать аккаунт по ссылке направленной письмом на указанный Email`
      });
      this.router.navigateByUrl('auth');
    },
      error=>{
        this.messageService.add({
          severity:'error',
          summary:`Ошибка регистрации (${error.error.statusCode})`,
          detail:`${error.error.message}`
        });
      })
  }
  ngOnDestroy(): void {
    if(this.subscription) this.subscription.unsubscribe();
  }
}
