import { Component } from '@angular/core';

@Component({
  selector: 'app-login-page',
  template: `
    <div class="wrapper">
      <app-wrapper widthPercent="74">
        <app-demo></app-demo>
      </app-wrapper>
      <app-wrapper widthPercent="24">
        <router-outlet></router-outlet>
      </app-wrapper>
    </div>
  `,
  styles: [`
    .gallery{
      display: inline-block;
      margin: auto 1rem;
      padding: 1rem;
      width: 75%;
      height: 100%;
      overflow: hidden;
      border-radius: 5px;
      box-shadow: 5px 5px 5px 5px var(--white-color-opacity);
      background-color: var(--white-color);
    }
    .auth{
      display: inline-block;
      margin: auto 1rem;
      padding: 1rem;
      width: 25%;
      border-radius: 5px;
      box-shadow: 5px 5px 5px 5px var(--white-color-opacity);
      height: 100%;
      background-color: var(--white-color);
    }
  `
  ]
})
export class LoginPageComponent {}
