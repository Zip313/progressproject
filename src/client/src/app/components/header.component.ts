import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {UserService} from "../services/user.service";
import {Role} from "../enums/roles";

@Component({
  selector: 'app-header',
  template: `
    <div class="header">
      <div *ngIf="authService.isActivated">
        <p-menubar [model]="items" styleClass="header"></p-menubar>
      </div>
      <div (click)="handleClickLogin()" class="avatar" *ngIf="authService.isAuthenticated">
        <p-avatar label="{{(authService.userService.User?.email??'N')[0] | uppercase }}" styleClass="mr-2"
                  class="header-item" size="large"></p-avatar>
      </div>
    </div>
  `,
  styles: [
    `
      .header{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        line-height: 50px;
        background: var(--gradient-header);
        color: var(--white-color);
        padding: .5rem;
      }
     .header-item{
       display: inline-block;
       margin:auto 1rem;
       color: #2b2b3a;
      }
      p-menuitem{
       background: var(--gradient-header);
     }
      .avatar{
        display: inline-block;
        cursor: pointer;
        transition: all .1s ease-in-out;
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Old versions of Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
        user-select: none;
        &:hover{
          //translate:2px -2px;
          transform: scale(1.08);
          transition: all .3s ease-in-out;
        }
        &:active{
          transform: scale(0.95);
          transition: all .1s ease-in-out;
        }
      }
    `
  ]
})
export class HeaderComponent implements OnInit,AfterViewInit{

  items: MenuItem[]=[];
  constructor(
    private router:Router,
    public authService:AuthService,
    public userService:UserService,
    ) { }
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.items = this.initMenuItems();
  }
  initMenuItems(): MenuItem[] {
    console.log('initMenuItems')
    return [
      {
        label: 'Портфель',
        routerLink:['workspace']
      },
      {
        label: 'Отчеты',
        routerLink:['/reports']
      },
      {
        label: 'Администрирование',
        routerLink:['/admin'],
        //TODO: не успевает инициализироваться пользователь...
        visible: false//this.userService.User?.roles?.includes(Role.noRole)
      },
      {
        label: 'Настройки',
        routerLink:['/settings'],
        visible: true
      },

    ];
  }
  handleClickLogin() {
    this.router.navigate(['/login']);
  }
}
