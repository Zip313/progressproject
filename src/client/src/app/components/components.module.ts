import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WrapperComponent} from "./wrapper.component";
import { OrderItemComponent } from './pages/workspace-page/order-item.component';
import {BadgeModule} from "primeng/badge";

@NgModule({
  declarations: [
    WrapperComponent,
    OrderItemComponent,
  ],
  imports: [
    CommonModule,
    BadgeModule,
  ],
  exports: [WrapperComponent, OrderItemComponent]
})
export class ComponentsModule { }
