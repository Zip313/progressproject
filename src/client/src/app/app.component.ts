import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit,AfterViewInit{

  constructor(private authService:AuthService) {}

  ngOnInit(): void {
    if (this.authService.Token) this.authService.updateCurrentUser();
  }
  ngAfterViewInit(): void {

  }
  title = 'pias_ui';
}
