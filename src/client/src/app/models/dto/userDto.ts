import {Role} from "../../enums/roles";

export class UserDto {
    email: string;
    password:string;
    roles?:Role[];
    isActivated?:boolean;
}
