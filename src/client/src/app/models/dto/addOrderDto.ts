
export class AddOrderDto{
  public id?: number
  constructor(
    public shortName:string,
    public count:number,
    public ownDate:Date,
    public price:number  )
  {}
}
