export class summaryReportDto {
  isMy:boolean
  currentAmount: number
  userAmount: number
  currentAmountCommission: number
  currentAmountPure: number
  saleProfit: number
  saleProfitPercent: number
  isProfit: boolean
  dividendsValuePercent?: number
  dividendsAmount?: number
  dividendsAmountTax?: number
  dividendsAmountPure?: number
  secId: string;
  stockShortName: string;
  currentPrice: number;
  userPrice: number;
  count: number;
  dividendsRegistryCloseDate?: string;
  dividendsValue?: number;


}
