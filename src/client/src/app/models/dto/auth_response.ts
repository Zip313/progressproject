import {User} from "../User";

export class AuthResponse {
  accessToken:string;
  refreshToken:string;
  user:User;
}
