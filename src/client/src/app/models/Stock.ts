import {Dividend} from "./Dividend";

export class Stock{
  id: number;
  secId:string;
  shortName: string;
  prevPrice: number;
  currencyId: string;
  dividends:Dividend[];
  modifiedDate:Date;
  isIncrease:boolean;
  increaseValuePer:number
  constructor() {}
}
