import {Role} from "../enums/roles";

export class User{
  id?:number;
  email?: string;
  password?: string;
  roles:Role[];
  isActivated?:boolean;
}



