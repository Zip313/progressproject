import {Stock} from "./Stock";
import {User} from "./User";

export class Order{
  constructor(
    public id: number,
    public stock:Stock,
    public count:number,
    public ownDate:Date,
    public price:number,
    public currentAmount:number,
    public costAmount:number,
    public isIncrease:boolean,
    public increaseValue:number,
    public increaseValuePer:number
  ) {  }
}





