import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./guards/auth.guard";
import {AdminGuard} from "./guards/admin.guard";

const routes: Routes = [
  {
    path:'login',
    loadChildren:() => import('./components/pages/login-page/login-page.module').then(m=>m.LoginPageModule),
  },
  {
    path:'workspace',
    loadChildren:() => import('./components/pages/workspace-page/workspace-page.module').then(m=>m.WorkspacePageModule),
    canActivate:[AuthGuard],
  },
  {
    path:'admin',
    loadChildren:() => import('./components/pages/admin-page/admin-page.module').then(m=>m.AdminPageModule),
    canActivate:[AdminGuard],
  },
  {
    path:'settings',
    loadChildren:() => import('./components/pages/settings-page/settings-page.module').then(m=>m.SettingsPageModule),
    canActivate:[AuthGuard],
  },
  {
    path:'reports',
    loadChildren:() => import('./components/pages/reports-page/reports-page.module').then(m=>m.ReportsPageModule),
    canActivate:[AuthGuard],
  },
  {
    path:'**',
    redirectTo:'login'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
