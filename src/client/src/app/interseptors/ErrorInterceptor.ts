
import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  concat,
  concatMap,
  EMPTY, forkJoin,
  interval,
  map, mergeMap,
  Observable, of,
  retry,
  Subject,
  switchMap,
  tap,
  throwError
} from 'rxjs';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

/** Pass untouched request through to the next request handler. */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  private refreshTokenInProgress : boolean = false;
  constructor(
    private authService:AuthService,
    private router:Router
  ) {  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // let refreshTokenInProgress = false;
    //@ts-ignore
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse,restart)=> {
        console.log(error.status)
        if(error.status===401 && !this.refreshTokenInProgress) { //error.error.message === 'InvalidToken' &&
          this.refreshTokenInProgress = true;
          return concat(
            this.authService.refreshTokens().pipe(
            tap(async (auth)=>{
              await this.authService.setAuth(auth);
              const authReq = req.clone({
                  headers: req.headers.set('Authorization', `Bearer ${this.authService.Token}`)
              });
              return next.handle(authReq);
            },)))
          if (this.refreshTokenInProgress) return EMPTY
        }
        if(error.status===400 && this.refreshTokenInProgress){ //error.error.message === 'InvalidToken' &&
          console.log('error refresh token, logout2',error)
          this.authService.logout();
        }
        return throwError(error);
      })
    );
  }
}
