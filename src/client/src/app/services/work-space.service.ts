import { Injectable } from '@angular/core';
import {WorkSpaceRestService} from "./rest/work-space-rest.service";
import {map, Observable, Subject, take, tap} from "rxjs";
import {Order} from "../models/Order";
import {AddOrderDto} from "../models/dto/addOrderDto";
import {MessageService} from "primeng/api";
import {ShortStock, ShortStocks} from "../models/ShortStocks";

@Injectable({
  providedIn: 'root'
})
export class WorkSpaceService {
  public subjectCheckedOrder$: Subject<Order> = new Subject<Order>();
  private shortNames: ShortStock[] = [];

  public get ShortNames(): ShortStock[] {
    return this.shortNames
  }
  public Orders: Order[];
  public summaryText : string;
  public summaryText2() : string{
    let currentAmount: number = Math.round(( this.Orders.map(x=>x.count*x.stock.prevPrice).reduce((value,acc)=>value+acc,0))*100)/100;
    let usersAmount: number = Math.round(( this.Orders.map(x=>x.count*x.price).reduce((value,acc)=>value+acc,0))*100)/100;
    let profit = Math.round((currentAmount - usersAmount)*100)/100;
    let profitPercent = Math.round((profit/usersAmount)*100*100)/100

    return `Текущая стоимость портфеля: ${currentAmount}руб. Себестоимость: ${usersAmount}руб. Профит: ${profit}руб. (${profitPercent}%) `;
  };

  checkedOrder?: Order;
  constructor(
    private readonly workSpaceRestService: WorkSpaceRestService,
    private readonly messageService: MessageService,
  ) {
  }
  handleSummaryText(orders:Order[]){

  }
  getOrders$(): Observable<Order[]> {
    return this.workSpaceRestService.getOrders$()
      .pipe(
        tap(orders => {
          this.Orders = orders.sort((a, b) => {
            if (a.stock.shortName < b.stock.shortName) {
              return -1;
            }
            if (a.stock.shortName > b.stock.shortName) {
              return 1;
            }
            return 0;
          });
          this.handleSummaryText(this.Orders);
          if (this.checkedOrder) {
            this.checkedOrder = this.Orders.find(order => order.id === this.checkedOrder?.id);
          }
        })
      )
  }
  getShortNames$(): Observable<any> {
    return this.workSpaceRestService.getStocksShortNames$().pipe(tap(sn => this.shortNames = sn.shortStocks))
  }
  addOrder$(newOrder: AddOrderDto) {
    return this.workSpaceRestService.addOrder$(newOrder)
      .pipe(
        tap(order => this.updateOrderInStore(order))
      )
  }
  deleteOrder$(order: Order) {

    return this.workSpaceRestService.deleteOrder$({orderId : order.id})
      .pipe(
        tap(x => this.deleteOrderFromStore(order))
      )
  }
  updateOrder$(updateOrder: AddOrderDto) {
    return this.workSpaceRestService.updateOrder$(updateOrder)
      .pipe(
        tap(order => {
          this.updateOrderInStore(order);
        })
      )
  }
  private addOrderToStore(order: Order) {
    this.Orders.push(order);
    this.Orders = this.Orders.sort((a, b) => {
      if (a.stock.shortName < b.stock.shortName) {
        return -1;
      }
      if (a.stock.shortName > b.stock.shortName) {
        return 1;
      }
      return 0;
    })
  }
  deleteOrderFromStore(order: Order) {
    this.Orders = this.Orders.filter(o => o.id != order.id);
  }
  updateCheckedOrderInStore() {
    this.checkedOrder = this.Orders.find(o => o.id === this.checkedOrder?.id)
  }
  updateOrderInStore(order: Order) {
    this.deleteOrderFromStore(order);
    this.addOrderToStore(order)
    this.updateCheckedOrderInStore();
  }
  getOrdersDemo() {
    return this.workSpaceRestService.getOrdersDemo$()
      .pipe(
        tap(orders=>this.Orders=orders)
      )
  }
}
