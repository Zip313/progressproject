import { Injectable, Injector } from '@angular/core';
import {AuthRestService} from "./rest/auth-rest.service";
import {Observable} from "rxjs";
import {AuthResponse} from "../models/dto/auth_response";
import {UserService} from "./user.service";
import {Router} from "@angular/router";
import {Role} from "../enums/roles";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token?:string;
  public get Token(){ return this.token;}
  public set Token(_token){
    this.token=_token;
    if(_token)
      localStorage.setItem('token',_token);
    else
      localStorage.removeItem('token')
  }
  private refreshToken?:string;
  public get RefreshToken(){ return this.refreshToken;}
  public set RefreshToken(_token){
    this.refreshToken=_token;
    if(_token)
      localStorage.setItem('refresh_token',_token);
    else
      localStorage.removeItem('refresh_token')
  }
  public isAuthenticated:boolean=false;
  public isDemo:boolean=false;
  public get isActivated(){return this.userService.User?.isActivated};
  constructor(
    private authRestService:AuthRestService,
    public userService:UserService,
    private router:Router,
    private injector: Injector
  ) {
    const token = localStorage.getItem('token');
    const refresh_token = localStorage.getItem('refresh_token');
    if(token && refresh_token) {
      this.token=token;
      this.refreshToken = refresh_token;
      this.isAuthenticated = true;
    }
  }
  updateCurrentUser(){
    this.authRestService.getCurrentUser().subscribe(user=>{
      this.userService.User = user;
    },
      error => '');
  }
  registration(email:string,password:string):Observable<AuthResponse>{
    return this.authRestService.signUp(email,password)
  }
  authenticate(email:string,password:string):Observable<AuthResponse>{
    return this.authRestService.signIn(email,password)
  }
  async setAuth(authResponse:AuthResponse){
    console.log(authResponse.user,authResponse)
    this.isAuthenticated=true;
    this.isDemo=false;
    this.Token = authResponse.accessToken;
    this.RefreshToken = authResponse.refreshToken;
    this.userService.User = authResponse.user;
    console.log(this.userService.User)
  }
  async logout(){
    this.isAuthenticated=false;
    this.Token = undefined;
    this.userService.User = undefined;
    this.router.navigate(["/login"]);
  }
  refreshTokens():Observable<AuthResponse> {
    if (!this.RefreshToken) throw new Error('invalid refresh token');
    return this.authRestService.refreshTokens(this.RefreshToken);
  }
  setDemoAuth() {
    this.isAuthenticated=true;
    this.isDemo=true;
    this.userService.User = {id:0,email:'DemoUser',roles:[Role.User],isActivated:true};
  }
}
