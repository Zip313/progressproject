import {ReportsRestService} from "./rest/reports-rest.service";
import {Injectable} from "@angular/core";

@Injectable()
export class ReportsService {
  constructor(
    private readonly reportsRestService:ReportsRestService,
  ) { }
  getSummary$() {
    return this.reportsRestService.getSummary$();
  }
  getSummaryDemo$() {
    return this.reportsRestService.getSummaryDemo$();
  }
}
