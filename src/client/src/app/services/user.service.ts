import { Injectable } from '@angular/core';
import {User} from "../models/User";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user?:User;
  public get User(){return this.user};
  public set User(_user){this.user = _user};
  constructor() { }
}
