import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {AuthResponse} from "../../models/dto/auth_response";
import {HttpClient} from "@angular/common/http";
import {UserDto} from "../../models/dto/userDto";
import {User} from "../../models/User";
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthRestService {

  base_api_url : string = environment.apiUrl; //`api/`
  constructor(
    private readonly http:HttpClient,
  ) { }
  signUp(email:string, password:string):Observable<AuthResponse>{
    const userDto:UserDto = {email:email,password:password};
    return this.http.post<AuthResponse>(`${this.base_api_url}auth/signup`,userDto)
  }
  signIn(email:string, password:string):Observable<AuthResponse>{
    const userDto:UserDto = {email:email,password:password};
    return this.http.post<AuthResponse>(`${this.base_api_url}auth/SignIn`,userDto)
  }
  getCurrentUser():Observable<User> {
    return this.http.get<User>(`${this.base_api_url}auth/getCurrentUser`);
  }
  refreshTokens(refresh_token: string):Observable<AuthResponse> {
    return this.http.post<AuthResponse>(`${this.base_api_url}auth/RefreshToken`,{RefreshToken:refresh_token})
  }
}
