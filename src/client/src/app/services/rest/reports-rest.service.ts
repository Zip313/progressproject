import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {summaryReportDto} from "../../models/dto/summaryReportDto";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable()
export class ReportsRestService {
  base_api_url : string = environment.apiUrl;
  constructor(
    private readonly http:HttpClient
  ) {
  }
  getSummary$():Observable<summaryReportDto[]> {
    return this.http.get<summaryReportDto[]>(`${this.base_api_url}reports/getSummary`);
  }

  getSummaryDemo$():Observable<summaryReportDto[]> {
    return this.http.get<summaryReportDto[]>('../../../../../assets/demo-summary.json');
  }
}
