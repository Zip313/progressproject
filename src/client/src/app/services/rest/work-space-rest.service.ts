import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Order} from "../../models/Order";
import {AddOrderDto} from "../../models/dto/addOrderDto";
import {DeleteOrderDto} from "../../models/dto/deleteOrderDto";
import {QueryDto} from "../../models/dto/queryDto";
import {environment} from "../../../environments/environment";
import {ShortStocks} from "../../models/ShortStocks";

@Injectable({
  providedIn: 'root'
})
export class WorkSpaceRestService {
  base_api_url : string = environment.apiUrl;
  constructor(
    private readonly http:HttpClient,
  ) { }
  getOrders$(): Observable<Order[]> {
    return this.http.get<Order[]>(`${this.base_api_url}orders/`);
  }
  getStocksShortNames$(): Observable<ShortStocks> {
    return this.http.get<ShortStocks>(`${this.base_api_url}stocks/getShortNames`);
  }
  addOrder$(newOrder: AddOrderDto):Observable<Order> {
    console.log(newOrder,'rest')
    return this.http.post<Order>(`${this.base_api_url}orders/`,newOrder);
  }
  deleteOrder$(deleteOrder: DeleteOrderDto):Observable<QueryDto> {
    return this.http.delete<QueryDto>(`${this.base_api_url}orders/`,{body: deleteOrder});
  }
  updateOrder$(updateOrder: AddOrderDto) {
    return this.http.put<Order>(`${this.base_api_url}orders/`,updateOrder);
  }
  getOrdersDemo$(): Observable<Order[]> {
    return this.http.get<Order[]>('../../../../../assets/demo-orders.json');
  }
}
