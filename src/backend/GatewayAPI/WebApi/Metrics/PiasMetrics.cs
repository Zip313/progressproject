﻿using System.Diagnostics.Metrics;

namespace WebApi.Metrics
{
    public class PiasMetrics : IDisposable
    {
        internal const string MeterName = "MeterUsing.PIAS";
        private readonly Meter _meter;
        private double requestsPerSecond;
        private Dictionary<string, int> requests = new Dictionary<string, int>();
        private readonly ObservableGauge<double> requestsGauge;


        public PiasMetrics()
        {
            this._meter = new Meter(MeterName);
            this.requestsGauge = _meter.CreateObservableGauge<double>("req", GetRequests, "perSecond", "");
        }

        public void AddRequestsPerSecond()
        {
            string currentTime = DateTime.Now.ToShortTimeString();
            if (this.requests.TryGetValue(currentTime, out int requests))
            {
                this.requests[currentTime] += 1;
            }
            else
            {
                this.requests.Add(currentTime, 1);
            }

            this.requestsPerSecond = this.requests.Average(x => x.Value);
        }

        public void AddRequest()
        {
            this.Dispose();
        }

        private Measurement<double> GetRequests()
        {
            var requestsTotal = this.requests.Count;
            var result = requestsPerSecond;
            requestsPerSecond = 0;
            requests.Clear();
            return new Measurement<double>(result);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
