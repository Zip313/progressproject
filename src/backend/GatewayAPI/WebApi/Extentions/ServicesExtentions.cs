﻿using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using WebApi.Metrics;

namespace WebApi.Extentions
{
    public static class ServicesExtentions
    {
        public static IServiceCollection AddPresentation(this IServiceCollection services)
        {
            services.AddMetrics();
            services.AddLokiLogging();

            return services;
        }

        private static IServiceCollection AddMetrics(this IServiceCollection services)
        {
            services.AddSingleton<PiasMetrics>();
            services.AddOpenTelemetry()
                .ConfigureResource(res => res.AddService(PiasMetrics.MeterName, serviceInstanceId: Environment.MachineName))
                .WithMetrics(builder => builder
                    .AddMeter(PiasMetrics.MeterName)
                    .AddHttpClientInstrumentation()
                    .AddRuntimeInstrumentation()
                    .AddProcessInstrumentation()
                    .AddPrometheusExporter());
            return services;
        }
        private static IServiceCollection AddLokiLogging(this IServiceCollection services)
        {
            services.AddLogging(builder =>
            {
                builder.AddLoki(options =>
                {
                    options.Http = new LokiLoggingProvider.Options.HttpOptions()
                    {
                        Address = "http://pias-loki:3100"
                    };
                    options.Client = LokiLoggingProvider.Options.PushClient.Http;
                    options.StaticLabels.JobName = "PIAS_GatewayAPI_Service";
                    options.Formatter = LokiLoggingProvider.Options.Formatter.Json;
                });
                builder.AddConsole();
            });

            return services;
        }
    }
}
