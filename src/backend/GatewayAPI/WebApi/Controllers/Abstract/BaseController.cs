﻿using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApi.Metrics;

namespace WebApi.Controllers.Abstract
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        private IMediator _mediator;
        private IMapper _mapper;
        protected IMediator Mediator =>
            _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        protected IMapper Mapper=>
            _mapper ??= HttpContext.RequestServices.GetService<IMapper>();
        protected int UserId =>
            !User.Identity.IsAuthenticated
            ? -1
            : int.Parse(User.FindFirst("user_id")?.Value ?? "-1");
        protected string Roles =>
            !User.Identity.IsAuthenticated
            ? "-1"
            : User.FindFirst("roles")?.Value ?? "-1";
        public BaseController(PiasMetrics metrics)
        {
            metrics.AddRequestsPerSecond();
        }
    }
}
