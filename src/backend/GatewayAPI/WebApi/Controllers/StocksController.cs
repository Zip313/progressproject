﻿using Application.Requests.Stocks.Queries.GetShortNames;
using Application.ViewModels.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Controllers.Abstract;
using WebApi.Metrics;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StocksController : BaseController
    {
        public StocksController(PiasMetrics metrics) : base(metrics)
        {            
        }

        [HttpGet("GetShortNames")]
        public async Task<ActionResult<IEnumerable<OrderVm>>> GetShortNames()
        {
            GetShortNamesQuery request = new GetShortNamesQuery();
            return Ok(await Mediator.Send(request));
        }
    }
}
