﻿using Application.Requests.Orders.Commands.AddOrder;
using Application.Requests.Orders.Commands.DeleteOrder;
using Application.Requests.Orders.Commands.UpdateOrder;
using Application.Requests.Orders.Queries.GetOrdersByUser;
using Application.ViewModels.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Controllers.Abstract;
using WebApi.Metrics;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrdersController : BaseController
    {
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(PiasMetrics metrics, ILogger<OrdersController> logger) : base(metrics)
        {
            this._logger = logger;
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<OrderVm>>> GetOrders()
        {
            try
            {
                _logger.LogInformation($"GetOrders попытка получения портфеля {this.UserId}");
                GetOrdersByUserIdQuery request = new GetOrdersByUserIdQuery()
                {
                    UserId = this.UserId,
                };
                var result = await Mediator.Send(request);
                _logger.LogInformation($"GetOrders портфель получен {this.UserId}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetOrders Неудачная попытка получения портфеля {this.UserId}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        public async Task<ActionResult<IEnumerable<OrderVm>>> AddOrder([FromBody] AddOrderCommand command)
        {
            try
            {
                _logger.LogInformation($"AddOrder попытка добавления ордера в портфель {this.UserId}, {command.ShortName}");
                command.UserId = this.UserId;
                var result = await Mediator.Send(command);
                _logger.LogInformation($"AddOrder ордер добавлен {this.UserId}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddOrder Неудачная попытка добавления ордера в портфель {this.UserId}, {command.ShortName}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        [HttpPut()]
        public async Task<ActionResult<IEnumerable<OrderVm>>> UpdateOrder([FromBody] UpdateOrderCommand command)
        {
            try
            {
                _logger.LogInformation($"UpdateOrder попытка обновления ордера {this.UserId}, {command.ShortName}");
                var result = await Mediator.Send(command);
                _logger.LogInformation($"UpdateOrder ордер обновлен {this.UserId}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateOrder Неудачная попытка обновления ордера {this.UserId}, {command.ShortName}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete()]
        public async Task<IActionResult> DeleteOrder([FromBody] DeleteOrderCommand command)
        {
            try
            {
                _logger.LogInformation($"DeleteOrder попытка удаления ордера userId:{this.UserId}, orderId:{command.OrderId}");
                await Mediator.Send(command);
                _logger.LogInformation($"DeleteOrder ордер удален userId:{this.UserId}, orderId:{command.OrderId}");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteOrder Неудачная попытка удаления ордера userId:{this.UserId}, orderId:{command.OrderId}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }
    }
}
