﻿using Application.Requests.Auth.Commands.ActivateUser;
using Application.Requests.Auth.Commands.RefreshToken;
using Application.Requests.Auth.Commands.SignIn;
using Application.Requests.Auth.Commands.SignUp;
using Application.Requests.Auth.Queries.GetUser;
using Application.ViewModels.Auth;
using Application.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using WebApi.Controllers.Abstract;
using WebApi.Metrics;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {
        private readonly string host;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IConfiguration configuration, ILogger<AuthController> logger, PiasMetrics metrics) : base(metrics)
        {
            host = Environment.GetEnvironmentVariable("HOST") ?? configuration["Host"];            
            _logger = logger;
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        ///     POST /api/auth/signin
        ///     {
        ///        "Email" : "user@example.com", 
        ///        "Password" : "12345678" 
        ///     }
        /// Пароль не менне 8 символов
        /// </remarks>
        /// <param name="command">Credentials DTO</param>
        /// <returns>Данные аутентификации</returns>
        [HttpPost]
        [Route("SignIn")]
        public async Task<ActionResult<AuthVm>> SignInByEmail([FromBody] SignInCommand command)
        {
            try
            {
                _logger.LogInformation($"SignInByEmail попытка входа {command.Email}");
                var result = await Mediator.Send(command);
                _logger.LogInformation($"Успешная попытка входа {command.Email}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"SignInByEmail Неудачная попытка входа {command.Email}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Данные аутентификации</returns>
        [HttpPost("SignUp")]
        public async Task<ActionResult<AuthVm>> SignUp([FromBody] SignUpCommand command)
        {
            try
            {
                _logger.LogInformation($"SignUp попытка регистрации {command.Email}");
                var result = await Mediator.Send(command);
                _logger.LogInformation($"Успешная регистрация {command.Email}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"SignUp Неудачная попытка регистрации {command.Email}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }
               
        /// <summary>
        /// Активация пользователя по ссылке отправленной при регистрации
        /// </summary>
        /// <param name="activationLink"></param>
        /// <returns>HTML</returns>
        [HttpGet("Activate/{activationLink}")]
        public async Task<IActionResult> Activate([FromRoute] string activationLink)
        {
            try
            {
                ActivateUserCommand command = new ActivateUserCommand() { ActivationCode = activationLink };
                await Mediator.Send(command);

                //TODO: переделать ссылку на приложение
                string content = $"""<a href = "{host}" > Активация успешна, перейти в приложение </a>""";
                _logger.LogInformation($"Активация усешно пройдена {activationLink}");
                return new ContentResult
                {
                    ContentType = "text/html;charset=utf-8",
                    Content = content,
                    StatusCode = 200
                };
            } 
            catch (Exception ex)
            {
                _logger.LogError($"Активация не пройдена {activationLink}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить текущего пользователя
        /// </summary>
        /// <returns>Представление пользователя</returns>
        [Authorize]
        [HttpGet("GetCurrentUser")]
        public async Task<ActionResult<UserVm>> GetCurrentUser()
        {
            try
            {
                GetUserQuery request = new GetUserQuery() { UserId = this.UserId };
                _logger.LogInformation($"GetCurrentUser данные успешно получены {this.UserId}");
                return Ok(await Mediator.Send(request));
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetCurrentUser Не удается получить данные по пользователю: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Обновление токенов при истечении срока AccessToken
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Данные аутентификации</returns>
        [HttpPost]
        [Route("RefreshToken")]
        public async Task<ActionResult<AuthVm>> RefreshToken([FromBody] RefreshTokenCommand request)
        {
            try
            {
                _logger.LogInformation($"RefreshToken попытка обновления токена {request.RefreshToken}");
                var result = await Mediator.Send(request);
                _logger.LogInformation($"RefreshToken токен успешно обновлен {request.RefreshToken}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"RefreshToken Неудачная попытка обновления токена {request.RefreshToken}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

    }
}
