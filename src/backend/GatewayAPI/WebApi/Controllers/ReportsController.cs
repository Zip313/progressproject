﻿using Application.Requests.Orders.Queries.GetOrdersByUser;
using Application.Requests.Reports.Queries.GetSummaryReport;
using Application.ViewModels.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Controllers.Abstract;
using WebApi.Metrics;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportsController : BaseController
    {
        public ReportsController(PiasMetrics metrics) : base(metrics)
        {            
        }

        [HttpGet("getSummary")]
        public async Task<ActionResult<IEnumerable<OrderVm>>> GetSummaryReport()
        {
            GetSummaryReportQuery query = new GetSummaryReportQuery()
            {
                UserId = this.UserId,
            };
            return Ok(await Mediator.Send(query));
        }
    }
}
