﻿namespace WebApi.Middlewares.Extentions
{
    public static class CustomExceptioHandlerMiddlewareExtentions
    {
        public static IApplicationBuilder UseCustomMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<CustomExceptionHandlerMiddleware>();
        }
    }
}
