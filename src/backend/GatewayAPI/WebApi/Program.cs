using MapsterMapper;
using Application;
using Persistence;
using WebApi.Middlewares.Extentions;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.HttpOverrides;
using WebApi.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Metrics;
using WebApi.Extentions;

namespace WebApi
{
    public class Program
    {

        public static void Main(string[] args)
        {

            const string _CorsPolicy = "CorsPolicy";
            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddHttpClient();
            builder.Services.AddHttpContextAccessor();

            // Add services to the container.
            builder.Services.AddTransient<IMapper, Mapper>();
            builder.Services.AddApplication(builder.Configuration);
            builder.Services.AddPersistance(builder.Configuration);
            builder.Services.AddPresentation();

            builder.Services.AddControllers()
                .AddNewtonsoftJson();


            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                var basePath = System.AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "APIGateway.xml");
                options.IncludeXmlComments(xmlPath);

                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Progress Web API",
                    Description = "Personal Investment accounting service API Gateway",
                });

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"Enter 'Bearer token'",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                            Array.Empty<string>()
                    }
                    });
            });

            builder.Services.AddCors(options =>
            {

                options.AddPolicy(_CorsPolicy,
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    //.AllowCredentials()
                    //.WithOrigins(host,"http://localhost", "http://client")
                        );

            });


            var app = builder.Build();
            app.UseCustomMiddleware();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                    ForwardedHeaders.XForwardedProto
            });
            app.UseCors(_CorsPolicy);

            using (var scope = app.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                try
                {
                    var ctx = serviceProvider.GetRequiredService<Persistence.AppContext>();
                    DbInitializer.Initialize(ctx);
                }
                catch (Exception ex) { }
            }
            // Configure the HTTP request pipeline.
            if (true) //app.Environment.IsDevelopment() временно
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.UseOpenTelemetryPrometheusScrapingEndpoint();

            app.Run();
        }
    }
}