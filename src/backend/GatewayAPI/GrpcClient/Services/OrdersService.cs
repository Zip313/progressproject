﻿using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using GrpcClient.Interfaces;

namespace GrpcClient.Services
{
    /// <inheritdoc/>
    public class OrdersService : IOrdersService
    {
        private readonly Orders.OrdersClient client;

        public OrdersService(string grpcServerAddress)
        {
            var channel = GrpcChannel.ForAddress(grpcServerAddress);
            client = new Orders.OrdersClient(channel);
        }
        
        /// <inheritdoc/>
        public async Task<OrdersResponse> GetOrdersByUserId(GetOrdersByUserIdRequest request)
        {
            var response = await client.GetOrdersByUserIdAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;

            
        }

        /// <inheritdoc/>
        public async Task<OrdersResponse> AddOrderAsync(AddOrderRequest request)
        {
            var response = await client.AddOrderAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;

            
        }

        /// <inheritdoc/>
        public async Task<OrdersResponse> UpdateOrderAsync(UpdateOrderRequest request)
        {
            var response = await client.UpdateOrderAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;

            
        }

        /// <inheritdoc/>
        public async Task<OrdersResponse> DeleteOrderAsync(DeleteOrderRequest request)
        {
            var response = await client.DeleteOrderAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;

            
        }

        /// <inheritdoc/>
        public async Task<SummaryReportResponse> GetSummaryReportAsync(GetSummaryRequest request)
        {
            var response = await client.GetSummaryAsync(request);
            return response;
        }

        /// <inheritdoc/>
        public async Task<ShortStocksResponse> GetStocksShortNames()
        {
            var response = await client.GetStocksShortNamesAsync(new EmptyDto());           
            return response;
        }

        
    }
}
