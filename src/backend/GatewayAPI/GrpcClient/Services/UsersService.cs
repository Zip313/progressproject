﻿using Grpc.Net.Client;
using GrpcClient.Interfaces;

namespace GrpcClient.Services
{
    public class UsersService : IUsersService
    {
        private readonly Users.UsersClient client;
        public UsersService(string grpcServerAddress)
        {
            var channel = GrpcChannel.ForAddress(grpcServerAddress);
            client = new Users.UsersClient(channel);
        }
        public async Task<UserResponse> GetUserByCredentialsAsync(UserCredentials request)
        {
            var response = await client.GetUserByCredentialsAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;
        }

        public async Task<UserResponse> GetUserByIdAsync(UserRequestById request)
        {
            var response = await client.GetUserByIdAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;
        }

        public async Task<UserResponse> AddUserAsync(UserCredentials request)
        {
            var response = await client.AddUserAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;
        }

        public async Task<ActivationResponse> ActivateUserAsync(ActivateRequest request)
        {
            var response = await client.ActivateUserAsync(request);
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response;
        }
        
    }
}
