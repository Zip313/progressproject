﻿namespace GrpcClient.Interfaces
{
    public interface IUsersService
    {
        /// <summary>
        /// Получение пользователя по Email и Password
        /// </summary>
        /// <param name="request">Пользовательские данные</param>
        /// <returns>Ответ с пользователем</returns>
        Task<UserResponse> GetUserByCredentialsAsync(UserCredentials request);

        /// <summary>
        /// Получение пользователя по id
        /// </summary>
        /// <param name="request">Запрос для получения пользователя по email и паролю.</param>
        /// <returns>Ответ с пользователем</returns>
        Task<UserResponse> GetUserByIdAsync(UserRequestById request);

        /// <summary>
        /// Добавление пользователя 
        /// </summary>
        /// <param name="request">Пользовательские данные</param>
        /// <returns>Ответ с пользователем</returns>
        Task<UserResponse> AddUserAsync(UserCredentials request);

        /// <summary>
        /// Активация пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Ответ активации пользователя</returns>
        Task<ActivationResponse> ActivateUserAsync(ActivateRequest request);
    }
}
