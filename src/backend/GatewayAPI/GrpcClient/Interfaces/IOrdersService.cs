﻿namespace GrpcClient.Interfaces
{
    /// <summary>
    /// Сервис для работы с акциями и ордерами
    /// </summary>
    public interface IOrdersService
    {
        /// <summary>
        /// Получение ордеров по идентификатору пользователя.
        /// </summary>
        /// <param name="getByUserId">Запрос для получения ордеров по идентификатору пользователя.</param>
        /// <returns>Контейнер с ордерами.</returns>
        Task<OrdersResponse> GetOrdersByUserId(GetOrdersByUserIdRequest getByUserId);

        /// <summary>
        /// Добавить ордер.
        /// </summary>
        /// <param name="request">Запрос на добавление ордера.</param>
        /// <returns>Контейнер с ордерами.</returns>
        Task<OrdersResponse> AddOrderAsync(AddOrderRequest request);

        /// <summary>
        /// Обновить ордер.
        /// </summary>
        /// <param name="request">Запрос на обновление ордера.</param>
        /// <returns>Контейнер с ордерами.</returns>
        Task<OrdersResponse> UpdateOrderAsync(UpdateOrderRequest request);

        /// <summary>
        /// Удалить ордер.
        /// </summary>
        /// <param name="request">Запрос на удаление ордера.</param>
        /// <returns>Контейнер с ордерами.</returns>
        Task<OrdersResponse> DeleteOrderAsync(DeleteOrderRequest request);

        /// <summary>
        /// Получить данные суммарного отчета.
        /// </summary>
        /// <param name="request">Запрос суммарного отчета</param>
        /// <returns>Данные для суммарного отчета.</returns>
        Task<SummaryReportResponse> GetSummaryReportAsync(GetSummaryRequest request);

        /// <summary>
        /// Получить краткие наименования всех акций на бирже.
        /// </summary>
        /// <returns>Краткие наименования акций</returns>
        Task<ShortStocksResponse> GetStocksShortNames();
    }
}
