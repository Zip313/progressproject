﻿using Application.Common.JWT;
using Application.Interfaces;
using Application.ViewModels.Auth;
using Application.ViewModels.User;
using Domain;
using Mapster;
using Microsoft.AspNetCore.Http;

namespace Application.Common.Services
{
    public class AuthService : IAuthService
    {
        private readonly IContext _dbContext;
        private readonly ITokenService _tokenService;
        private readonly IHttpContextAccessor _contextAccessor;
        public AuthService(IContext context, ITokenService tokenService, IHttpContextAccessor httpContextAccessor )
        {
            _dbContext = context;
            _tokenService = tokenService;
            _contextAccessor = httpContextAccessor;
        }

        public async Task<AuthVm> GetAuthAsync(User user, CancellationToken cancellationToken, DateTime? signInDate = null)
        {
            //Создаем токены
            string accessToken = _tokenService.GenerateJWT(user, TokenType.AccessToken);
            string refreshToken = _tokenService.GenerateJWT(user, TokenType.RefreshToken);

            //Записываем информацию о получении refreshToken в БД
            Auth auth = new Auth()
            {
                RefreshToken = refreshToken,
                RefreshTokenDate = DateTime.UtcNow,
                SignInDate = signInDate.HasValue ? signInDate.Value : DateTime.UtcNow,
                UserId = user.Id,
                AddressIP = _contextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString() ?? ""
            };
            var ip = _contextAccessor?.HttpContext?.Connection?.RemoteIpAddress;
            var authToDelete = _dbContext.Auth.Where(a => a.UserId == auth.UserId);
            _dbContext.Auth.RemoveRange(authToDelete);
            _dbContext.Auth.Add(auth);
            await _dbContext.SaveChangesAsync(cancellationToken);

            //Подготавливаем ответ
            AuthVm authVm = new AuthVm()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                User = user.Adapt<UserVm>(),
            };

            return authVm;
        }
    }
}
