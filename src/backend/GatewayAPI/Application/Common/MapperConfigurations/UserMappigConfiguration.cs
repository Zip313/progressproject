﻿using Application.ViewModels.Dividend;
using Domain;
using GrpcClient;
using Mapster;

namespace Application.Common.MapperConfigurations
{
    public class UserMappigConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<UserDto, User>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Email, src => src.Email)
                .Map(dest => dest.Roles, src => src.Roles)
                .Map(dest => dest.IsActivated, src => src.IsActivated);

            config.NewConfig<UserResponse, User>()
                .Map(dest => dest, src => src.User);
        }
    }
}
