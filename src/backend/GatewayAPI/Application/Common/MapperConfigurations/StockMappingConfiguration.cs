﻿using Application.ViewModels.Dividend;
using Application.ViewModels.Stock;
using Domain;
using GrpcClient;
using Mapster;

namespace Application.Common.MapperConfigurations
{
    public class StockMappingConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Stock, StockVm>()
                .Map(dest => dest.Dividends, src => src.Dividends.Adapt<List<DividendVm>>());

            config.NewConfig<ShortStockDto, ShortStokVm>();

            config.NewConfig<ShortStocksResponse, ShortStocksVm>()
                .Map(dest => dest.ShortStocks, src => src.ShortStocks.ToList().ConvertAll(x=>x.Adapt<ShortStokVm>()));


        }
    }
}
