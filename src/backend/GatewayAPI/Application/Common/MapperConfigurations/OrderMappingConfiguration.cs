﻿using Application.Requests.Orders.Commands.AddOrder;
using Application.Requests.Orders.Commands.UpdateOrder;
using Application.Requests.Orders.Queries.GetOrdersByUser;
using Application.ViewModels.Dividend;
using Application.ViewModels.Order;
using Application.ViewModels.Stock;
using Domain;
using Google.Protobuf.WellKnownTypes;
using GrpcClient;
using Mapster;

namespace Application.Common.MapperConfigurations
{
    public class OrderMappingConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Order, OrderVm>()
                .Map(dest => dest.Stock, src => src.Stock.Adapt<StockVm>());

            config.NewConfig<AddOrderCommand, AddOrderRequest>()
                .Map(dest => dest.StockShortName, src => src.ShortName)
                .Map(dest => dest.OwnDate, src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.OwnDate, DateTimeKind.Utc)));
            
            config.NewConfig<UpdateOrderCommand, UpdateOrderRequest>()
                .Map(dest => dest.OrderId, src => src.Id)
                .Map(dest => dest.StockShortName, src => src.ShortName)
                .Map(dest => dest.OwnDate, src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.OwnDate, DateTimeKind.Utc)));
            
            config.NewConfig<GetOrdersByUserIdQuery, GetOrdersByUserIdRequest>();

            config.NewConfig<OrderDto, OrderVm>()
                .Map(dest => dest.Id, src => src.OrderId)
                .Map(dest => dest.OwnDate, src => src.OwnDate.ToDateTime())
                .Map(dest => dest.Stock, src => src.Stock.Adapt<StockVm>());

            config.NewConfig<StockDto, StockVm>()
                .Map(dest => dest.Id, src => src.StockId)
                .Map(dest => dest.CurrencyId, src => src.CurrencyId)
                .Map(dest => dest.IncreaseValuePer, src => src.IncreaseValuePercent)
                .Map(dest => dest.IsIncrease, src => src.IsIncrease)
                .Map(dest => dest.ModifiedDate, src => src.ModifiedDate.ToDateTime())
                .Map(dest => dest.PrevPrice, src => src.PrevPrice)
                .Map(dest => dest.SecId, src => src.SecId)
                .Map(dest => dest.ShortName, src => src.ShortName)
                .Map(dest => dest.Dividends, src => src.Dividends.ToList().ConvertAll(d=>d.Adapt<DividendVm>()));

            config.NewConfig<DividendDto, DividendVm>()
                .Map(dest => dest.RegistryCloseDate, src => src.RegistryCloseDate.ToDateTime());

            //config.NewConfig<SummaryReportDto, SummaryRepoprtVm>()
            //    .Map(dest => dest., src => src.SecId);




        }
    }
}
