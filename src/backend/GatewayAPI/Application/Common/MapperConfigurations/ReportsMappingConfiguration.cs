﻿using Application.ViewModels.Reports;
using GrpcClient;
using Mapster;

namespace Application.Common.MapperConfigurations
{
    public class ReportsMappingConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<SummaryReportDto, SummaryReportVm>()
                .Map(dest => dest.DividendsRegistryCloseDate, src => DateTime.Now); //src.DividendsRegistryCloseDate.ToDateTime()
        }
    }
}
