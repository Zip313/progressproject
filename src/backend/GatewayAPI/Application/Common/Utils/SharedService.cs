﻿using Application.Interfaces;
using Application.ViewModels.Auth;
using Application.ViewModels.User;
using Domain;
using Mapster;

namespace Application.Common.Utils
{
    public static class SharedService
    {
        public async static Task CheckAttemptsLogin(int userId, IContext _dbContext, CancellationToken cancellationToken)
        {
            throw new UnauthorizedAccessException($"Пользователь заблокирован");
        }
    }
}
