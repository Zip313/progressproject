﻿namespace Application.Common.Utils
{
    internal static class ValidatorUtils
    {
        public static bool IsPhoneValid(string phone)
        {
            return !(!phone.StartsWith("9")
            || !phone.Substring(1).All(c => Char.IsDigit(c)))
            || phone.Length != 10;
        }

        public static bool IsDigit(string str)
        {
            return str.Substring(1).All(c => Char.IsDigit(c));
        }
    }
}
