﻿using Domain;
using Application.Common.Options;
using Application.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Application.Common.JWT
{
    public enum TokenType { AccessToken, RefreshToken };
    public class TokenService : ITokenService
{
        public AuthOptions AuthOptions { get; }
        public TokenService(AuthOptions authOptions)
        {
            AuthOptions = authOptions;
        }
        public string GenerateJWT(User user, TokenType tokenType )
        {
            var securityKey = AuthOptions.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Name,user.Email),
                new Claim("user_id",user.Id.ToString()),
                new Claim("roles", user.Roles.ToString())
            };

            DateTime expiresTime;
            switch (tokenType)
            {
                case TokenType.AccessToken:
                    expiresTime = DateTime.Now.AddMinutes(AuthOptions.AccessTokenLifeTimeInMinutes); break;
                case TokenType.RefreshToken:
                    expiresTime = DateTime.Now.AddDays(AuthOptions.RefreshTokenLifeTimeInDays); break;
                default: throw new ArgumentException("invalid token type");
            }
            var token = new JwtSecurityToken(
                AuthOptions.Issuer,
                AuthOptions.Audience,
                claims,
                expires: expiresTime,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public JwtBearerOptions GetBearerOptions(JwtBearerOptions options)
        {
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = AuthOptions.Issuer,

                ValidateAudience = true,
                ValidAudience = AuthOptions.Audience,

                ValidateLifetime = true,
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                ValidateIssuerSigningKey = true
            };
            return options;
        }
    }
}
