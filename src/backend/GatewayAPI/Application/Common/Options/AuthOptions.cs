﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Application.Common.Options
{
    public class AuthOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Secret { get; set; }
        public int AccessTokenLifeTimeInMinutes { get; set; }
        public int RefreshTokenLifeTimeInDays { get; set; }
        //public int RefreshTokenLifeTimeInMinutes { get; set; }
        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));
            //return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
        }

    }
}
