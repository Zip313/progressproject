﻿using Application.Common.JWT;
using Application.Common.Options;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Application.Interfaces
{
    public interface ITokenService
    {
        AuthOptions AuthOptions { get; }
        string GenerateJWT(User user, TokenType tokenType);
        JwtBearerOptions GetBearerOptions(JwtBearerOptions options);
    }
}
