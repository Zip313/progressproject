﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces
{
    public interface IContext
    {
        DbSet<Auth> Auth { get; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
