﻿using Application.ViewModels.Auth;
using Domain;

namespace Application.Interfaces
{
    public interface IAuthService
    {
        Task<AuthVm> GetAuthAsync(User user, CancellationToken cancellationToken, DateTime? signInDate = null);
    }
}
