﻿using FluentValidation;
using Application.Common.Behavior;
using Application.Common.JWT;
using Application.Common.Options;
using Application.Interfaces;
using Mapster;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Application.Common.Services;
using GrpcClient.Interfaces;
using GrpcClient.Services;
using System.Diagnostics;
using Application.Common.MapperConfigurations;

namespace Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration = null)
        {
            #region Mapster DI
            var typeAdapterConfig = TypeAdapterConfig.GlobalSettings;
            // scans the assembly and gets the IRegister, adding the registration to the TypeAdapterConfig
            typeAdapterConfig.Scan(Assembly.GetExecutingAssembly());
            typeAdapterConfig.Apply(new StockMappingConfiguration());
            // register the mapper as Singleton service for my application
            var mapperConfig = new Mapper(typeAdapterConfig);
            services.AddSingleton<IMapper>(mapperConfig);
            #endregion

            #region AddMediator
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
            services.AddValidatorsFromAssemblies(new[] { Assembly.GetExecutingAssembly() });
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            #endregion

            #region Auth Jwt
            //services.Configure<AuthOptions>(Configuration.GetSection("Auth"));
            if (configuration != null)
            {
                var authOptions = configuration.GetSection("Auth").Get<AuthOptions>();
                services.AddTransient<ITokenService, TokenService>(s => new TokenService(authOptions));
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        var service = services.BuildServiceProvider().GetService<ITokenService>();
                        options = service.GetBearerOptions(options);
                    });
            }
            #endregion

            #region AddServices            
            services.AddTransient<IAuthService, AuthService>();
            #endregion

            #region AddGrpcServices
            var grpcUsersServerAddress = Environment.GetEnvironmentVariable("GRPC_USERSSERVICE_ADDRESS");
            if (string.IsNullOrEmpty(grpcUsersServerAddress))
            {
                grpcUsersServerAddress = configuration["GrpcUsersServerAddress"] ?? string.Empty;
            }
            services.AddTransient<IUsersService, UsersService>(u => new UsersService(grpcUsersServerAddress));
            var grpcOrdersServerAddress = Environment.GetEnvironmentVariable("GRPC_ORDERSSERVICE_ADDRESS");
            if (string.IsNullOrEmpty(grpcOrdersServerAddress))
            {
                grpcOrdersServerAddress = configuration["GrpcOrdersServerAddress"] ?? string.Empty;
            }
            services.AddTransient<IOrdersService, OrdersService>(u=>new OrdersService(grpcOrdersServerAddress));
            Console.WriteLine($"grpcUsersServerAddress:{grpcUsersServerAddress}");
            Console.WriteLine($"grpcOrdersServerAddress:{grpcOrdersServerAddress}");
            #endregion

            return services;
        }
    }
}
