﻿using DocumentFormat.OpenXml.VariantTypes;
using System;

namespace Application.ViewModels.Reports
{
    public class SummaryReportVm
    {
        public bool IsMy { get; set; } 

        //Текущая стоимость акций пользователя (current_price * count)
        public double CurrentAmount { get; set; }

        //Себестоимость акций пользователя (user_price * count)
        public double UserAmount { get; set; }

        //Комиссия продажи по текущей цене (current_amount*0.003)
        public double CurrentAmountComission { get; set; }

        //Чистая сумма с продажи по текущей цене (current_amount - current_amount_comission)
        public double CurrentAmountPure { get; set; }

        //Профит от продажи по текущей цене (current_amount_pure - user_amount)
        public double SaleProfit { get; set; }

        //Профит в процентах от продажи по текущей цене (sale_profit / current_amount)
        public double SaleProfitPercent { get; set; }

        //Признак наличия профита (sale_profit > 0)
        public bool IsProfit { get; set; }

        //Процент дивидендного дохода (value / current_price)
        public double DividendsValuePercent { get; set; }

        //Сумма дивидендов (value * count)
        public double DividendsAmount { get; set; }

        //Налог с дивидендов (dividends_amount * 0.13)
        public double DividendsAmountTax { get; set; }

        //Сумма дивидендов после вычета налогов (dividends_amount - dividends_amount_tax)
        public double DividendsAmountPure { get; set; }

        //Биржевой идентификатор акции
        public string SecId { get; set; }

        //Краткое наименование акции
        public string StockShortName { get; set; }

        //Текущая цена
        public double CurrentPrice { get; set; }

        //Цена покупки
        public double UserPrice { get; set; }

        //Количество
        public int Count { get; set; }

        //Дата закрытия реестра
        public DateTime DividendsRegistryCloseDate { get; set; }

        //Размер дивиденда
        public double DividendsValue { get; set; }
    }
}
