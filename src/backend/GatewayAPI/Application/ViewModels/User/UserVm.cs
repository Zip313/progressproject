﻿using Domain.Enums;

namespace Application.ViewModels.User
{
    public class UserVm
    {
        public int Id { get; set; }
        /// <summary>
        /// Адрес емэйл, он же логин
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Статус активации
        /// </summary>
        public bool IsActivated { get; set; }
    }
    public enum RoleVm { User, Admin }
}
