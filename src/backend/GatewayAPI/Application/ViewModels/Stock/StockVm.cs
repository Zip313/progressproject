﻿using Application.ViewModels.Dividend;

namespace Application.ViewModels.Stock
{
    public class StockVm
    {
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор акции на бирже
        /// </summary>
        public string SecId { get; set; }

        /// <summary>
        /// Краткое наименование акции
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Предыдущая цена
        /// </summary>
        public Double PrevPrice { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// Дивиденды за прошлые и будущие периоды
        /// </summary>
        public List<DividendVm> Dividends { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Признак роста
        /// </summary>
        public bool IsIncrease { get; set; }

        /// <summary>
        /// Процент роста
        /// </summary>
        public Double IncreaseValuePer { get; set; }
    }
}
