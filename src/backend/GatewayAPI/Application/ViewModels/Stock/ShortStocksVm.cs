﻿namespace Application.ViewModels.Stock
{
    public class ShortStocksVm
    {
        public List<ShortStokVm> ShortStocks { get; set; }
    }

    public class ShortStokVm
    {
        public string SecId { get; set;}
        public string ShortName { get; set;}
        public double Price { get; set; }
    }
}
