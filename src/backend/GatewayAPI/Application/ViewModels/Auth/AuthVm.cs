﻿using Application.ViewModels.User;

namespace Application.ViewModels.Auth
{
    public class AuthVm
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public UserVm User { get; set; }
    }

    
}
