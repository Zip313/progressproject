﻿using Application.ViewModels.Auth;
using MediatR;

namespace Application.Requests.Auth.Commands.RefreshToken
{
    public class RefreshTokenCommand : IRequest<AuthVm>
    {
        public string RefreshToken { get; set; }
    }
}
