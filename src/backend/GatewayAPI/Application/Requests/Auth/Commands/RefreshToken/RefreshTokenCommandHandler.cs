﻿using Application.Common.Options;
using Application.Interfaces;
using Application.Requests.Abstract;
using Application.ViewModels.Auth;
using Domain;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Requests.Auth.Commands.RefreshToken
{
    public class RefreshTokenCommandHandler : BaseRequestHandler, IRequestHandler<RefreshTokenCommand, AuthVm>
    {
        private readonly IAuthService _authService;
        private readonly IUsersService _usersService;
        private readonly AuthOptions _authOptions;

        public RefreshTokenCommandHandler(IContext dbContext, IAuthService authService, ITokenService tokenService, IUsersService usersService) 
            : base(dbContext)
        {
            _authOptions = tokenService.AuthOptions;
            _authService = authService;
            _usersService = usersService;
        }

        public async Task<AuthVm> Handle(RefreshTokenCommand request, CancellationToken cancellationToken)
        {
            //Проверяем наличие RefreshToken и его актуальность
            var oldAuth = await _dbContext.Auth.FirstOrDefaultAsync(a => a.RefreshToken == request.RefreshToken);
            if (oldAuth == null) throw new ArgumentException("RefreshToken не найден"); 
            var minRefreshTokenDate = DateTime.Now.AddDays(_authOptions.RefreshTokenLifeTimeInDays * -1);
            if (oldAuth.RefreshTokenDate < minRefreshTokenDate) throw new ArgumentException("RefreshToken истек");

            //Получение пользователя у сервиса пользователей
            UserRequestById requestById = new UserRequestById
            {
                Id = oldAuth.UserId,
            };
            UserResponse response = await _usersService.GetUserByIdAsync(requestById);

            User user = response.Adapt<User>();
            var authVm = await _authService.GetAuthAsync(user, cancellationToken, oldAuth.SignInDate);

            return authVm;
        }
    }
}
