﻿using Application.Interfaces;
using Application.Requests.Abstract;
using Application.ViewModels.Auth;
using Domain;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Auth.Commands.SignIn
{
    public class SignInCommandHandler : BaseRequestHandler, IRequestHandler<SignInCommand, AuthVm>
    {
        private readonly IAuthService _authService;
        private readonly IUsersService _usersService;

        public SignInCommandHandler(IContext dbContext, IAuthService authService, IUsersService usersService) : base(dbContext)
        {
            _authService = authService;
            _usersService = usersService;
        }

        public async Task<AuthVm> Handle(SignInCommand request, CancellationToken cancellationToken)
        {
            //Получение пользователя у сервиса пользователей
            UserResponse response = await _usersService.GetUserByCredentialsAsync(request.Adapt<UserCredentials>());
            User user = response.Adapt<User>();

            //Получаем ответ с токенами
            var authVm = await _authService.GetAuthAsync(user, cancellationToken);

            return authVm;
        }
    }
}
