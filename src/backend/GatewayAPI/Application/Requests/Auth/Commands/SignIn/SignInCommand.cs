﻿using Application.ViewModels.Auth;
using MediatR;

namespace Application.Requests.Auth.Commands.SignIn
{
    public class SignInCommand : IRequest<AuthVm>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
