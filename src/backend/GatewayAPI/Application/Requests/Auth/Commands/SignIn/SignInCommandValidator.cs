﻿using FluentValidation;

namespace Application.Requests.Auth.Commands.SignIn
{
    public class SignInCommandValidator : AbstractValidator<SignInCommand>
    {
        public SignInCommandValidator()
        {
            RuleFor(command => command.Email).NotEmpty().WithMessage("is empty");
            RuleFor(command => command.Email).EmailAddress().WithMessage("invalid email");
            RuleFor(command => command.Password).NotEmpty().WithMessage("is empty");
            //RuleFor(command => command.Password).MinimumLength(8).WithMessage("low password");
        }
    }
}
