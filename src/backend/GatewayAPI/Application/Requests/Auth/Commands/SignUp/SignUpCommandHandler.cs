﻿using Application.Common.Utils;
using Application.Interfaces;
using Application.Requests.Abstract;
using Application.ViewModels.Auth;
using Domain;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Auth.Commands.SignUp
{
    public class SignUpCommandHandler : BaseRequestHandler, IRequestHandler<SignUpCommand, AuthVm>
    {
        private readonly IAuthService _authService;
        private readonly IUsersService _usersService;

        public SignUpCommandHandler(IContext dbContext, IAuthService authService, IUsersService usersService) : base(dbContext)
        {
            _authService = authService;
            _usersService = usersService;
        }


        public async Task<AuthVm> Handle(SignUpCommand request, CancellationToken cancellationToken)
        {
            // Получение пользователя у сервиса пользователей
            UserResponse response = await _usersService.AddUserAsync(request.Adapt<UserCredentials>());
            
            User user = response.Adapt<User>();

            var authVm = await _authService.GetAuthAsync(user, cancellationToken);

            return authVm;
        }
    }
}
