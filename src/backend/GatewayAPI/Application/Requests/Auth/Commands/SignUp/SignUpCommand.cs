﻿using Application.ViewModels.Auth;
using MediatR;

namespace Application.Requests.Auth.Commands.SignUp
{
    public class SignUpCommand : IRequest<AuthVm>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
