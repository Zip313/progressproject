﻿using FluentValidation;

namespace Application.Requests.Auth.Commands.SignUp
{
    public class SignUpCommandValidator : AbstractValidator<SignUpCommand>
    {
        public SignUpCommandValidator()
        {
            RuleFor(command => command.Email).NotEmpty().WithMessage("is empty");
            RuleFor(command => command.Email).EmailAddress().WithMessage("invalid email");
            RuleFor(command => command.Password).NotEmpty().WithMessage("is empty");
            //RuleFor(command => command.Password).MinimumLength(8).WithMessage("low password");
        }
    }
}
