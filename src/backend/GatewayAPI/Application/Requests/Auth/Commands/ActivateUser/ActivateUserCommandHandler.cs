﻿using Application.Interfaces;
using Application.Requests.Abstract;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Auth.Commands.ActivateUser
{
    public class ActivateUserCommandHandler : BaseRequestHandler, IRequestHandler<ActivateUserCommand>
    {
        private readonly IUsersService _usersService;

        public ActivateUserCommandHandler(IContext dbContext, IUsersService usersService) : base(dbContext)
        {
            this._usersService = usersService;
        }

        public async Task Handle(ActivateUserCommand request, CancellationToken cancellationToken)
        {
            await _usersService.ActivateUserAsync(request.Adapt<ActivateRequest>());
        }


    }
}
