﻿using MediatR;

namespace Application.Requests.Auth.Commands.ActivateUser
{
    public class ActivateUserCommand : IRequest
    {
        public string ActivationCode { get; set; }
    }
}
