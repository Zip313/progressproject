﻿using Application.ViewModels.Auth;
using MediatR;

namespace Application.Requests.Auth.Queries.GetAuthData
{
    public class GetAuthDataQuery : IRequest<AuthVm>
    {
        public int UserId { get; set; }
    }
}
