﻿using Application.Interfaces;
using Application.Requests.Abstract;
using Application.ViewModels.Auth;
using MediatR;

namespace Application.Requests.Auth.Queries.GetAuthData
{
    public class GetAuthDataQueryHandler : BaseRequestHandler, IRequestHandler<GetAuthDataQuery, AuthVm>
    {
        private ITokenService _tokenService;
        public GetAuthDataQueryHandler(IContext dbContext, ITokenService tokenService) : base(dbContext)
        {
            _tokenService = tokenService;
        }

        public async Task<AuthVm> Handle(GetAuthDataQuery request, CancellationToken cancellationToken)
        {
            //Получаем токены для ответа
            //var auth = await _dbContext.Auth.FirstOrDefaultAsync(a=>a.RefreshToken)
            //TODO: get user and tokens
            AuthVm vm = new AuthVm();
            return vm;
        }
    }
}
