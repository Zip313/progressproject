﻿using Application.ViewModels.User;
using MediatR;

namespace Application.Requests.Auth.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserVm>
    {
        public int UserId { get; set; }
    }
}
