﻿using Application.Interfaces;
using Application.Requests.Abstract;
using Application.ViewModels.User;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Auth.Queries.GetUser
{
    internal class GetUserQueryHandler : BaseRequestHandler, IRequestHandler<GetUserQuery, UserVm>
    {
        private readonly IUsersService usersService;

        public GetUserQueryHandler(IContext dbContext, IUsersService usersService) : base(dbContext)
        {
            this.usersService = usersService;
        }

        public async Task<UserVm> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var user = await usersService.GetUserByIdAsync(new UserRequestById { Id = request.UserId });
                        
            return user.User.Adapt<UserVm>();
        }
    }
}
