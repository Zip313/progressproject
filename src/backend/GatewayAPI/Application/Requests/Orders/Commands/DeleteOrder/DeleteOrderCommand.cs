﻿using MediatR;

namespace Application.Requests.Orders.Commands.DeleteOrder
{
    public class DeleteOrderCommand : IRequest
    {
        public int OrderId { get; set; }
    }
}
