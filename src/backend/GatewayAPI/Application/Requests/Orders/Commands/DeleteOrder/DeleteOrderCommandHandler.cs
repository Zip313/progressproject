﻿using Application.Common.Exceptions;
using Application.ViewModels.Order;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Orders.Commands.DeleteOrder
{
    public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand>
    {
        private readonly IOrdersService _ordersService;

        public DeleteOrderCommandHandler(IOrdersService ordersService)
        {
            this._ordersService = ordersService;
        }

        public async Task Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            //TODO: сделать вызов удаления записи ордера сервису ордера
            var deleteDto = request.Adapt<DeleteOrderRequest>();
            var response = await _ordersService.DeleteOrderAsync(deleteDto);
            if (response.IsError)
            {
                throw new Exception($"Получена ошибка от сервиса ордеров: {response.ErrorMessage}");
            }

            //var result = response.Orders.FirstOrDefault();
            //if (result == null)
            //{
            //    throw new NotFoundException($"Сервис ордеров не прислал некорректный ответ", response);
            //}
        }
    }
}
