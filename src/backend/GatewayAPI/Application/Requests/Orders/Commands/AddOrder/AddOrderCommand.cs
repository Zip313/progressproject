﻿using Application.ViewModels.Order;
using MediatR;

namespace Application.Requests.Orders.Commands.AddOrder
{
    /// <summary>
    /// Команда на добавление ордера.
    /// </summary>
    public class AddOrderCommand : IRequest<OrderVm>
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Краткое наименование акции.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Количество акций.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Дата приобретения.
        /// </summary>
        public DateTime OwnDate { get; set; }

        /// <summary>
        /// Цена покупки.
        /// </summary>
        public double Price { get; set; }
    }
}
