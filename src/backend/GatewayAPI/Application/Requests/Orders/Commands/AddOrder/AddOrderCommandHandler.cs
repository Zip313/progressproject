﻿using Application.Common.Exceptions;
using Application.ViewModels.Dividend;
using Application.ViewModels.Order;
using Application.ViewModels.Stock;
using Google.Protobuf.WellKnownTypes;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Orders.Commands.AddOrder
{
    public class AddOrderCommandHandler : IRequestHandler<AddOrderCommand, OrderVm>
    {
        private readonly IOrdersService _ordersService;

        public AddOrderCommandHandler(IOrdersService ordersService)
        {
            this._ordersService = ordersService;
        }

        public async Task<OrderVm> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            //TODO: переделать на работу с сервисом ордеров
            AddOrderRequest addOrder = request.Adapt<AddOrderRequest>();
            var response = await _ordersService.AddOrderAsync(addOrder);
            if (response.IsError)
            {
                throw new Exception($"Получена ошибка от сервиса ордеров: {response.ErrorMessage}");
            }

            var result = response.Orders.FirstOrDefault();
            if (result == null)
            {
                throw new NotFoundException($"Сервис ордеров не прислал некорректный ответ", response);
            }
            
            return result.Adapt<OrderVm>();
                
        }
    }
}
