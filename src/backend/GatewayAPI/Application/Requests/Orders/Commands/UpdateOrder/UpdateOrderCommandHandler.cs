﻿using Application.Common.Exceptions;
using Application.ViewModels.Dividend;
using Application.ViewModels.Order;
using Application.ViewModels.Stock;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Orders.Commands.UpdateOrder
{
    public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, OrderVm>
    {
        private readonly IOrdersService _ordersService;

        public UpdateOrderCommandHandler(IOrdersService ordersService)
        {
            this._ordersService = ordersService;
        }
        public async Task<OrderVm> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            //TODO: сдулать вызов сервиса ордеров для обновления
            var updateDto = request.Adapt<UpdateOrderRequest>();
            var response = await _ordersService.UpdateOrderAsync(updateDto);
            if (response.IsError)
            {
                throw new Exception($"Получена ошибка от сервиса ордеров: {response.ErrorMessage}");
            }

            var result = response.Orders.FirstOrDefault();
            if (result == null)
            {
                throw new NotFoundException($"Сервис ордеров не прислал некорректный ответ", response);
            }

            return result.Adapt<OrderVm>();            
        }
    }
}
