﻿using Application.ViewModels.Order;
using MediatR;

namespace Application.Requests.Orders.Commands.UpdateOrder
{
    public class UpdateOrderCommand : IRequest<OrderVm>
    {
        /// <summary>
        /// Идентификатор ордера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Краткое наименование акции
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Количество акций
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Дата приобретения
        /// </summary>
        public DateTime OwnDate { get; set; }

        /// <summary>
        /// Цена покупки
        /// </summary>
        public double Price { get; set; }
    }
}
