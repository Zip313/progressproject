﻿using Application.ViewModels.Order;
using MediatR;

namespace Application.Requests.Orders.Queries.GetOrdersByUser
{
    public class GetOrdersByUserIdQuery : IRequest<IEnumerable<OrderVm>>
    {
        public int UserId { get; set; }
    }
}
