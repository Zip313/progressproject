﻿using Application.Common.Exceptions;
using Application.ViewModels.Dividend;
using Application.ViewModels.Order;
using Application.ViewModels.Stock;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Orders.Queries.GetOrdersByUser
{
    internal class GetOrdersByUserQueryHandler : IRequestHandler<GetOrdersByUserIdQuery, IEnumerable<OrderVm>>
    {
        private readonly IOrdersService _ordersService;

        public GetOrdersByUserQueryHandler(IOrdersService ordersService)
        {
            this._ordersService = ordersService;
        }
        public async Task<IEnumerable<OrderVm>> Handle(GetOrdersByUserIdQuery request, CancellationToken cancellationToken)
        {
            var response = await _ordersService.GetOrdersByUserId(request.Adapt<GetOrdersByUserIdRequest>());
            if (response.IsError)
            {
                throw new Exception($"Получена ошибка от сервиса ордеров: {response.ErrorMessage}");
            }

            var result = response.Orders.ToList();

            if (result == null)
            {
                throw new NotFoundException($"Сервис ордеров не прислал некорректный ответ", response);
            }

            return result.ConvertAll(x => x.Adapt<OrderVm>());
        }
    }
}
