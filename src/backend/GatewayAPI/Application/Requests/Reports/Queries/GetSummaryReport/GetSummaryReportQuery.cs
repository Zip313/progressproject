﻿using Application.ViewModels.Reports;
using MediatR;

namespace Application.Requests.Reports.Queries.GetSummaryReport
{
    public class GetSummaryReportQuery : IRequest<List<SummaryReportVm>>
    {
        public int UserId { get; set; }
    }
}
