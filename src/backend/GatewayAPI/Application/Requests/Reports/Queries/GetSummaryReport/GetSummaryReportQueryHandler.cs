﻿using Application.Common.Exceptions;
using Application.ViewModels.Reports;
using GrpcClient;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;

namespace Application.Requests.Reports.Queries.GetSummaryReport
{
    public class GetSummaryReportQueryHandler : IRequestHandler<GetSummaryReportQuery, List<SummaryReportVm>>
    {
        private readonly IOrdersService _ordersService;

        public GetSummaryReportQueryHandler(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        public async Task<List<SummaryReportVm>> Handle(GetSummaryReportQuery request, CancellationToken cancellationToken)
        {
            var response = await _ordersService.GetSummaryReportAsync(request.Adapt<GetSummaryRequest>());
            if (response == null)
            {
                throw new Exception($"Ошибка сервиса ордеров, параметр вызова {request.UserId}");
            }

            var result = response.Data.ToList().ConvertAll(d => d.Adapt<SummaryReportVm>());

            return result;
        }
    }
}
