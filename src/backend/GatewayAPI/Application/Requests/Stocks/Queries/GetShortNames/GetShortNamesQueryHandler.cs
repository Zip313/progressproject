﻿using Application.ViewModels.Stock;
using GrpcClient.Interfaces;
using Mapster;
using MediatR;
using System.Collections.Generic;

namespace Application.Requests.Stocks.Queries.GetShortNames
{
    public class GetShortNamesQueryHandler : IRequestHandler<GetShortNamesQuery, ShortStocksVm>
    {
        private readonly IOrdersService _ordersService;

        public GetShortNamesQueryHandler(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        public async Task<ShortStocksVm> Handle(GetShortNamesQuery request, CancellationToken cancellationToken)
        {
            var response = await _ordersService.GetStocksShortNames();
            var result = response.Adapt<ShortStocksVm>();
            return result;
        }
    }
}
