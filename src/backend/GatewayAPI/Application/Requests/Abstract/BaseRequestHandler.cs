﻿using Application.Interfaces;
using Domain.Enums;

namespace Application.Requests.Abstract
{
    public abstract class BaseRequestHandler
    {
        public readonly IContext _dbContext;
        public BaseRequestHandler(IContext dbContext)
        {
            _dbContext = dbContext;
        }

        protected async Task<Role> GetUserRole(int userId, CancellationToken cancellationToken)
        {
            //var result = await _dbContext.Users.Where(u => u.Id == userId).Select(u => u.Role).FirstOrDefaultAsync(cancellationToken);
            //if (result == null) throw new NotFoundException($"role not found for user with Id {userId}", userId);
            return Role.User;
        }
        
    }
}
