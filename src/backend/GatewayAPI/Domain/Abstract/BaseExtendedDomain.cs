﻿namespace Domain.Abstract
{
    public class BaseExtendedDomain : BaseDomain
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public int? UpdatedByUserId { get; set; }
        public bool IsDeleted { get; set; } = false;

    }
}
