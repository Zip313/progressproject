﻿using Domain.Enums;
using Domain.Abstract;

namespace Domain
{
    public class User : BaseDomain
    {
        /// <summary>
        /// Адрес емэйл, он же логин
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        /// Не должен его принимать от других сервисов, передача в DTO
        //public string Password { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Статус активации
        /// </summary>
        public bool IsActivated { get; set; }
    }
}
