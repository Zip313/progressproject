﻿using Domain.Abstract;

namespace Domain
{
    public class Auth : BaseDomain
    {
        public string RefreshToken { get; set; }
        public int UserId { get; set; }
        public DateTime SignInDate { get; set; }
        public DateTime RefreshTokenDate { get; set; }
        public string AddressIP { get; set; }
    }
}
