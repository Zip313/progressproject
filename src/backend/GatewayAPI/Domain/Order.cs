﻿using Domain.Abstract;

namespace Domain
{
    /// <summary>
    /// Ордер - содержит информацию об определенном наборе акций пользователя 
    /// </summary>
    public class Order : BaseDomain
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Акция
        /// </summary>
        public Stock Stock { get; set; }

        /// <summary>
        /// Количество акций
        /// </summary>
        public uint Count { get; set; }

        /// <summary>
        /// Дата приобретения
        /// </summary>
        public DateTime OwnDate { get; set; }

        /// <summary>
        /// Цена покупки
        /// </summary>
        public Double Price { get; set; }

        /// <summary>
        /// Сумма покупки
        /// </summary>
        public Double Amount { get; set; }

        /// <summary>
        /// Сумма себестоимости
        /// </summary>
        public Double CostAmount { get; set; }

        /// <summary>
        /// Доходная? Текущая цена больше цены покупки
        /// </summary>
        public bool IsIncrease { get; set; }

        /// <summary>
        /// Доходность
        /// </summary>
        public double IncreaseValue { get; set; }
    }
}
