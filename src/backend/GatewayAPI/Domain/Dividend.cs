﻿using Domain.Abstract;

namespace Domain
{
    public class Dividend : BaseDomain
    {
        /// <summary>
        /// Биржевой идентификатор акции
        /// </summary>
        public string SecId { get; set; }
        /// <summary>
        /// Дата закрытия реестра
        /// </summary>
        public DateTime RegistryCloseDate { get; set; }
        /// <summary>
        /// Размер дивиденда
        /// </summary>
        public Double Value { get; set; }
        /// <summary>
        /// Валюта, строка
        /// </summary>
        public string CurrencyId { get; set; }
    }
}
