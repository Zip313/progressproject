﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserService
{
    public class TestRequest
    {
        public int UserId { get; set; }
    }
    public class TestResponse
    {
        public int UserId { get; set; }
        public string UserName { get; set; } = "testUser";
    }
}
