﻿using MassTransit;

namespace UserService
{
    public class TestConsumer : IConsumer<TestRequest>
    {
        public Task Consume(ConsumeContext<TestRequest> context)
        {
            return context.RespondAsync(new TestResponse() { UserId = 1, UserName = "Test" });
        }
    }
}
