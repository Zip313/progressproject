﻿using Application;
using Application.Common.JWT;
using Application.Common.Options;
using Application.Interfaces;
using Persistence;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace LB.Tests.Common
{
    public class TestCommandBase : IDisposable
    {
        protected readonly Persistence.AppContext context;

        public TestCommandBase()
        {
            this.context = ContextFactory.Create();
        }

        protected IMediator GetMediator()
        {
            var services = new ServiceCollection();
            services.AddApplication();

            //services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(LB.).Assembly.GetExecutingAssembly()));

            var provider = services.BuildServiceProvider();

            var mediator = provider.GetService<IMediator>();

            return mediator;
        }

        protected ITokenService GetTokenService()
        {
            var authOptions = new AuthOptions()
            {
                Issuer = "authServer",
                Secret = "secretKey1234567890!sdsafasdfasdzxczvs12!!!32@#",
                AccessTokenLifeTimeInMinutes = 30,
                RefreshTokenLifeTimeInDays = 14,
                Audience = "resourceServer",
            };
            var tokenService = new TokenService(authOptions);
            return tokenService;
        }

        public void Dispose()
        {
            ContextFactory.Destroy(context);
        }
    }
}
