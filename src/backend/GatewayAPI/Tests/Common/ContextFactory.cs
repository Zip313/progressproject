﻿using Application.Common.Utils;
using Domain;
using Persistence;
using Microsoft.EntityFrameworkCore;

namespace LB.Tests.Common
{
    internal class ContextFactory
    {


        public static Persistence.AppContext Create()
        {
            var options = new DbContextOptionsBuilder<Persistence.AppContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new Persistence.AppContext(options);
            context.Auth.AddRange(
                new Auth
                {
                    Id = 1,
                    UserId = 2,
                    RefreshToken = "testToken",
                    RefreshTokenDate = DateTime.Now,
                    SignInDate = DateTime.Now,
                    AddressIP = "10.8.65.11",
                }
                );
            context.SaveChanges();
            return context;
        }

        public static void Destroy(Persistence.AppContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }


    }
}
