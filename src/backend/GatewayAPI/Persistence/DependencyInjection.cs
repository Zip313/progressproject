﻿using Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistance(this IServiceCollection services, IConfiguration configuration)
        {
            var user = Environment.GetEnvironmentVariable("DB_USR");
            var password = Environment.GetEnvironmentVariable("DB_PSW");
            var connectionString = configuration["DbConnection"];
            connectionString = connectionString.Replace(@"<user>", user);
            connectionString = connectionString.Replace(@"<password>", password);
            Console.WriteLine(connectionString);
            services.AddDbContext<AppContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
            System.AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            services.AddScoped<IContext>(provider => provider.GetService<AppContext>()!);
            return services;
        }
    }
}
