﻿using Persistence.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Application.Interfaces;
using Domain;

namespace Persistence
{
    public class AppContext : DbContext, IContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options) { }
        public DbSet<Auth> Auth { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AuthConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
