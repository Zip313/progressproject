﻿using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public static class DbInitializer
    {
        public static void Initialize(this AppContext context)
        {
            context.Database.Migrate();
        }
    }
}
