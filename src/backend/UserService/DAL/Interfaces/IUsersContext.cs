﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Interfaces
{
    /// <summary>
    /// Интерфейс контекста пользователей
    /// </summary>
    public interface IUsersContext
    {
        /// <summary>
        /// Коллекция пользователей
        /// </summary>
        DbSet<User> Users { get; }

        /// <summary>
        /// Сохранение данных в БД
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
