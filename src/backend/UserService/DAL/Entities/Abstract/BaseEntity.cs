﻿namespace DAL.Entities.Abstract
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
