﻿namespace DAL.Entities.Enums
{
    public enum Role 
    {
        User,
        Admin
    }
}
