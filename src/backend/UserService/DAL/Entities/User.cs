﻿using DAL.Entities.Abstract;
using DAL.Entities.Enums;

namespace DAL.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Адрес email, он же логин
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Статус активации
        /// </summary>
        public bool IsActivated { get; set; }

        /// <summary>
        /// Код активации пользователя
        /// </summary>
        public string ActivationCode { get; set; }
    }
}
