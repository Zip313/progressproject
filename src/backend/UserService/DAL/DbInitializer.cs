﻿using Microsoft.EntityFrameworkCore;

namespace DAL
{
    /// <summary>
    /// Инициализация БД
    /// </summary>
    public static class DbInitializer
    {
        /// <summary>
        /// Инициализация БД
        /// </summary>
        public static void Initialize(this UsersContext context)
        {
            context.Database.Migrate();
        }
    }
}
