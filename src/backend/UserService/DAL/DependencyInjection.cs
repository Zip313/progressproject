﻿using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DAL
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Добавление контекста в зависимости
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddDAL(this IServiceCollection services, IConfiguration configuration) 
        {
            var user = Environment.GetEnvironmentVariable("DB_USR");
            var password = Environment.GetEnvironmentVariable("DB_PSW");
            var connectionString = configuration["DbConnection"];
            connectionString = connectionString.Replace(@"<user>", user);
            connectionString = connectionString.Replace(@"<password>", password);
            services.AddDbContext<UsersContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            services.AddScoped<IUsersContext>(provider => provider.GetService<UsersContext>()!);
            return services;
        }

        /// <summary>
        /// Вызов инициализации БД
        /// </summary>
        /// <param name="serviceProvider"></param>
        public static void InitializeDb(this IServiceProvider serviceProvider)
        {
            var ctx = serviceProvider.GetRequiredService<UsersContext>();
            DbInitializer.Initialize(ctx);
        }
    }
}
