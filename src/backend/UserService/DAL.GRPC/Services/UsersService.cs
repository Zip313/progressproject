﻿using DAL.Entities;
using DAL.Entities.Enums;
using DAL.Interfaces;
using Google.Protobuf.Collections;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace DAL.GRPC.Services
{
    public class UsersService : Users.UsersBase
    {
        private readonly IUsersContext _context;

        public UsersService(IUsersContext usersContext)
        {
            _context = usersContext;
        }

        //rpc GetUserByCredentials (GetUserByCredentialsRequest) returns (UserResponse) {}
        public override async Task<UserResponse> GetUserByCredentials(UserCredentials request, ServerCallContext context)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == request.Email && u.Password == request.Password);

            if (user == null) throw new RpcException(new Status(StatusCode.DataLoss, "Имя пользователя или пароль не совпадают"));
            UserResponse response = new UserResponse()
            {
                Id = user.Id,
                Email = user.Email,
                IsActivated = user.IsActivated,
            };
            response.Roles.AddRange(user.Roles.ConvertAll(r => (int)r));
            return response;
        }

        //rpc AddUser(UserCredentials) returns(UserResponse) { }
        public override async Task<UserResponse> AddUser(UserCredentials request, ServerCallContext context)
        {
            var isUserExists = await _context.Users.AnyAsync(u => u.Email == request.Email && u.Password == request.Password);
            if (isUserExists)
            {
                throw new RpcException(new Status(StatusCode.AlreadyExists,"Указанный пользователь уже существует"));
            }
            User user = new User
            {
                Email = request.Email,
                Password = request.Password,
                Roles = new List<Role> { Role.User },
                IsActivated = false,
                ActivationCode = Guid.NewGuid().ToString(),
            };

            await _context.Users.AddAsync(user,context.CancellationToken);
            await _context.SaveChangesAsync(context.CancellationToken);
            UserResponse response = new UserResponse()
            {
                Id = user.Id,
                Email = user.Email,
                //Roles = userRoles,
                IsActivated = user.IsActivated,
            };
            response.Roles.AddRange(user.Roles.ConvertAll(r => (int)r));
            return response;
        }
    }
}
