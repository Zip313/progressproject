using BLL.Common;

namespace UserService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
                        
            // ��������� ����������� BLL
            builder.Services.AddBLL(builder.Configuration);

            var app = builder.Build();
            
            //������������� ��
            app.InitializeDB();

            // ��������� ������� GRPC �� BLL
            app.AddBLLMapGrpc();
            app.Run();
        }
    }
}