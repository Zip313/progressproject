﻿using BLL.Services;
using BLL.Settings;
using DAL;
using Mapster;
using MapsterMapper;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace BLL.Common
{
    /// <summary>
    /// Внедрение зависимостей
    /// </summary>
    public static class DependencyInjection
    {
        /// <summary>
        /// Добавление зависимостей DAL и BLL
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddBLL(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDAL(configuration);

            #region Mapster DI
            var typeAdapterConfig = TypeAdapterConfig.GlobalSettings;
            // scans the assembly and gets the IRegister, adding the registration to the TypeAdapterConfig
            typeAdapterConfig.Scan(Assembly.GetExecutingAssembly());
            // register the mapper as Singleton service for my application
            var mapperConfig = new Mapper(typeAdapterConfig);
            services.AddSingleton<IMapper>(mapperConfig);
            #endregion

            #region Loki DI
            services.AddLogging(builder =>
            {
                builder.AddLoki(options =>
                {
                    options.Http = new LokiLoggingProvider.Options.HttpOptions()
                    {
                        Address = "http://pias-loki:3100"
                    };
                    options.Client = LokiLoggingProvider.Options.PushClient.Http;
                    options.StaticLabels.JobName = "PIAS_UserService";
                    options.Formatter = LokiLoggingProvider.Options.Formatter.Json;
                });
                builder.AddConsole();
            });
            #endregion

            #region RMQ 
            var rmqSettings = configuration.Get<ApplicationSettings>().RmqSettings;
            if (string.IsNullOrEmpty(rmqSettings.Login)) 
            {
                rmqSettings.Login = Environment.GetEnvironmentVariable("RABBIT_USR"); 
                rmqSettings.Password = Environment.GetEnvironmentVariable("RABBIT_PSW");
            }
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    
                    cfg.Host(rmqSettings.Host,rmqSettings.Port, rmqSettings.VHost, h =>
                    {
                        h.Username(rmqSettings.Login);
                        h.Password(rmqSettings.Password);
                    });
                });
            });
            #endregion

            services.AddGrpc();
            return services;
        }

        /// <summary>
        /// Добавление маппинга GRPC
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static WebApplication AddBLLMapGrpc(this WebApplication app)
        {
            app.MapGrpcService<UsersService>();
            app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client.");

            return app;
        }

        public static WebApplication InitializeDB(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                try
                {
                    serviceProvider.InitializeDb();
                    Console.WriteLine("!!!!!!DB init is OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("!!!!!!DB init is not OK " + ex.Message);
                }
            }
            return app;
        }

    }
}
