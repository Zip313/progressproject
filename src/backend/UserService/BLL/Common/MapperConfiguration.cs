﻿using DAL.Entities;
using Google.Protobuf.Collections;
using Mapster;
using UserService;

namespace BLL.MapperConfigurations
{
    public class MapperConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            TypeAdapterConfig.GlobalSettings.Default
                .UseDestinationValue(member => member.SetterModifier == AccessModifier.None &&
                                   member.Type.IsGenericType &&
                                   member.Type.GetGenericTypeDefinition() == typeof(RepeatedField<>));
         
            config.NewConfig<User, UserResponse>()
                .Map(dest => dest.IsError, src => false)
                .Map(dest => dest.User, src => src);
        }
    }
}
