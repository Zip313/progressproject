﻿using Application.Common.Utils;
using DAL.Entities;
using DAL.Entities.Enums;
using DAL.Interfaces;
using Grpc.Core;
using Mapster;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RMQ;
using UserService;

namespace BLL.Services
{
    /// <summary>
    /// Сервис пользователей, реализация protobuf users.proto
    /// </summary>
    public class UsersService : Users.UsersBase
    {
        private readonly IUsersContext _context;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IBus _bus;
        private readonly ILogger<UsersService> _logger;

        public UsersService(
            IUsersContext usersContext, 
            IPublishEndpoint publishEndpoint, 
            IBus bus, 
            ILogger<UsersService> logger)
        {
            _context = usersContext;
            _publishEndpoint = publishEndpoint;
            _bus = bus;
            _logger = logger;
        }

        /// <summary>
        /// Получение пользователя по ID
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<UserResponse> GetUserById(UserRequestById request, ServerCallContext context)
        {
            _logger.LogInformation($"GetUserById попытка получения пользователя по userId:{request.Id}");
            var user = await _context.Users.FindAsync(request.Id);

            if (user == null)
            {
                _logger.LogWarning($"GetUserById пользователь не найден userId:{request.Id}");
                return GetUserResponseError("Пользователь не найден");
            }
            
            _logger.LogInformation($"GetUserById пользователь получен userId:{request.Id}");
            return user.Adapt<UserResponse>();
        }

        /// <summary>
        /// Получение пользователя по логину и паролю
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<UserResponse> GetUserByCredentials(UserCredentials request, ServerCallContext context)
        {
            _logger.LogInformation($"GetUserByCredentials попытка получения пользователя по UserCredentials email:{request.Email}");
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == request.Email);

            if (user == null || !SecurePasswordHasher.Verify(request.Password, user.Password))
            {
                if (user == null)
                {
                    _logger.LogWarning($"GetUserByCredentials пользователь не найден email:{request.Email}");
                } else
                {
                    _logger.LogWarning($"GetUserByCredentials неправильный пароль пользователя email:{request.Email}");
                }

                return GetUserResponseError("Имя пользователя или пароль не верны");
            }
                        
            _logger.LogInformation($"GetUserByCredentials пользователь получен email:{request.Email}");
            return user.Adapt<UserResponse>();
        }

        /// <summary>
        /// Добавление пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<UserResponse> AddUser(UserCredentials request, ServerCallContext context)
        {
            _logger.LogInformation($"AddUser попытка добавления пользователя по UserCredentials email:{request.Email}");
            request.Password = SecurePasswordHasher.Hash(request.Password);
            var isUserExists = await _context.Users.AnyAsync(u => u.Email == request.Email && u.Password == request.Password);
            if (isUserExists)
            {
                _logger.LogWarning($"AddUser указанный пользователь уже существует email:{request.Email}");
                Console.WriteLine("Указанный пользователь уже существует");
                return GetUserResponseError("Указанный пользователь уже существует");
            }

            User user = request.Adapt<User>();
            user.Roles = new List<Role> { Role.User };
            user.IsActivated = false;
            user.ActivationCode = Guid.NewGuid().ToString();

            await _context.Users.AddAsync(user, context.CancellationToken);
            await _context.SaveChangesAsync(context.CancellationToken);
            
            _logger.LogInformation($"AddUser пользователь добавлен(id:{user.Id}), начинается отправка сообщения активации через rabbit email:{request.Email}");
            try
            {
                UserAddedDto userAddedDto = new UserAddedDto
                {
                    Id = user.Id,
                    Email = user.Email,
                    ActivationCode = user.ActivationCode,
                    CreatedDate = DateTime.UtcNow,
                };
                await _publishEndpoint.Publish(userAddedDto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddUser неудачная попытка отправки сообщения активации через rabbit email:{request.Email}. {ex.Message}");
                throw;
            }

            _logger.LogInformation($"AddUser пользователь добавлен, cообщение активации отправлено в rabbit email:{request.Email}, userId:{user.Id}");
            return user.Adapt<UserResponse>();
        }

        /// <summary>
        /// Активация пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<ActivationResponse> ActivateUser(ActivateRequest request, ServerCallContext context)
        {
            _logger.LogInformation($"ActivateUser активация пользователя ActivationCode:{request.ActivationCode}");
            var user = await _context.Users.FirstOrDefaultAsync(u=>u.ActivationCode == request.ActivationCode);
            if(user == null)
            {
                _logger.LogWarning($"ActivateUser пользователь не найден по ActivationCode:{request.ActivationCode}");
                return new ActivationResponse
                {
                    IsError = true,
                    ErrorMessage = "Активационный код не соответствует ни одному пользователю",
                };
            }
            user.IsActivated = true;
            await _context.SaveChangesAsync(context.CancellationToken);

            var result = new ActivationResponse
            {
                IsError = false,
                ErrorMessage = string.Empty,
                IsActivated = true
            };
            
            _logger.LogInformation($"ActivateUser активация пользователя успешна ActivationCode:{request.ActivationCode}, userId:{user.Id}");
            return result;
        }

        private UserResponse GetUserResponseError(string message)
        {
            return new UserResponse()
            {
                IsError = true,
                ErrorMessage = message,
            };
        }

    }
    
}
