using EmailSender.Extensions;

namespace EmailSender
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHostBuilder builder = Host.CreateDefaultBuilder(args);
            IHost host = builder.ConfigureServices((context,services) =>
                {
                    services.AddServices(context);
                })
                .Build();
            host.Run();
        }
    }
}