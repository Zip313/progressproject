﻿using EmailSender.Services.Interfaces;
using EmailSender.Settings;
using Google.Apis.Auth.OAuth2;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using RMQ;
using System.Runtime;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Channels;

namespace EmailSender.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _settings;
        private readonly ILogger _logger;

        public MailService(MailSettings mailSettings, ILogger logger)
        {
            this._settings = mailSettings;
            _logger = logger;
        }


        /// <summary>
        /// Отправка письма активации пользователя
        /// </summary>
        /// <param name="userAddedDto">DTO добавления пользователя</param>
        /// <returns>Task</returns>
        public async Task SendActivateMessage(UserAddedDto userAddedDto)
        {
            _logger.LogInformation($"SendActivateMessage Отправка письма активации пользователя userId:{userAddedDto.Id}");
            try
            {
                string subject = $"Активация аккаунта на сервисе";
                string message = $"""
                    <div>
                      <h1>Для активации перейдите по ссылке</h1>
                      <a href="{_settings.ApiUrl}/Auth/Activate/{userAddedDto.ActivationCode}">Link</a>
                    </div>
                    """;
                await this.SendMessageAsync(userAddedDto.Email, subject, message);
                _logger.LogInformation($"SendActivateMessage Отправка письма активации выполнена userId:{userAddedDto.Id}, email:{userAddedDto.Email}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"SendActivateMessage Во время отправки произошла ошибка userId:{userAddedDto.Id}, email:{userAddedDto.Email}. {ex.Message}");
                throw;
            }
        }


        /// <summary>
        /// Общий алгоритм отправки письма
        /// </summary>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        private async Task SendMessageAsync(string email, string subject, string message)
        {
            using var msg = new MimeMessage();

            msg.From.Add(new MailboxAddress("Сервис личных инвестиций", _settings.User));
            msg.To.Add(new MailboxAddress("", email));
            msg.Subject = subject;
            msg.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_settings.Host, _settings.Port, false);//
                    //client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_settings.User, _settings.Password);
                    await client.SendAsync(msg);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                finally
                {
                    Console.WriteLine("success");
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }
    }
}
