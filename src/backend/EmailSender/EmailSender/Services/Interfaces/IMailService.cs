﻿using RMQ;

namespace EmailSender.Services.Interfaces
{
    public interface IMailService
    {
        Task SendActivateMessage(UserAddedDto userAddedDto);
    }
}
