﻿namespace RMQ
{
    public class UserAddedDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string ActivationCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
