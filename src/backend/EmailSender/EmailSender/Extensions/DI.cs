﻿using EmailSender.Services.Interfaces;
using EmailSender.Services;
using EmailSender.Settings;
using MassTransit;
using System.Reflection;

namespace EmailSender.Extensions
{
    public static class DI
    {
        public static IServiceCollection AddServices(this IServiceCollection services, HostBuilderContext context)
        {
            #region Loki DI
            services.AddLogging(builder =>
            {
                builder.AddLoki(options =>
                {
                    options.Http = new LokiLoggingProvider.Options.HttpOptions()
                    {
                        Address = "http://pias-loki:3100"
                    };
                    options.Client = LokiLoggingProvider.Options.PushClient.Http;
                    options.StaticLabels.JobName = "PIAS_EmailSender";
                    options.Formatter = LokiLoggingProvider.Options.Formatter.Json;
                });
                builder.AddConsole();
            });
            #endregion

            #region AddMailService

            var mailSettings = context.Configuration.Get<ApplicationSettings>().MailSettings;
            if (string.IsNullOrEmpty(mailSettings.User))
            {
                mailSettings.Host = Environment.GetEnvironmentVariable("SMTP_HOST") ?? "";
                ushort.TryParse(Environment.GetEnvironmentVariable("SMTP_PORT"), out var port);
                mailSettings.Port = port;
                mailSettings.User = Environment.GetEnvironmentVariable("SMTP_USER") ?? "";
                mailSettings.Password = Environment.GetEnvironmentVariable("SMTP_PASSWORD") ?? "";
                mailSettings.ApiUrl = Environment.GetEnvironmentVariable("API_URL") ?? "";
            }

            var logger = services.BuildServiceProvider().GetService<ILogger<MailService>>();

            services.AddTransient<IMailService, MailService>(x => new MailService(mailSettings,logger));

            #endregion


            #region AddRabbit

            var rmqSettings = context.Configuration.Get<ApplicationSettings>().RmqSettings;
            if (string.IsNullOrEmpty(rmqSettings.Login))
            {
                //rmqSettings.Host= Environment.GetEnvironmentVariable("RABBIT_USR") ?? "";
                //rmqSettings.Port=""
                rmqSettings.Login = Environment.GetEnvironmentVariable("RABBIT_USR") ?? "";
                rmqSettings.Password = Environment.GetEnvironmentVariable("RABBIT_PSW") ?? "";
                Console.WriteLine($"""
                    host:{rmqSettings.Host}
                    port:{rmqSettings.Port}
                    user:{rmqSettings.Login}
                    pass:{rmqSettings.Password}
                    """);
            }

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(rmqSettings.Host, rmqSettings.Port, rmqSettings.VHost, h =>
                    {
                        h.Username(rmqSettings.Login);
                        h.Password(rmqSettings.Password);
                    });

                    cfg.ConfigureEndpoints(context);
                });

                var entryAssembly = Assembly.GetEntryAssembly();
                x.AddConsumers(entryAssembly);
            });

            #endregion

            return services;
        }
    }
}
