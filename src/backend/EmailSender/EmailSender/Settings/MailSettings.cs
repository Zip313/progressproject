﻿namespace EmailSender.Settings
{
    public class MailSettings
    {
        public string Host { get; set; }
        public ushort Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string ApiUrl { get; set; }

    }
}
