﻿namespace EmailSender.Settings
{
    public class ApplicationSettings
    {
        public RmqSettings RmqSettings { get; set; }
        public MailSettings MailSettings { get; set; }
    }
}
