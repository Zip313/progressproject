﻿using EmailSender.Services.Interfaces;
using MassTransit;
using Newtonsoft.Json;
using RMQ;

namespace EmailSender.Consumers
{
    
    public class UserAddedConsumer : IConsumer<UserAddedDto>
    {
        private readonly IMailService _mailService;

        public UserAddedConsumer(IMailService mailService)
        {
            this._mailService = mailService;
        }

        public async Task Consume(ConsumeContext<UserAddedDto> context)
        {
            await Task.Delay(1000);

            await _mailService.SendActivateMessage(context.Message);

            var jsonMessage = JsonConvert.SerializeObject(context.Message);
            Console.WriteLine($"OrderCreated message: {jsonMessage}");
        }
    }
}
