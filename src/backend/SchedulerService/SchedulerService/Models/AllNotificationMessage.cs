﻿namespace BLL.Models.Bus
{
    public class AllNotificationMessage
    {
        public NotificationTypes Type { get; set; }

        public string? Message { get; set; }
    }
}
