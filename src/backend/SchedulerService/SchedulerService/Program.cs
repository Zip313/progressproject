using MassTransit;
using Quartz;
using SchedulerService.BackgroundJobs;
using SchedulerService.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

#region RMQ 
var rmqSettings = builder.Configuration.GetSection("RmqSettings").Get<RmqSettings>();

if (string.IsNullOrEmpty(rmqSettings.Login))
{
    rmqSettings.Login = Environment.GetEnvironmentVariable("RABBIT_USR");
    rmqSettings.Password = Environment.GetEnvironmentVariable("RABBIT_PSW");
}
builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq((context, cfg) =>
    {

        cfg.Host(rmqSettings.Host, rmqSettings.Port, rmqSettings.VHost, h =>
        {
            h.Username(rmqSettings.Login);
            h.Password(rmqSettings.Password);
        });
    });
});
#endregion

builder.Services.AddQuartz(configurator =>
{
    var jobKeyNotifications = new JobKey("SendNotificationsAsyncJob");
    configurator.AddJob<SendNotificationsAsyncJob>(opts => opts.WithIdentity(jobKeyNotifications));

    configurator.AddTrigger(opts => opts
        .ForJob(jobKeyNotifications)
        .WithIdentity("SendNotificationsAsyncJob-trigger")
        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(13, 00).InTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time")))); // ������ ���� � 00:05.
});
builder.Services.AddQuartzHostedService(configurator =>
{
    configurator.WaitForJobsToComplete = true;
});

var app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

Console.WriteLine(DateTime.Now.ToString());

app.Run();

Console.WriteLine(DateTime.Now.ToString());
