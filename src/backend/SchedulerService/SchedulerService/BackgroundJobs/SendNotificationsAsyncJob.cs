﻿using BLL.Models.Bus;
using MassTransit;
using Quartz;
using SchedulerService.Models;

namespace SchedulerService.BackgroundJobs
{
    /// <summary>
    /// Работа в фоне на отправку уведомлений
    /// Выполняется в соответствии с планом, который находится в <see cref="Program"/> метод AddQuartz 
    /// </summary>
    [DisallowConcurrentExecution]
    public class SendNotificationsAsyncJob : IJob
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IBus _bus;
        private readonly ILogger<SendNotificationsAsyncJob> _logger;

        public SendNotificationsAsyncJob(IPublishEndpoint publishEndpoint, IBus bus, ILogger<SendNotificationsAsyncJob> logger)
        {
            _publishEndpoint = publishEndpoint;
            _bus = bus;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation("Запуск джобы с рассылкой информации по портфелям");

            try
            {
                var notificationMessage = new AllNotificationMessage
                {
                    Type = NotificationTypes.DailyPortfolio
                };
                await _publishEndpoint.Publish(notificationMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Неудачная попытка отправки сообщения с утренней рассылкой. {ex.Message}");
                throw;
            }

            _logger.LogInformation("Запрос рассылки с информацией по портфелям отправлен в очередь");
        }
    }
}

