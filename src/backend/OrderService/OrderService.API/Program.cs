using OrderService.BLL.Extentions;
using OrderService.BLL.Jobs.Schedulers;

namespace OrderService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            
            // ��������� ����������� BLL
            builder.Services.AddBll(builder.Configuration);
            builder.Services.AddHttpClient();

            var app = builder.Build();

            //������������� ��
            app.InitializeDB();

            // ��������� ������� GRPC �� BLL
            app.AddBLLMapGrpc();

            using (var scope = app.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                try
                {
                    UpdateStocksScheduler.Start(serviceProvider);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            //UpdateStocksScheduler.Start(app.Services);
            app.Run();
        }
    }
}