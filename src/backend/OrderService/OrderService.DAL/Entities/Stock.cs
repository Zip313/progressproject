﻿using OrderService.DAL.Entities.Abstract;

namespace OrderService.DAL.Entities
{
    /// <summary>
    /// Сущность акции
    /// </summary>
    public class Stock : BaseEntity
    {
        /// <summary>
        /// Внешний идентификатор акции
        /// </summary>
        public string SecId { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Предыдущая цена
        /// </summary>
        public Double PrevPrice { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// Дивиденды за прошлые и будущие периоды
        /// </summary>
        public List<Dividend> Dividends { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}
