﻿using OrderService.DAL.Entities.Abstract;

namespace OrderService.DAL.Entities
{
    /// <summary>
    /// Сущность дивиденда
    /// </summary>
    public class Dividend : BaseEntity
    {
        /// <summary>
        /// Идентификатор акции
        /// </summary>
        public int StockId { get; set; }

        /// <summary>
        /// Акция
        /// </summary>
        public Stock Stock { get; set; }

        /// <summary>
        /// Биржевой идентификатор акции
        /// </summary>
        public string SecId { get; set; }

        /// <summary>
        /// Дата закрытия реестра
        /// </summary>
        public DateTime RegistryCloseDate { get; set; }

        /// <summary>
        /// Размер дивиденда
        /// </summary>
        public Double Value { get; set; }
        
        /// <summary>
        /// Валюта, строка
        /// </summary>
        public string CurrencyId { get; set; }
    }
}
