﻿using OrderService.DAL.Entities.Abstract;

namespace OrderService.DAL.Entities
{
    /// <summary>
    /// Сущность ордера пользователя - содержит информацию об определенном наборе акций пользователя
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор акции
        /// </summary>
        public int StockId { get; set; }

        /// <summary>
        /// Акция
        /// </summary>
        public Stock Stock { get; set; }

        /// <summary>
        /// Количество акций
        /// </summary>
        public uint Count { get; set; }

        /// <summary>
        /// Дата приобретения
        /// </summary>
        public DateTime OwnDate { get; set; }

        /// <summary>
        /// Цена покупки
        /// </summary>
        public Double Price { get; set; }
    }
}
