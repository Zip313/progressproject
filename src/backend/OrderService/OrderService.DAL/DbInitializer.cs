﻿using Microsoft.EntityFrameworkCore;

namespace OrderService.DAL
{
    /// <summary>
    /// Инициализация БД
    /// </summary>
    public static class DbInitializer
    {
        /// <summary>
        /// Инициализация БД
        /// </summary>
        public static void Initialize(this OrdersContext context)
        {
            context.Database.Migrate();
        }
    }
}
