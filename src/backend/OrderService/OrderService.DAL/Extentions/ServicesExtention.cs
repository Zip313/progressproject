﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OrderService.DAL.Interfaces;

namespace OrderService.DAL.Extentions
{
    public static class ServicesExtention
    {
        public static IServiceCollection AddDAL(this IServiceCollection services, IConfiguration configuration) 
        {
            var user = Environment.GetEnvironmentVariable("DB_USR");
            var password = Environment.GetEnvironmentVariable("DB_PSW");
            var connectionString = configuration["DbConnection"];
            connectionString = connectionString.Replace(@"<user>", user);
            connectionString = connectionString.Replace(@"<password>", password);
            services.AddDbContext<OrdersContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            services.AddScoped<IOrdersContext>(provider => provider.GetService<OrdersContext>()!);
            return services;
        }

        /// <summary>
        /// Вызов инициализации БД
        /// </summary>
        /// <param name="serviceProvider"></param>
        public static void InitializeDb(this IServiceProvider serviceProvider)
        {
            var ctx = serviceProvider.GetRequiredService<OrdersContext>();
            DbInitializer.Initialize(ctx);
        }
    }
}
