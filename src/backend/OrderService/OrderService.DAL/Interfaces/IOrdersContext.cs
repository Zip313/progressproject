﻿using Microsoft.EntityFrameworkCore;
using OrderService.DAL.Entities;

namespace OrderService.DAL.Interfaces
{
    /// <summary>
    /// Интерфейс контекста ордеров
    /// </summary>
    public interface IOrdersContext
    {
        /// <summary>
        /// Коллекция дивидендов
        /// </summary>
        DbSet<Dividend> Dividends { get; set; }
        
        /// <summary>
        /// Коллекция акций
        /// </summary>
        DbSet<Stock> Stocks { get; set; }
        
        /// <summary>
        /// Коллекция ордеров
        /// </summary>
        DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Сохранение данных в БД
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
