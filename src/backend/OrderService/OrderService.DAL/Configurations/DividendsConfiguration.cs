﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.DAL.Entities;

namespace OrderService.DAL.Configurations
{
    /// <summary>
    /// Конфигурация сущностей дивиденда
    /// </summary>
    public class DividendsConfiguration : IEntityTypeConfiguration<Dividend>
    {
        /// <summary>
        /// Метод конфигурации
        /// </summary>
        /// <param name="builder">EntityTypeBuilder</param>
        public void Configure(EntityTypeBuilder<Dividend> builder)
        {
            builder.HasKey(d => d.Id);
            builder.HasOne(d => d.Stock);
            builder.HasIndex(d => d.StockId);
            //builder.HasAlternateKey(d => d.StockId);
        }
    }
}
