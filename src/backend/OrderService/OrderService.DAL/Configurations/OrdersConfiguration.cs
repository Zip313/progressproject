﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.DAL.Entities;

namespace OrderService.DAL.Configurations
{
    /// <summary>
    /// Конфигурация сущностей ордеров
    /// </summary>
    public class OrdersConfiguration : IEntityTypeConfiguration<Order>
    {
        /// <summary>
        /// Метод конфигурации
        /// </summary>
        /// <param name="builder">EntityTypeBuilder</param>
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(o => o.Id);
            builder.HasIndex(o => o.UserId);
            builder.HasIndex(o => o.StockId);
        }
    }
}
