﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.DAL.Configurations
{
    /// <summary>
    /// Конфигурация сущностей акций
    /// </summary>
    public class StocksConfiguration : IEntityTypeConfiguration<Stock>
    {
        /// <summary>
        /// Метод конфигурации
        /// </summary>
        /// <param name="builder">EntityTypeBuilder</param>
        public void Configure(EntityTypeBuilder<Stock> builder)
        {
            builder.HasKey(s => s.Id);
            builder.HasMany(s => s.Dividends);
            builder.HasIndex(s => s.SecId);
        }
    }
}
