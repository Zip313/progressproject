﻿using Microsoft.EntityFrameworkCore;
using OrderService.DAL.Configurations;
using OrderService.DAL.Entities;
using OrderService.DAL.Interfaces;

namespace OrderService.DAL
{
    public class OrdersContext : DbContext, IOrdersContext
    {
        public OrdersContext(DbContextOptions options) : base(options) { }
        public DbSet<Dividend> Dividends { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Order> Orders { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DividendsConfiguration());
            modelBuilder.ApplyConfiguration(new StocksConfiguration());
            modelBuilder.ApplyConfiguration(new OrdersConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
