﻿namespace OrderService.BLL.Settings
{
    public class CronSettings
    {
        public string Every2MinutesEveryHourFrom7To18EveryDayMonToFri { get; set; }
        public string EveryHourFrom7To18EveryDayMonToFri { get; set; }
        public string At12hEveryDayMonToFri { get; set; }
        public string Every5min { get; set; }
        public string Every1min { get; set; }
    }
}
