﻿using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OrderService.BLL.Jobs;
using OrderService.BLL.Services;
using OrderService.DAL.Extentions;
using System.Reflection;

namespace OrderService.BLL.Extentions
{
    public static class ServicesExtention
    {
        public static IServiceCollection AddBll(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDAL(configuration);

            #region Mapster DI
            var typeAdapterConfig = TypeAdapterConfig.GlobalSettings;
            // scans the assembly and gets the IRegister, adding the registration to the TypeAdapterConfig
            typeAdapterConfig.Scan(Assembly.GetExecutingAssembly());
            // register the mapper as Singleton service for my application
            var mapperConfig = new Mapper(typeAdapterConfig);
            services.AddSingleton<IMapper>(mapperConfig);
            #endregion

            #region Loki DI
            
            services.AddLogging(builder =>
            {
                builder.AddLoki(options =>
                {
                    options.Http = new LokiLoggingProvider.Options.HttpOptions()
                    {
                        Address = "http://pias-loki:3100"
                    };
                    options.Client = LokiLoggingProvider.Options.PushClient.Http;
                    options.StaticLabels.JobName = "PIAS_OrderService";
                    options.Formatter = LokiLoggingProvider.Options.Formatter.Json;
                });
                
                builder.AddConsole();
            });
            #endregion
            services.AddTransient<JobFactory>();
            services.AddScoped<UpdateStocksJob>();
            services.AddScoped<UpdateDividendsJob>();

            services.AddGrpc();


            return services;
        }

        /// <summary>
        /// Добавление маппинга GRPC
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static WebApplication AddBLLMapGrpc(this WebApplication app)
        {
            app.MapGrpcService<OrdersService>();
            app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client.");

            return app;
        }

        public static WebApplication InitializeDB(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                try
                {
                    serviceProvider.InitializeDb();
                    Console.WriteLine("!!!!!!DB init is OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("!!!!!!DB init is not OK " + ex.Message);
                }
            }
            return app;
        }
    }
}
