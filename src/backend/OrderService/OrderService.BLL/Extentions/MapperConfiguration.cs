﻿using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Mapster;
using OrderService.DAL.Entities;

namespace OrderService.BLL.Extentions
{
    public class MapperConfiguration : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            TypeAdapterConfig.GlobalSettings.Default
                .UseDestinationValue(member => member.SetterModifier == AccessModifier.None &&
                                   member.Type.IsGenericType &&
                                   member.Type.GetGenericTypeDefinition() == typeof(RepeatedField<>));

            config.NewConfig<Dividend, DividendDto>()
                .Map(dest => dest.DividendId, src => src.Id)
                .Map(dest => dest.RegistryCloseDate, src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.RegistryCloseDate, DateTimeKind.Utc)));

            // TODO: IsIncrease?IncreaseValuePercent?
            config.NewConfig<Stock, StockDto>()
                .Map(dest => dest.StockId, src => src.Id)
                //.Map(dest => dest.Dividends, src => src.Dividends.ConvertAll(d=>d.Adapt<DividendDto>()))
                .Map(dest => dest.ModifiedDate, src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.ModifiedDate, DateTimeKind.Utc)));
                //.Map(dest => dest.IsIncrease, src => true)
                //.Map(dest => dest.IncreaseValuePercent, src => 0);

            config.NewConfig<Order, OrderDto>()
                .Map(dest => dest.OrderId, src => src.Id)
                .Map(dest => dest.Stock, src => src.Stock)
                .Map(dest => dest.Stock.IsIncrease, src => src.Stock.PrevPrice >= src.Price)
                .Map(dest => dest.Stock.IncreaseValuePercent, src => src.Stock.PrevPrice / src.Price)
                .Map(dest => dest.OwnDate, src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.OwnDate, DateTimeKind.Utc)))
                .Map(dest => dest.CostAmount, src => src.Count * src.Price)
                .Map(dest => dest.CurrentAmount, src => src.Count * src.Stock.PrevPrice)
                .Map(dest => dest.IsIncrease, src => src.Stock.PrevPrice >= src.Price)
                .Map(dest => dest.IncreaseValue, src => (src.Price - src.Stock.PrevPrice) * src.Count);
        }
    }
}
