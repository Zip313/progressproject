﻿using OrderService.DAL.Entities;
using System.Text.Json;

namespace OrderService.BLL.Models
{
    public class DividendsRootMoexDto
    {
        public DividendsMoexDto dividends { get; set; }
    }

    public class DividendsMoexDto
    {
        public string[] columns { get; set; }
        public object[][] data { get; set; }

        public List<Dividend> GetDividends(Stock stock)
        {
            List<Dividend> dividends = new List<Dividend>();

            foreach (var item in data)
            {
                dividends.Add(new Dividend()
                {
                    SecId = item[0].ToString(),
                    StockId = stock.Id,
                    Stock = stock,
                    RegistryCloseDate = DateTime.Parse(item[1].ToString()),
                    Value = JsonSerializer.Deserialize<double>(item[2].ToString()),//double.Parse(item[2].ToString().Replace('.', ',')),
                    CurrencyId = item[3].ToString(),
                }) ;
            }
            return dividends;
        }
    }

}
