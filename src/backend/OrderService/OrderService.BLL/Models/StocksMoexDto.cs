﻿using OrderService.DAL.Entities;
using System.Text.Json;

namespace OrderService.BLL.Models
{
    public class StocksMoexDto
    {
        public Securities securities { get; set; }
    }

    public class Securities
    {
        public string[] columns { get; set; }
        public object[][] data { get; set; }

        public List<Stock> GetStocks()
        {
            List<Stock> stocks = new List<Stock>();

            foreach (var item in data)
            {
                stocks.Add(new Stock()
                {
                    SecId = item[0].ToString(),
                    ShortName = item[1].ToString(),
                    PrevPrice = JsonSerializer.Deserialize<double>(item[2].ToString()), //double.Parse(item[2].ToString().Replace('.', ',')),
                    CurrencyId = item[4].ToString(),
                    ModifiedDate = DateTime.Now,
                });
            }
            return stocks;
        }
    }
}
