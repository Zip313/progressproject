﻿using Grpc.Core;

namespace OrderService.BLL.Intetrfaces
{
    public interface IOrdersService
    {
        Task<OrdersResponse> GetOrdersByUserId(GetOrdersByUserIdRequest request, ServerCallContext context);

        Task<OrdersResponse> AddOrder(AddOrderRequest request, ServerCallContext context);

        Task<OrdersResponse> UpdateOrder(UpdateOrderRequest request, ServerCallContext context);

        Task<OrdersResponse> DeleteOrder(DeleteOrderRequest request, ServerCallContext context);

        Task<SummaryReportResponse> GetSummary(GetSummaryRequest request, ServerCallContext context);

        Task<ShortStocksResponse> GetStocksShortNames(EmptyDto request, ServerCallContext context);
    }
}
