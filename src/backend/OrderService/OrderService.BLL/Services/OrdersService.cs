﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OrderService.BLL.Intetrfaces;
using OrderService.DAL.Entities;
using OrderService.DAL.Interfaces;
using System.Diagnostics;

namespace OrderService.BLL.Services
{
    public class OrdersService : Orders.OrdersBase, IOrdersService
    {
        private readonly IOrdersContext _context;
        private readonly ILogger<OrdersService> _logger;

        public OrdersService(IOrdersContext context, ILogger<OrdersService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public override async Task<OrdersResponse> GetOrdersByUserId(GetOrdersByUserIdRequest request, ServerCallContext context)
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                _logger.LogInformation("Start GetOrdersByUserId", request);
                var ordersByUser = await _context.Orders
                    .Where(o => o.UserId == request.UserId)
                    .Include(o=>o.Stock)
                    //.ThenInclude(s=>s.Dividends)
                    .ToListAsync();
                var orderByUserDtos = ordersByUser.ConvertAll(o => o.Adapt<OrderDto>());
                OrdersResponse response = new OrdersResponse();
                response.Orders.AddRange(orderByUserDtos);
                
                sw.Stop();
                _logger.LogInformation($"Success GetOrdersByUserId, время {sw.ElapsedMilliseconds}мс", request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error GetOrdersByUserId: {ex.Message}", ex);
                return new OrdersResponse() { ErrorMessage = ex.Message, IsError = true };
            }
        }

        public override async Task<OrdersResponse> AddOrder(AddOrderRequest request, ServerCallContext context)
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                _logger.LogInformation($"Start AddOrder {request.StockShortName}");
                var stock = await _context.Stocks.AsNoTracking().FirstOrDefaultAsync(s => s.ShortName == request.StockShortName);

                if (stock == null)
                {
                    _logger.LogInformation("Can't find stock by shortname", request.StockShortName);
                    throw new ArgumentException($"Некорректное краткое наименование акции {request.StockShortName}");
                }

                Order newOrder = new Order()
                {
                    UserId = request.UserId,
                    StockId = stock.Id,
                    Count = (uint)request.Count,
                    Price = request.Price,
                    OwnDate = request.OwnDate.ToDateTime(),
                };
                _context.Orders.Add(newOrder);
                await _context.SaveChangesAsync(default);
                OrdersResponse response = new OrdersResponse()
                {
                    IsError = false,
                    ErrorMessage = string.Empty,
                };
                newOrder.Stock = stock;
                var orderDto = newOrder.Adapt<OrderDto>();
                response.Orders.Add(orderDto);
                
                sw.Stop();
                _logger.LogInformation($"Success AddOrder, время {sw.ElapsedMilliseconds}мс", request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error AddOrder: {ex.Message}", ex);
                return new OrdersResponse() { ErrorMessage = ex.Message, IsError = true };
            }
        }

        public override async Task<OrdersResponse> UpdateOrder(UpdateOrderRequest request, ServerCallContext context)
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                _logger.LogInformation("Start UpdateOrder", request);
                var stock = await _context.Stocks.AsNoTracking().FirstOrDefaultAsync(s => s.ShortName == request.StockShortName);

                if (stock == null)
                {
                    throw new ArgumentException($"Некорректное краткое наименование акции {request.StockShortName}");
                }

                var updatedOrder = await _context.Orders.FirstOrDefaultAsync(o => o.Id == request.OrderId);
                
                if (updatedOrder == null)
                {
                    throw new ArgumentException($"Некорректный идентификатор ордера {request.StockShortName}");
                }

                updatedOrder.StockId = stock.Id;
                updatedOrder.Stock = stock;
                updatedOrder.Count = (uint)request.Count;
                updatedOrder.Price = request.Price;
                updatedOrder.OwnDate = request.OwnDate.ToDateTime();
                
                await _context.SaveChangesAsync(default);
                OrdersResponse response = new OrdersResponse()
                {
                    IsError = false,
                    ErrorMessage = string.Empty,
                };
                response.Orders.Add(updatedOrder.Adapt<OrderDto>());
            
                sw.Stop();
                _logger.LogInformation($"Success UpdateOrder, время {sw.ElapsedMilliseconds}мс", request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error UpdateOrder: {ex.Message}", ex);
                return new OrdersResponse() { ErrorMessage = ex.Message, IsError = true };
            }
        }

        public override async Task<OrdersResponse> DeleteOrder(DeleteOrderRequest request, ServerCallContext context)
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                _logger.LogInformation("Start DeleteOrder", request);
                var deleteOrder = _context.Orders.Where(o => o.Id == request.OrderId);

                _context.Orders.RemoveRange(deleteOrder);
                var deleteResult = await _context.SaveChangesAsync(default);
                if (deleteOrder == default)
                {
                    throw new ArgumentException($"Некорректный идентификатор ордера {request.OrderId}");
                }
                OrdersResponse response = new OrdersResponse() 
                {
                    IsError = false,
                    ErrorMessage = string.Empty,
                };
                
                sw.Stop();
                _logger.LogInformation($"Success DeleteOrder, время {sw.ElapsedMilliseconds}мс", request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error DeleteOrder: {ex.Message}", ex);
                return new OrdersResponse() { ErrorMessage = ex.Message, IsError = true };
            }
        }

        public override async Task<SummaryReportResponse> GetSummary(GetSummaryRequest request, ServerCallContext context)
        {
            Stopwatch sw = Stopwatch.StartNew();
            _logger.LogInformation("Start GetSummary", request);
            var myOrders = await _context.Orders.AsNoTracking().Where(o => o.UserId == request.UserId).ToListAsync();
            var stocks = await _context.Stocks
                .AsNoTracking()
                .Include(s=>s.Dividends.Where(d=>d.RegistryCloseDate >= DateTime.Now.AddDays(-7)))
                .ToListAsync();

            List<SummaryReportDto> summaryReportDtos = new List<SummaryReportDto>();

            foreach (var stock in stocks)
            {
                SummaryReportDto summary = new SummaryReportDto();
                summary.SecId = stock.SecId;
                summary.StockShortName = stock.ShortName;
                summary.CurrentPrice = stock.PrevPrice;
                Order order = myOrders.FirstOrDefault(o => o.StockId == stock.Id);
                if(order != null)
                {
                    summary.IsMy = true;
                    summary.Count = (int)order.Count;
                    summary.UserPrice = order.Price;
                    summary.UserAmount = order.Price * order.Count;
                    summary.CurrentAmount = stock.PrevPrice * order.Count;
                    summary.CurrentAmountComission = summary.CurrentAmount * 0.003;
                    summary.CurrentAmountPure = summary.CurrentAmount - summary.CurrentAmountComission;
                    summary.SaleProfit = summary.CurrentAmountPure - summary.UserAmount;
                    summary.SaleProfitPercent = summary.SaleProfit / summary.CurrentAmount;
                    summary.IsProfit = summary.SaleProfit > 0;
                }
                var dividend = stock.Dividends.FirstOrDefault();
                if(dividend != null)
                {
                    summary.DividendsValue = dividend.Value;
                    summary.DividendsRegistryCloseDate = Timestamp.FromDateTime(DateTime.SpecifyKind(dividend.RegistryCloseDate, DateTimeKind.Utc));
                    summary.DividendsValuePercent = dividend.Value / summary.CurrentPrice;
                    if(order != null)
                    {
                        summary.DividendsAmount = dividend.Value * order.Count;
                        summary.DividendsAmountTax = summary.DividendsAmount * 0.13;
                        summary.DividendsAmountPure = summary.DividendsAmount - summary.DividendsAmountTax;
                    }
                }

                summaryReportDtos.Add(summary);
            }

            SummaryReportResponse summaryReportResponse = new SummaryReportResponse();
            summaryReportResponse.Data.AddRange(summaryReportDtos.OrderByDescending(r => r.IsMy).ThenBy(r => r.SecId).ToList());
            
            sw.Stop();
            _logger.LogInformation($"Success GetSummary, время {sw.ElapsedMilliseconds}мс", request);
            return summaryReportResponse;
        }

        public override async Task<ShortStocksResponse> GetStocksShortNames(EmptyDto request, ServerCallContext context)
        {
            Stopwatch sw = Stopwatch.StartNew();
            _logger.LogInformation("Start GetStocksShortNames", request);
            var StocksNames = await _context.Stocks
                .AsNoTracking()
                .Select(s => new ShortStockDto 
                { 
                    SecId = s.SecId, 
                    ShortName = s.ShortName,
                    Price = s.PrevPrice
                })
                .ToListAsync();
            var response = new ShortStocksResponse();
            response.ShortStocks.AddRange(StocksNames);

            sw.Stop();
            _logger.LogInformation($"Success GetStocksShortNames, время {sw.ElapsedMilliseconds}мс", request);
            return response;
        }
    }
}
