﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OrderService.BLL.Models;
using OrderService.DAL.Interfaces;
using Quartz;
using System.Diagnostics;
using System.Text.Json;

namespace OrderService.BLL.Jobs
{
    public class UpdateStocksJob : IJob
    {
        private readonly IServiceScopeFactory serviceScopeFactory;

        public UpdateStocksJob(IServiceScopeFactory serviceScopeFactory)
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }


        public async Task Execute(IJobExecutionContext context)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var _logger = scope.ServiceProvider.GetService<ILogger<UpdateStocksJob>>();
                _logger.LogInformation("Start execute UpdateStocksJob");

                var _httpClientFactory = scope.ServiceProvider.GetService<IHttpClientFactory>();
                var _ordersContext = scope.ServiceProvider.GetService<IOrdersContext>();

                using HttpClient client = _httpClientFactory.CreateClient();

                try
                {
                    var response = await client.GetAsync(
                        "https://iss.moex.com/iss/engines/stock/markets/shares/boards/tqbr/securities.json?iss.only=securities&iss.meta=off&securities.columns=SECID,SHORTNAME,PREVPRICE,SECNAME,CURRENCYID"
                        );
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception("Сервер не отвечает");
                    }

                    var content = await response.Content.ReadAsStringAsync();
                    var root = JsonSerializer.Deserialize<StocksMoexDto>(content);

                    var newStocks = root.securities.GetStocks();

                    stopwatch.Stop();
                    _logger.LogInformation($"Получены данные биржи по акциям({newStocks.Count}), время:{stopwatch.ElapsedMilliseconds}мс", newStocks);
                    stopwatch.Restart();

                    var stocks = await _ordersContext.Stocks.ToListAsync();
                    foreach (var stock in stocks)
                    {
                        var newStock = newStocks.FirstOrDefault(x => x.SecId == stock.SecId && x.PrevPrice != stock.PrevPrice);
                        if (newStock == null) { continue; }
                        stock.PrevPrice = newStock.PrevPrice;
                        stock.CurrencyId = newStock.CurrencyId;
                        stock.ModifiedDate = DateTime.Now;
                    }

                    var missingStocks = newStocks.Where(s => !stocks.Select(x => x.SecId).Contains(s.SecId));
                    _ordersContext.Stocks.AddRange(missingStocks);

                    var cnt = await _ordersContext.SaveChangesAsync(default);
                    stopwatch.Stop();
                    _logger.LogInformation($"Данные биржи сохранены в БД(всего {cnt} записей изменено, {missingStocks.ToList().Count} добавлено новых), время:{stopwatch.ElapsedMilliseconds}мс", new { missingStocks });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Debug.WriteLine(ex.Message);
                }
            }
        }
    }
}

