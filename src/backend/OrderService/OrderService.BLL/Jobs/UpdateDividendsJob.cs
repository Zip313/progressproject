﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OrderService.BLL.Models;
using OrderService.DAL.Entities;
using OrderService.DAL.Interfaces;
using Quartz;
using Quartz.Logging;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Text.Json;

namespace OrderService.BLL.Jobs
{
    public class UpdateDividendsJob : IJob
    {
        private readonly IServiceScopeFactory serviceScopeFactory;

        public UpdateDividendsJob(IServiceScopeFactory serviceScopeFactory)
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            Stopwatch stopwatch = Stopwatch.StartNew();
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var _logger = scope.ServiceProvider.GetService<ILogger<UpdateDividendsJob>>();
                _logger.LogInformation("Start execute UpdateDividendsJob");

                var _httpClientFactory = scope.ServiceProvider.GetService<IHttpClientFactory>();
                var _ordersContext = scope.ServiceProvider.GetService<IOrdersContext>();
                var stocks = await _ordersContext.Stocks.Include(s=>s.Dividends).ToListAsync();
                var oldDividends = stocks.SelectMany(s => s.Dividends).ToList();
                ConcurrentBag<Dividend> dividends = new ConcurrentBag<Dividend>();

                List<Task> tasks = new List<Task>();
                foreach (var stock in stocks)
                {
                    tasks.Add(UpdateStockDividends(stock, dividends, _httpClientFactory));
                    
                }
                await Task.WhenAll(tasks.ToArray());
                stopwatch.Stop();
                _logger.LogInformation($"Получена информация по дивидендам {dividends.Count}, время:{stopwatch.ElapsedMilliseconds}мс",dividends);
                stopwatch.Restart();
                
                var addDividends =  dividends.Where(d=>!oldDividends.Any(od=>od.StockId == d.StockId && od.RegistryCloseDate == d.RegistryCloseDate)).ToList();
                _ordersContext.Dividends.AddRange(addDividends);
                
                var updateDividends = dividends.Where(d => oldDividends
                    .Any(od => od.StockId == d.StockId && od.RegistryCloseDate == d.RegistryCloseDate && (od.Value != d.Value || od.CurrencyId != d.CurrencyId ))
                    ).ToList();
                foreach (var item in updateDividends)
                {
                    var dividend = oldDividends.FirstOrDefault(x => x.StockId == item.StockId && x.RegistryCloseDate == item.RegistryCloseDate);
                    dividend.Value = item.Value;
                    dividend.CurrencyId = item.CurrencyId;
                }
                
                var removeDividends = oldDividends.Where(d => dividends.All(od => od.StockId != d.StockId && od.RegistryCloseDate != d.RegistryCloseDate)).ToList();
                _ordersContext.Dividends.RemoveRange(removeDividends);
                
                stopwatch.Stop();
                _logger.LogInformation($"Подготовлены данные по добавлению({addDividends.Count}), удалению({removeDividends.Count}), обновлению дивидендов({updateDividends.Count}) {dividends.Count}, время:{stopwatch.ElapsedMilliseconds}мс", new { addDividends, removeDividends, updateDividends });
                stopwatch.Restart();

                var cnt = await _ordersContext.SaveChangesAsync(default);
                
                stopwatch.Stop();
                _logger.LogInformation($"Данные записаны в БД({cnt} записей изменено), время:{stopwatch.ElapsedMilliseconds}мс, работа завершена");
            }
        }

        private async Task UpdateStockDividends(Stock stock, ConcurrentBag<Dividend> dividends, IHttpClientFactory _httpClientFactory)
        {
            using HttpClient client = _httpClientFactory.CreateClient();

            try
            {
                var response = await client.GetAsync(
                $@"https://iss.moex.com/iss/securities/{stock.SecId}/dividends.json?dividends.columns=secid,registryclosedate,value,currencyid&iss.meta=off"
                );
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Сервер не отвечает");
                }

                var content = await response.Content.ReadAsStringAsync();
                var root = JsonSerializer.Deserialize<DividendsRootMoexDto>(content);
                var newDividends = root.dividends.GetDividends(stock);
                foreach (var item in newDividends)
                {
                    dividends.Add(item);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
