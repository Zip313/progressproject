﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OrderService.BLL.Settings;
using OrderService.DAL.Interfaces;
using Quartz;
using Quartz.Impl;

namespace OrderService.BLL.Jobs.Schedulers
{
    public class UpdateStocksScheduler
    {
        public static async void Start(IServiceProvider serviceProvider)
        {
            var cronSettings = serviceProvider.GetService<IConfiguration>().Get<ApplicationSettings>().CronSettings;
            
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            scheduler.JobFactory = serviceProvider.GetService<JobFactory>();
            var _logger = serviceProvider.GetService<ILogger<UpdateStocksScheduler>>();
            await scheduler.Start();

            IJobDetail stocksJob = JobBuilder.Create<UpdateStocksJob>().Build();
            ITrigger triggerStocks = CreateTrigger("TriggerStocks", cronSettings.Every2MinutesEveryHourFrom7To18EveryDayMonToFri);
            _logger.LogInformation($"CreateTrigger TriggerStocks {cronSettings.Every2MinutesEveryHourFrom7To18EveryDayMonToFri}");

            IJobDetail dividendsJob = JobBuilder.Create<UpdateDividendsJob>().Build();
            ITrigger triggerDividends = CreateTrigger("TriggerDividends", cronSettings.EveryHourFrom7To18EveryDayMonToFri);
            _logger.LogInformation($"CreateTrigger TriggerDividends {cronSettings.EveryHourFrom7To18EveryDayMonToFri}");

            await scheduler.ScheduleJob(stocksJob, triggerStocks);
            await scheduler.ScheduleJob(dividendsJob, triggerDividends);
        }

        private static ITrigger CreateTrigger(string name, string cronExpression)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{name}.trigger")
                .WithCronSchedule(cronExpression)
                .WithDescription(cronExpression)
                .Build();
        }
    }
}
