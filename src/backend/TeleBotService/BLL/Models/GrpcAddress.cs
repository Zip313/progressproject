﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class GrpcAddress
    {
        public string GrpcUsersServerAddress { get; set; }

        public string GrpcOrdersServerAddress { get; set; }
    }
}
