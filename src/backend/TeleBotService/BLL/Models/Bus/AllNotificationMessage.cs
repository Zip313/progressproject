﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.Bus
{
    public class AllNotificationMessage
    {
        public NotificationTypes Type { get; set; }

        public string? Message { get; set; }
    }
}
