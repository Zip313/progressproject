﻿using BLL.Models;
using BLL.Services.Abstract;
using Grpc.Net.Client;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeleBotService;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly Users.UsersClient _usersClient;
        private readonly GrpcAddress requestURIconfig;

        public UserService(IOptions<GrpcAddress> configuration) 
        {
            string? grpcUsersServerAddress = null;
            grpcUsersServerAddress = Environment.GetEnvironmentVariable("GRPC_USERSSERVICE_ADDRESS");
            grpcUsersServerAddress = grpcUsersServerAddress == null ? configuration.Value.GrpcUsersServerAddress : grpcUsersServerAddress;
            
            var channel = GrpcChannel.ForAddress(grpcUsersServerAddress);
            _usersClient = new Users.UsersClient(channel);
        }

        public async Task<UserResponse> GetUserByCredentialsAsync(UserCredentials request)
        {
            var response = await _usersClient.GetUserByCredentialsAsync(request);
            return response;
        }

        public async Task<UserResponse> GetUserByIdAsync(UserRequestById request)
        {
            var response = await _usersClient.GetUserByIdAsync(request);
            return response;
        }

        public async Task<UserResponse> AddUserAsync(UserCredentials request)
        {
            var response = await _usersClient.AddUserAsync(request);
            return response;
        }

        public async Task<ActivationResponse> ActivateUserAsync(ActivateRequest request)
        {
            var response = await _usersClient.ActivateUserAsync(request);
            return response;
        }
    }
}
