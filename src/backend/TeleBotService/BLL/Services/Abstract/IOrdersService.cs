﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeleBotService;

namespace BLL.Services.Abstract
{
    public interface IOrdersService
    {
        Task<List<ShortStockDto>> GetStockInfoAsync();

        Task<List<OrderDto>> GetUserPortfolioAsync(int userCommonId);

        Task<List<SummaryReportDto>> GetSummaryAnalysisAsync(int userCommonId);
    }
}
