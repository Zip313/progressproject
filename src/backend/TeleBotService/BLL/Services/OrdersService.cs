﻿using BLL.Models;
using BLL.Services.Abstract;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeleBotService;
using static TeleBotService.Users;

namespace BLL.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly Orders.OrdersClient _ordersClient;
        private readonly GrpcAddress requestURIconfig;

        public OrdersService(IOptions<GrpcAddress> configuration)
        {
            string? grpcOrdersServerAddress = null;
            grpcOrdersServerAddress = Environment.GetEnvironmentVariable("GRPC_ORDERSSERVICE_ADDRESS");
            grpcOrdersServerAddress = grpcOrdersServerAddress == null ? configuration.Value.GrpcOrdersServerAddress : grpcOrdersServerAddress;

            var channel = GrpcChannel.ForAddress(grpcOrdersServerAddress);
            _ordersClient = new Orders.OrdersClient(channel);
            //var channel = GrpcChannel.ForAddress("");
            //_ordersClient = new Orders.OrdersClient(channel);
        }

        public async Task<List<ShortStockDto>> GetStockInfoAsync()
        {
            var response = _ordersClient.GetStocksShortNames(new EmptyDto());
            var res = response.ShortStocks.ToList();
            return res;

            ////тест
            //var response = new StocksShortNamesDto();
            //response.StockShortName.AddRange(new string[] { "MTSS", "SUR", "GZP" });
            //return response;
        }

        public async Task<List<OrderDto>> GetUserPortfolioAsync(int userCommonId)
        {
            var response = _ordersClient.GetOrdersByUserId(new GetOrdersByUserIdRequest()
            {
                UserId = userCommonId
            });
            if (response.IsError)
            {
                throw new Exception(response.ErrorMessage);
            }
            return response.Orders.ToList();

            ////тест
            //var order_response = new OrdersResponse()
            //{
            //    IsError = false,
            //    ErrorMessage = string.Empty
            //};
            //var order = new OrderDto()
            //{
            //    CurrentAmount = 1000,
            //    CostAmount = 1000,
            //    Count = 10,
            //    IsIncrease = true,
            //    OwnDate = Timestamp.FromDateTime(DateTime.UtcNow),
            //    Price = 100,
            //    Stock = new StockDto()
            //    {
            //        StockId = 2234,
            //        SecId = "MTSS",
            //        CurrencyId = "SUR",
            //        PrevPrice = 1,
            //        IsIncrease = true,
            //        ShortName = "МТС",
            //        IncreaseValuePercent = 0,
            //        ModifiedDate = Timestamp.FromDateTime(DateTime.UtcNow),
            //    }
            //};
            //order_response.Orders.Add(order);

            //return await Task.FromResult(order_response);
        }

        public async Task<List<SummaryReportDto>> GetSummaryAnalysisAsync(int userCommonId)
        {
            var response = _ordersClient.GetSummary(new GetSummaryRequest()
            {
                UserId = userCommonId
            });
            return response.Data.ToList();

            //тест
            //var response = new SummaryReportDto()
            //{
            //    Count = 3,
            //    CurrentAmount = 1000,
            //    CurrentAmountComission = 100,
            //    CurrentPrice = 100,
            //    CurrentAmountPure = 100,
            //    DividendsAmount = 100,
            //    DividendsAmountPure = 100,
            //    DividendsAmountTax = 100,
            //    DividendsRegistryCloseDate = Timestamp.FromDateTime(DateTime.UtcNow),
            //    DividendsValue = 100,
            //    DividendsValuePercent = 100,
            //    IsMy = true,
            //    IsProfit = true,
            //    SaleProfit = 100,
            //    SaleProfitPercent = 100,
            //    SecId = "MTSS",
            //    StockShortName = "MTSS",
            //    UserAmount = 100,
            //    UserPrice = 100
            //};
            //return response;
        }
    }
}
