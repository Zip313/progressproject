﻿using BLL.Operations.Abstractions;
using BLL.Services.Abstract;
using TeleBotService;
using static TeleBotService.Orders;

namespace BLL.Operations
{
    public class OrdersOperations : IOrdersOperations
    {
        private readonly ILoginOperations loginOperations;
        private readonly IOrdersService ordersService;

        public OrdersOperations(ILoginOperations loginOperations, IOrdersService ordersService)
        {
            this.loginOperations = loginOperations;
            this.ordersService = ordersService;
        }

        public async Task<List<ShortStockDto>> GetStockInfoAsync()
        {
            var response = await ordersService.GetStockInfoAsync();
            return response;
        }

        public async Task<List<OrderDto>> GetUserPortfolioAsync(int userCommonId)
        {
            var response = await ordersService.GetUserPortfolioAsync(userCommonId);
            return response;
        } 
        
        public async Task<List<SummaryReportDto>> GetSummaryAnalysisAsync(int userCommonId)
        {
            var response = await ordersService.GetSummaryAnalysisAsync(userCommonId);
            return response;
        }
    }
}
