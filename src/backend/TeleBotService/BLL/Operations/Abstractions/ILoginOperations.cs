﻿using DAL.Db;
using DAL.Entities;

namespace BLL.Operations.Abstractions
{
    public interface ILoginOperations
    {
        Task AddRegistrationAsync(long chatId);

        Task<DAL.Entities.Registration?> GetRegistrationAsync(long chatId);

        Task RemoveRegistrationAsync(long chatId);

        /// <summary>
        /// Возвращает null, если произошла регистрация, true, если активирован, false, если не активирован
        /// </summary>
        Task<bool?> SignContinueAsync(long chatId, string emailAndPassword);

        Task<bool> CheckUserInAsync(long chatId);

        Task<DAL.Entities.UserDto?> GetUserAsync(long chatId);

        Task<bool> GetIsActivate(DAL.Entities.UserDto userDto);

        Task<List<DAL.Entities.UserDto>> GetActiveUsers();
    }
}
