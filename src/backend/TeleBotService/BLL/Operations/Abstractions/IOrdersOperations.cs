﻿
using TeleBotService;

namespace BLL.Operations.Abstractions
{
    public interface IOrdersOperations
    {
        Task<List<ShortStockDto>> GetStockInfoAsync();

        Task<List<OrderDto>> GetUserPortfolioAsync(int userCommonId);

        Task<List<SummaryReportDto>> GetSummaryAnalysisAsync(int userCommonId);
    }
}
