﻿using BLL.Models;
using BLL.Operations.Abstractions;
using BLL.Services;
using BLL.Services.Abstract;
using DAL.Db.Abstractions;
using DAL.Entities;
using DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using TeleBotService;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BLL.Operations
{
    public class LoginOperations : ILoginOperations
    {
        private readonly IDbProvider dbProvider;
        private readonly IUserService userService;

        public LoginOperations(IDbProvider dbProvider, IUserService userService)
        {
            this.dbProvider = dbProvider;
            this.userService = userService;
        }

        public async Task AddRegistrationAsync(long chatId)
        {
            await dbProvider.AddRegistrationAsync(new DAL.Entities.Registration()
            {
                ChatId = chatId
            });
        }

        public async Task<DAL.Entities.Registration?> GetRegistrationAsync(long chatId)
        {
            var reg = await dbProvider.GetRegistrationAsync(chatId);
            return reg;
        }

        public async Task RemoveRegistrationAsync(long chatId)
        {
            var reg = await GetRegistrationAsync(chatId);
            if (reg == null)
            {
                return;
            }
            await dbProvider.RemoveRegistrationAsync(reg);
        }

        /// <summary>
        /// Возвращает null, если произошла регистрация, true, если активирован, false, если не активирован
        /// </summary>
        public async Task<bool?> SignContinueAsync(long chatId, string emailAndPassword)
        {
            var data = emailAndPassword.Split(" ");

            if (data.Count() != 2 || string.IsNullOrWhiteSpace(data[0]) 
                || !Regex.IsMatch(data[0], @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)) 
                || string.IsNullOrWhiteSpace(data[1]))
            {
                throw new ArgumentException();
            }

            var registration = await dbProvider.GetRegistrationAsync(chatId); //у registrationInfo есть только ChatId
            if (registration == null)
            {
                return await SignInUserAsync(chatId, data[0], data[1]);
            }
            else
            {
                await SignUpUserAsync(chatId, data[0], data[1]);
                return null;
            }
        }

        public async Task<bool> CheckUserInAsync(long chatId)
        {
            var userDto = await dbProvider.GetUserAsync(chatId);
            return userDto != null;
        }

        public async Task<DAL.Entities.UserDto?> GetUserAsync(long chatId)
        {
            var userDto = await dbProvider.GetUserAsync(chatId);
            return userDto;
        }

        public async Task<bool> GetIsActivate(DAL.Entities.UserDto userDto)
        {
            var user = await CheckUserCommonByIdAsync(userDto.ChatId);
            var changedUser = new DAL.Entities.UserDto()
            {
                ChatId = userDto.ChatId,
                IsActivated = user.IsActivated
            };
            await dbProvider.ChangeUserAsync(changedUser);
            return changedUser.IsActivated;
        }

        public async Task<List<DAL.Entities.UserDto>> GetActiveUsers()
        {
            var users = await dbProvider.GetActiveUsersAsync();
            return users;
        }

        private async Task<bool> SignInUserAsync(long chatId, string email, string password)
        {
            //InvalidOperationException при возвращении ошибки
            var user = await CheckUserCommonAsync(email, password);
            var userDto = new DAL.Entities.UserDto()
            {
                ChatId = chatId,
                Email = email,
                UserIdCommon = user.Id,
                IsActivated = user.IsActivated
            };
            await dbProvider.AddUserAsync(userDto);
            return userDto.IsActivated;
        }

        private async Task<bool> SignUpUserAsync(long chatId, string email, string password) 
        {
            try
            {
                //InvalidOperationException при возвращении ошибки
                var user = await CheckUserCommonAsync(email, password);

                throw new InvalidCredentialException("Пользователь уже зарегистрирован, войдите в аккаунт");
            }
            catch (InvalidOperationException) //плановая регистрация
            {
                var userDto = await dbProvider.GetUserAsync(chatId);
                if (userDto == null)
                {
                    var userResponse = await userService.AddUserAsync(new UserCredentials()
                    {
                        Email = email,
                        Password = password
                    });
                    if (userResponse.IsError)
                    {
                        throw new Exception(userResponse.ErrorMessage);
                    }
                    await dbProvider.AddUserAsync(new DAL.Entities.UserDto()
                    {
                        ChatId = chatId,
                        Email = email,
                        IsActivated = userResponse.User.IsActivated,
                        UserIdCommon = userResponse.User.Id
                    });
                    await RemoveRegistrationAsync(chatId);
                    return true;
                }
                else //нет в юзерах, есть в локале. Не должно быть такой ситуации
                {
                    throw new InvalidCredentialException("Пользователь зарегистрирован только в боте, непредвиденная ошибка");
                }
            }
        }

        private async Task<TeleBotService.UserDto> CheckUserCommonAsync(string email, string password)
        {
            var user = await userService.GetUserByCredentialsAsync(new UserCredentials()
            {
                Email = email,
                Password = password
            });
            if (user.IsError)
            {
                throw new InvalidOperationException(user.ErrorMessage);
            };
            return user.User;
        }

        private async Task<TeleBotService.UserDto> CheckUserCommonByIdAsync(long chatId)
        {
            var userDto = await GetUserAsync(chatId);
            if (userDto == null || userDto.UserIdCommon == null)
            {
                return null;
            }
            var user = await userService.GetUserByIdAsync(new UserRequestById()
            {
                Id = userDto.UserIdCommon.Value
            });
            if (user.IsError)
            {
                throw new InvalidOperationException(user.ErrorMessage);
            };
            return user.User;
        }
    }
}
