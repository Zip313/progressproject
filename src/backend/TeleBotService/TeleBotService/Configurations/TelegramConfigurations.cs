﻿using System.Threading;
using TeleBotService.Configurations.Abstract;
using TeleBotService.Services.Abstract;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TeleBotService.Configurations
{
    public class TelegramConfigurations : ITelegramConfigurations
    {
        private readonly IServiceScopeFactory scopeFactory;

        private Mutex mutexObj = new Mutex(true, "bot");

        public TelegramConfigurations(ILogger<TelegramConfigurations> logger, IServiceScopeFactory scopeFactory)
        {
            this.logger = logger;
            logger.LogInformation("Приложение проверяет открытые инстансы данного приложения...");
            var goRun = mutexObj.WaitOne(3000, false);
            if (goRun == false)
            {
                logger.LogError("Приложение не удалось запустить, так как оно уже запущено!");
                Console.Read();
            }
            logger.LogInformation("Приложение запускается...");
            this.scopeFactory = scopeFactory;
            // Присваиваем переменной значение, в параметре передаем Token, полученный от BotFather
            botClient = new TelegramBotClient(TelegramMeta.GetInstance().Token);
        }

        ~TelegramConfigurations()
        {
            mutexObj.ReleaseMutex();
        }

        private readonly ILogger<TelegramConfigurations> logger;

        private string token;
        // Это клиент для работы с Telegram Bot API, который позволяет отправлять сообщения, управлять ботом, подписываться на обновления и тп
        public ITelegramBotClient botClient { get; }

        // Это объект с настройками работы бота. Здесь мы будем указывать, какие типы Update мы будем получать, Timeout бота и тп
        private ReceiverOptions _receiverOptions;

        private ITeleBotService teleBotService;

        public async Task Init()
        {
            //присваем значение настройкам бота
            _receiverOptions = new ReceiverOptions
            {
                // указываем типы получаемых Update`ов, подробнее https://core.telegram.org/bots/api#update
                AllowedUpdates = new[]
                {
                    UpdateType.Message, // Сообщения (текст, фото/видео, голосовые/видео сообщения и т.д.)
                },
                // Параметр, отвечающий за обработку сообщений, пришедших за то время, когда ваш бот был оффлайн
                // True - не обрабатывать, False (стоит по умолчанию) - обрабаывать
                ThrowPendingUpdates = true,
            };
            using var cts = new CancellationTokenSource();

            // UpdateHander - обработчик приходящих Update`ов
            // ErrorHandler - обработчик ошибок, связанных с Bot API
            botClient.StartReceiving(UpdateHandler, ErrorHandler, _receiverOptions, cts.Token); // Запускаем бота

            var me = await botClient.GetMeAsync(); // Создаем переменную, в которую помещаем информацию о нашем боте.
            logger.LogInformation($"{me.FirstName} запущен!");
            //Console.WriteLine($"{me.FirstName} запущен!");

            await Task.Delay(-1); // Устанавливаем бесконечную задержку, чтобы наш бот работал постоянно
        }

        /// <summary>
        /// обработчик приходящих Update`ов
        /// </summary>
        private async Task UpdateHandler(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            try
            {
                switch (update.Type)
                {
                    case UpdateType.Message:
                        {
                            logger.LogInformation("Пришло сообщение!");
                            using (var scope = scopeFactory.CreateScope())
                            {
                                var teleBotService = scope.ServiceProvider.GetRequiredService<ITeleBotService>();
                                await teleBotService.MessageProcessing(botClient, update);
                            }
                            return;
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// обработчик ошибок, связанных с Bot API
        /// </summary>
        private Task ErrorHandler(ITelegramBotClient botClient, Exception error, CancellationToken cancellationToken)
        {
            // код ошибки и её сообщение 
            var ErrorMessage = error switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => error.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
