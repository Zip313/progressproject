﻿using Telegram.Bot;

namespace TeleBotService.Configurations.Abstract
{
    public interface ITelegramConfigurations
    {
        ITelegramBotClient botClient { get; }

        Task Init();
    }
}
