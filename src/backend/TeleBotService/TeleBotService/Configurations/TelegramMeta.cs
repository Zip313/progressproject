﻿namespace TeleBotService.Configurations
{
    public class TelegramMeta
    {
        static TelegramMeta instance = null;
        public string Token { get; private set; }

        private TelegramMeta() { }

        public static TelegramMeta GetInstance()
        {
            return instance;
        }

        public static TelegramMeta CreateInstance(string token)
        {
            if (instance == null)
            {
                instance = new TelegramMeta();
                instance.Token = token;
            }
            return instance;
        }
    }
}
