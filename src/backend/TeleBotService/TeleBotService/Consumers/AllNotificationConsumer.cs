﻿using BLL.Models.Bus;
using MassTransit;
using Newtonsoft.Json;
using TeleBotService.Services.Abstract;

namespace TeleBotService.Consumers
{
    public class AllNotificationConsumer : IConsumer<AllNotificationMessage>
    {
        private readonly ITeleBotService teleBotService;

        public AllNotificationConsumer(ITeleBotService teleBotService)
        {
            this.teleBotService = teleBotService;
        }

        public async Task Consume(ConsumeContext<AllNotificationMessage> context)
        {
            await Task.Delay(1000);

            await teleBotService.SendAllNotificationAsync(context.Message.Type);

            var jsonMessage = JsonConvert.SerializeObject(context.Message);
            Console.WriteLine($"AllNotificationConsumer message: {jsonMessage}");
        }
    }
}
