﻿using BLL.Models.Bus;
using BLL.Operations;
using BLL.Operations.Abstractions;
using DAL.Entities;
using MassTransit.Contracts;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Authentication;
using System.Text;
using TeleBotService.Configurations;
using TeleBotService.Configurations.Abstract;
using TeleBotService.Services.Abstract;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace TeleBotService.Services
{
    public class TeleBotService : ITeleBotService
    {
        private readonly ILogger<TeleBotService> logger;
        private readonly ILoginOperations loginOperations;
        private readonly IOrdersOperations ordersOperations;

        private readonly IServiceScopeFactory scopeFactory;
        private ITelegramBotClient botClient { get; set; }


        public TeleBotService(ILogger<TeleBotService> logger, ILoginOperations loginOperations,
            IOrdersOperations ordersOperations, IServiceScopeFactory scopeFactory)
        {
            this.logger = logger;
            this.loginOperations = loginOperations;
            this.ordersOperations = ordersOperations;
            this.scopeFactory = scopeFactory;
        }

        public async Task MessageProcessing(ITelegramBotClient botClient, Telegram.Bot.Types.Update update)
        {
            this.botClient = botClient;
            var commands = update!.Message!.Text.Split(" ");
            switch (commands[0])
            {
                case "/sign_up":
                    {
                        logger.LogInformation("Сообщение sign_up!");
                        await SignUpAsync(update);
                        return;
                    }
                case "/sign_in":
                    {
                        logger.LogInformation("Сообщение sign_in!");
                        await SignInAsync(update);
                        return;
                    }
                case "/stock_info":
                    {
                        logger.LogInformation("Сообщение stock_info!");
                        await GetStockInfoAsync(update);
                        return;
                    }
                case "/my_portfolio":
                    {
                        logger.LogInformation("Сообщение my_portfolio!");
                        await GetUserPortfolioAsync(update);
                        return;
                    }
                case "/get_analysis":
                    {
                        logger.LogInformation("Сообщение get_analysis!");
                        await GetSummaryAnalysisAsync(update);
                        return;
                    }
                default:
                    {
                        logger.LogInformation("Сообщение default!");
                        await SignContinueAsync(update);
                        return;
                    }
            }
        }

        /// <summary>
        /// Запрос рассылки всем пользователям
        /// </summary>
        /// <param name="notificationType">Тип рассылки</param>
        public async Task SendAllNotificationAsync(NotificationTypes notificationType)
        {
            switch (notificationType)
            {
                case NotificationTypes.DailyPortfolio:
                    await SendDailyPortfolioInformationRequestAsync();
                    break;
            }
        }

        /// <summary>
        /// Запрос рассылки информации по портфелям
        /// </summary>
        /// <param name="request">Информация по портфелям</param>
        private async Task SendDailyPortfolioInformationRequestAsync()
        {
            var users = await loginOperations.GetActiveUsers();
            foreach (var user in users)
            {
                await SendUserPortfolioAsync(user);
            }
        }

        private async Task SignUpAsync(Telegram.Bot.Types.Update update)
        {
            var chatId = update.Message!.Chat.Id;
            if (await CheckUserAsync(chatId))
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Вы уже зарегистрированы",
                    replyToMessageId: update.Message.MessageId
                    );
                return;
            }
            await loginOperations.AddRegistrationAsync(chatId);
            await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Для регистрации введите Ваш email и пароль через пробел:",
                    replyToMessageId: update.Message.MessageId
                    );
        }

        private async Task SignInAsync(Telegram.Bot.Types.Update update)
        {
            var chatId = update.Message!.Chat.Id;
            if (await CheckUserAsync(chatId))
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Вы уже вошли в систему",
                    replyToMessageId: update.Message.MessageId
                    );
                return;
            }
            await loginOperations.RemoveRegistrationAsync(chatId);
            await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Для входа в аккаунт введите Ваш email и пароль через пробел:",
                    replyToMessageId: update.Message.MessageId
                    );
        }

        private async Task SignContinueAsync(Telegram.Bot.Types.Update update)
        {
            var chatId = update.Message!.Chat.Id;

            if (await CheckUserAsync(chatId))
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Для работы с ботом используйте команды",
                    replyToMessageId: update.Message.MessageId
                    );
                return;
            }

            try
            {
                var res = await loginOperations.SignContinueAsync(chatId, update.Message!.Text);

                if (res == null)
                {
                    await botClient.SendTextMessageAsync(
                        update.Message.Chat.Id,
                        "Сообщение с инструкцией направлено на Вашу почту. Подтвердите свой email, чтобы воспользоваться услугами бота",
                        replyToMessageId: update.Message.MessageId
                        );
                }
                else if (res == true)
                {
                    await botClient.SendTextMessageAsync(
                       update.Message.Chat.Id,
                       "Можете воспользоваться услугами бота!",
                       replyToMessageId: update.Message.MessageId
                       );
                }
                else
                {
                    await botClient.SendTextMessageAsync(
                       update.Message.Chat.Id,
                       "Вы вошли в систему. Аккаунт не активирован. Подтвердите свой email, чтобы воспользоваться услугами бота",
                       replyToMessageId: update.Message.MessageId
                       );
                }
            }
            catch (ArgumentException)
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Имя пользователя или пароль не верны:",
                    replyToMessageId: update.Message.MessageId
                    );
            }
            catch (InvalidCredentialException ex) //если при обращении к userService возникла ошибка
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    $"Ошибка регистрации: {ex.Message}",
                    replyToMessageId: update.Message.MessageId
                    );
            }
            catch (InvalidOperationException ex) //если при обращении к userService возникла ошибка
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    $"Ошибка обращения к сервису пользователей: {ex.Message}",
                    replyToMessageId: update.Message.MessageId
                    );
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    $"Ошибка: {ex.Message}",
                    replyToMessageId: update.Message.MessageId
                    );
            }
        }

        private async Task GetStockInfoAsync(Telegram.Bot.Types.Update update)
        {
            if (await CheckUserBeforeActionAsync(update) == false)
            {
                return;
            }
            var stocksShortNames = await ordersOperations.GetStockInfoAsync();
            var res = await ReadAnswerListAsync(stocksShortNames);
            await botClient.SendTextMessageAsync(
                update.Message.Chat.Id,
                $"Краткие наименования всех акций на бирже:",
                replyToMessageId: update.Message.MessageId
                );
            foreach (var mes in res)
            {
                await botClient.SendTextMessageAsync(
                update.Message.Chat.Id,
                mes
                );
            }
        }

        private async Task GetUserPortfolioAsync(Telegram.Bot.Types.Update update)
        {
            var user = await loginOperations.GetUserAsync(update.Message.Chat.Id);
            if (user == null || await CheckUserBeforeActionAsync(update) == false)
            {
                return;
            }
            await SendUserPortfolioAsync(user);
        }

        private async Task SendUserPortfolioAsync(DAL.Entities.UserDto userDto)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var teleBotConfig = scope.ServiceProvider.GetRequiredService<ITelegramConfigurations>();
                this.botClient = teleBotConfig.botClient;
            }
            var ordersResponseList = new List<OrderDto>();
            try
            {
                ordersResponseList = await ordersOperations.GetUserPortfolioAsync(userDto.UserIdCommon.Value);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(
                userDto.ChatId,
                $"Ошибка получения информации по портфелю:\n {ex}"
                );
            }

            var res = await ReadAnswerListAsync(ordersResponseList);
            await botClient.SendTextMessageAsync(
                userDto.ChatId,
                $"Ваш портфель:"
                );
            foreach (var mes in res)
            {
                await botClient.SendTextMessageAsync(
                userDto.ChatId,
                mes
                );
            }
        }

        private async Task GetSummaryAnalysisAsync(Telegram.Bot.Types.Update update)
        {
            var user = await loginOperations.GetUserAsync(update.Message.Chat.Id);
            if (await CheckUserBeforeActionAsync(update) == false)
            {
                return;
            }
            var summaryReportDto = await ordersOperations.GetSummaryAnalysisAsync(user.UserIdCommon.Value);
            var res = await ReadAnswerListAsync(summaryReportDto);
            await botClient.SendTextMessageAsync(
                update.Message.Chat.Id,
                $"Суммарный анализ:",
                replyToMessageId: update.Message.MessageId
                );
            foreach (var mes in res)
            {
                await botClient.SendTextMessageAsync(
                update.Message.Chat.Id,
                mes
                );
            }
        }

        private async Task<bool> CheckUserAsync(long chatId)
        {
            return await loginOperations.CheckUserInAsync(chatId);
        }

        private async Task<bool> CheckUserBeforeActionAsync(Telegram.Bot.Types.Update update)
        {
            var user = await loginOperations.GetUserAsync(update.Message.Chat.Id);
            try
            {
                if (user == null)
                {
                    await SendNotRegisterAsync(update);
                    return false;
                }
                else if (user.IsActivated == false && await loginOperations.GetIsActivate(user) == false)
                {
                    await SendNotActivateAsync(update);
                    return false;
                }
                return true;
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

    private async Task SendNotRegisterAsync(Telegram.Bot.Types.Update update)
        {
            await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Чтобы пользоваться возможностями бота, зарегистрируйтесь с помощью команды sign_up " +
                    "или войдите с помощью команды sign_in",
                    replyToMessageId: update.Message.MessageId
                    );
        }

        private async Task SendNotActivateAsync(Telegram.Bot.Types.Update update)
        {
            await botClient.SendTextMessageAsync(
                    update.Message.Chat.Id,
                    "Аккаунт не активирован. Подтвердите свой email, чтобы воспользоваться услугами бота",
                    replyToMessageId: update.Message.MessageId
                    );
        }

        private async Task<string> DoAnswerStringAsync(object obj)
        {
            var answerString = new StringBuilder();
            var type = obj.GetType();
            var props = type.GetProperties();
            foreach(var prop in props )
            {
                if(prop.Name == "Parser" || prop.Name == "Descriptor" || prop.Name == "UserId")
                {
                    continue;
                }
                answerString.AppendLine($"{prop.Name} = {prop.GetValue(obj)}");
            }
            return answerString.ToString();
        }

        private async Task<List<string>> ReadAnswerListAsync<T>(List<T> list) where T : class
        {
            var res = new List<string>();
            var answerString = new StringBuilder();
            foreach(var obj in list)
            {
                var ans = await DoAnswerStringAsync(obj);
                if (answerString.Length + ans.Length > 4000)
                {
                    res.Add(answerString.ToString());
                    answerString = new StringBuilder();
                    answerString.AppendLine(ans);
                }
                else
                {
                    answerString.AppendLine(ans);
                }
            }
            res.Add(answerString.ToString());
            return res;
        }
    }
}
