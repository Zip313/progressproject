﻿using BLL.Models.Bus;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TeleBotService.Services.Abstract
{
    public interface ITeleBotService
    {
        Task MessageProcessing(ITelegramBotClient botClient, Update update);

        /// <summary>
        /// Запрос рассылки всем пользователям
        /// </summary>
        /// <param name="notificationType">Тип рассылки</param>
        Task SendAllNotificationAsync(NotificationTypes notificationType);
    }
}
