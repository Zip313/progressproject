﻿
using Microsoft.EntityFrameworkCore;
using TeleBotService.Configurations.Abstract;

namespace TeleBotService.Services
{
    public class StartHostedService : BackgroundService
    {
        private readonly IServiceScopeFactory scopeFactory;

        public StartHostedService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var telegramConfigurations = scope.ServiceProvider.GetRequiredService<ITelegramConfigurations>();
                Task.Run(() => telegramConfigurations.Init());
            }
        }
    }
}
