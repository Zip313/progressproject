using TeleBotService.Services.Abstract;
using TeleBotService.Services;
using TeleBotService.Configurations.Abstract;
using TeleBotService.Configurations;
using BLL.Models;
using BLL.Services.Abstract;
using BLL.Services;
using BLL.Operations.Abstractions;
using BLL.Operations;
using DAL.Db.Abstractions;
using DAL.Db;
using DAL.Extentions;
using BLL.Models.Bus;
using MassTransit;
using TeleBotService.Consumers;
using System.Reflection;
using Telegram.Bot;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.UseUrls(builder.Configuration.GetValue<string>("ApplicationUrl")!);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

TelegramMeta.CreateInstance(builder.Configuration.GetValue<string>("Token")!);
builder.Services.Configure<GrpcAddress>(builder.Configuration.GetSection("GrpcAddress"));

builder.Services.AddDAL(builder.Configuration);

builder.Services.AddHostedService<StartHostedService>();

builder.Services.AddSingleton<ITelegramConfigurations, TelegramConfigurations>();

builder.Services.AddTransient<ITeleBotService, TeleBotService.Services.TeleBotService>();
builder.Services.AddTransient<ILoginOperations, LoginOperations>();
builder.Services.AddTransient<IOrdersOperations, OrdersOperations>();
builder.Services.AddTransient<IDbProvider, DbProvider>();

builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IOrdersService, OrdersService>();

var rmqSettings = builder.Configuration.GetSection("RmqSettings").Get<RmqSettings>();
if (string.IsNullOrEmpty(rmqSettings.Login))
{
    //rmqSettings.Host= Environment.GetEnvironmentVariable("RABBIT_USR") ?? "";
    //rmqSettings.Port=""
    rmqSettings.Login = Environment.GetEnvironmentVariable("RABBIT_USR") ?? "";
    rmqSettings.Password = Environment.GetEnvironmentVariable("RABBIT_PSW") ?? "";
    Console.WriteLine($"""
                    host:{rmqSettings.Host}
                    port:{rmqSettings.Port}
                    user:{rmqSettings.Login}
                    pass:{rmqSettings.Password}
                    """);
}

builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host(rmqSettings.Host, rmqSettings.Port, rmqSettings.VHost, h =>
        {
            h.Username(rmqSettings.Login);
            h.Password(rmqSettings.Password);
        });

        cfg.ConfigureEndpoints(context);
    });

    var entryAssembly = Assembly.GetEntryAssembly();
    x.AddConsumers(entryAssembly);
});

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var serviceProvider = scope.ServiceProvider;
    try
    {
        serviceProvider.InitializeDb();
        Console.WriteLine("!!!!!!DB init is OK");
    }
    catch (Exception ex)
    {
        Console.WriteLine("!!!!!!DB init is not OK " + ex.Message);
    }
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
