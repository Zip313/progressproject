﻿using DAL.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Extentions
{
    public static class ServicesExtentions
    {
        public static IServiceCollection AddDAL(this IServiceCollection services, IConfiguration configuration)
        {
            var user = Environment.GetEnvironmentVariable("DB_USR");
            var password = Environment.GetEnvironmentVariable("DB_PSW");
            var connectionString = configuration["DbConnection"];
            connectionString = connectionString.Replace(@"<username>", user);
            connectionString = connectionString.Replace(@"<password>", password);
            services.AddDbContext<TeleBotContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            return services;
        }

        public static void InitializeDb(this IServiceProvider serviceProvider)
        {
            var ctx = serviceProvider.GetRequiredService<TeleBotContext>();
            DbInitializer.Initialize(ctx);
        }
    }

    public static class DbInitializer
    {
        /// <summary>
        /// Инициализация БД
        /// </summary>
        public static void Initialize(this TeleBotContext context)
        {
            context.Database.Migrate();
        }
    }
}
