﻿using DAL.Entities.Abstract;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class UserDto : IUserDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор пользователя для других микросервисов
        /// </summary>
        public int? UserIdCommon { get; set; }

        public string? Email { get; set; }

        public long ChatId { get; set; }

        public bool IsActivated { get; set; }
    }
}
