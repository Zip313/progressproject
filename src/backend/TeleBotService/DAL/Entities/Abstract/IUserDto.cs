﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities.Abstract
{
    public interface IUserDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [Key]
        int UserId { get; set; }

        /// <summary>
        /// Идентификатор пользователя для других микросервисов
        /// </summary>
        int? UserIdCommon { get; set; }

        string? Email { get; set; }

        long ChatId { get; set; }

        bool IsActivated { get; set; }
    }
}
