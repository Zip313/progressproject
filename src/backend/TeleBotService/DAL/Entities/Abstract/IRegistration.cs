﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities.Abstract
{
    public interface IRegistration
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        int RegistrationId { get; set; }

        long ChatId { get; set; }
    }
}
