﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Registration : IRegistration
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int RegistrationId { get; set; }

        public long ChatId { get; set; }
    }
}
