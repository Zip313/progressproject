﻿using DAL.Db.Abstractions;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Security.Authentication;

namespace DAL.Db
{
    public class DbProvider : IDbProvider
    {
        private readonly TeleBotContext _dataContext;

        public DbProvider(TeleBotContext _dataContext)
        {
            this._dataContext = _dataContext;
        }

        public async Task AddUserAsync(UserDto userInfo)
        {
            await _dataContext.Set<UserDto>().AddAsync(userInfo);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<UserDto?> GetUserAsync(long chatId)
        {
            var res = await _dataContext.Set<UserDto>().AsNoTracking().FirstOrDefaultAsync(x => x.ChatId == chatId);
            return res != null ? res : null;
        }

        public async Task<List<UserDto>> GetActiveUsersAsync()
        {
            var res = _dataContext.Set<UserDto>().AsNoTracking().Where(x => x.IsActivated == true);
            return await res.ToListAsync();
        }

        public async Task ChangeUserAsync(UserDto userInfo)
        {
            var res = await _dataContext.Set<UserDto>().FirstOrDefaultAsync(x => x.ChatId == userInfo.ChatId);
            if (res == null)
            {
                throw new InvalidCredentialException($"ChangeUserAsync. Не найден пользователь по ChatId {userInfo.ChatId}");
            }
            res.UserIdCommon = userInfo.UserIdCommon;
            res.IsActivated = userInfo.IsActivated;
            await _dataContext.SaveChangesAsync();
        }

        public async Task AddRegistrationAsync(Registration registrationInfo)
        {
            if (await _dataContext.Set<Registration>().FirstOrDefaultAsync(x => x.ChatId == registrationInfo.ChatId) != null)
            {
                return;
            }
            await _dataContext.Set<Registration>().AddAsync(registrationInfo);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<Registration?> GetRegistrationAsync(long chatId)
        {
            var res = await _dataContext.Set<Registration>().AsNoTracking().FirstOrDefaultAsync(x => x.ChatId == chatId);
            return res;
        }

        public async Task RemoveRegistrationAsync(Registration registrationInfo)
        {
            _dataContext.Set<Registration>().Remove(registrationInfo);
            await _dataContext.SaveChangesAsync();
        }
    }
}
