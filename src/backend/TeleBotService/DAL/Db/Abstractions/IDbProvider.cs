﻿using DAL.Entities;
using System.Security.Authentication;

namespace DAL.Db.Abstractions
{
    public interface IDbProvider
    {
        Task AddUserAsync(UserDto userInfo);

        Task<UserDto?> GetUserAsync(long chatId);

        Task<List<UserDto>> GetActiveUsersAsync();

        Task ChangeUserAsync(UserDto userInfo);

        Task AddRegistrationAsync(Registration registrationInfo);

        Task<Registration?> GetRegistrationAsync(long chatId);

        Task RemoveRegistrationAsync(Registration registrationInfo);
    }
}
