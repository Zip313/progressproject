﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Db
{
    public class TeleBotContext : DbContext
    {
        public TeleBotContext(DbContextOptions options) : base(options)
        {
            
        }

        //public TeleBotContext()
        //{
        //    //Database.EnsureCreated();
        //}

        public DbSet<UserDto> UserDtos { get; set; }

        public DbSet<Registration> Registrations { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseNpgsql("Host=pias-telebot;Port=5432;Database=telebot_db;Username=test;Password=test");
        //}
    }
}
