SET IMAGE-NAME=grafana/loki:latest
SET CONTAINER-NAME=LokiServer
COMPOSE_HTTP_TIMEOUT=200
docker-compose -p pias --env-file ./config/.env build 
docker compose -p pias --env-file ./config/.env up  -d --force-recreate
pause