FROM node:16-alpine as build
WORKDIR /app

#ENV PATH /app/node_modules/.bin:$PATH
COPY ./src/client/package.json /app/package.json
COPY ./src/client/package-lock.json /app/package-lock.json
COPY ./src/client/tsconfig.json /app/tsconfig.json
RUN npm install
COPY ./src/client/ /app
RUN npm run build
FROM nginx:1.16.0-alpine
COPY --from=build /app/dist/pias_ui /usr/share/nginx/html
EXPOSE 8082
CMD ["nginx", "-g", "daemon off;"]
