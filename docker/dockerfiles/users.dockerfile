FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 8080
ENV ASPNETCORE_URLS=http://*:8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/backend/UserService/UserService/UserService.csproj", "UserService/"]
COPY ["src/backend/UserService/BLL/BLL.csproj", "BLL/"]
COPY ["src/backend/UserService/DAL/DAL.csproj", "DAL/"]
RUN dotnet restore "UserService/UserService.csproj"
COPY src/backend/UserService/ .
WORKDIR "/src/UserService"
RUN dotnet build "UserService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "UserService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "UserService.dll", "--environment=Production"]