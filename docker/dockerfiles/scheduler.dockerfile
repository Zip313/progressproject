FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/backend/SchedulerService/SchedulerService/SchedulerService.csproj", "SchedulerService/"]
RUN dotnet restore "SchedulerService/SchedulerService.csproj"
COPY src/backend/SchedulerService/ .
WORKDIR "/src/SchedulerService"
RUN dotnet build "SchedulerService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SchedulerService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SchedulerService.dll", "--environment=Production"]