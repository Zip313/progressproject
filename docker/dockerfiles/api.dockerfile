FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 8080
ENV ASPNETCORE_URLS=http://*:8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/backend/GatewayAPI/WebApi/WebApi.csproj", "WebApi/"]
COPY ["src/backend/GatewayAPI/Application/Application.csproj", "Application/"]
COPY ["src/backend/GatewayAPI/Domain/Domain.csproj", "Domain/"]
COPY ["src/backend/GatewayAPI/Persistence/Persistence.csproj", "Persistence/"]
COPY ["src/backend/GatewayAPI/GrpcClient/GrpcClient.csproj", "GrpcClient/"]

RUN dotnet restore "WebApi/WebApi.csproj"
COPY src/backend/GatewayAPI/ .
WORKDIR "/src/WebApi"
RUN dotnet build "WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "WebApi.dll", "--environment=Production"]