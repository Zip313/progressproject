FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 8080
ENV ASPNETCORE_URLS=http://*:8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/backend/OrderService/OrderService.API/OrderService.API.csproj", "OrderService.API/"]
COPY ["src/backend/OrderService/OrderService.BLL/OrderService.BLL.csproj", "OrderService.BLL/"]
COPY ["src/backend/OrderService/OrderService.DAL/OrderService.DAL.csproj", "OrderService.DAL/"]

RUN dotnet restore "OrderService.API/OrderService.API.csproj"
COPY src/backend/OrderService .
WORKDIR "/src/OrderService.API"
RUN dotnet build "OrderService.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "OrderService.API.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "OrderService.API.dll", "--environment=Production"]