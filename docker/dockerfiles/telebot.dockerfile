FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["src/backend/TeleBotService/TeleBotService/TeleBotService.csproj", "TeleBotService/"]
COPY ["src/backend/TeleBotService/BLL/BLL.csproj", "BLL/"]
COPY ["src/backend/TeleBotService/DAL/DAL.csproj", "DAL/"]
RUN dotnet restore "TeleBotService/TeleBotService.csproj"
COPY src/backend/TeleBotService/ .
WORKDIR "/src/TeleBotService"
RUN dotnet build "TeleBotService.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "TeleBotService.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "TeleBotService.dll", "--environment=Production"]